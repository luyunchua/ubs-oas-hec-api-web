/**
 * 公共接口配置文件
 * * */
const serverConfig = {
  // 请求地址
  // VUE_APP_API_URL: "http://10.88.0.251:8111",//113.207.43.93:28111/
   VUE_APP_API_URL: 'http://localhost:9000',
  //VUE_APP_API_URL: 'http://10.6.8.187:9000',
  // VUE_APP_API_URL: 'http://ddz7vq.natappfree.cc',
  // http请求 put / delete是否转换成post，true：转换，false：默认不换
  VUE_APP_IS_HTTP_METHOD: false,
  // TOKEN_KEY
  VUE_APP_TOKEN_KEY: 'LFS-TOKEN', // report
  VUE_APP_REPORT_URL: ''
};
window.serverAPI = serverConfig;
