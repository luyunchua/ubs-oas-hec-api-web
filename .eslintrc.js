module.exports = {
 root: true,
 parserOptions: {
  parser: 'babel-eslint',
  sourceType: 'module'
 },
 env: {
  browser: true,
  node: true,
  es6: true
 },
 extends: ['eslint-config-ali/vue'],
 plugins: [
  'html',
  'vue',
  'standard'
 ],
 // it is base on https://github.com/vuejs/eslint-config-vue
 // "off" -> 0 关闭规则 "warn" -> 1 开启警告规则 "error" -> 2 开启错误规则
 rules: {
  'vue/max-attributes-per-line': [1, {
   singleline: 10,
   multiline: {
    max: 1,
    allowFirstLine: false
   }
  }],
  'vue/singleline-html-element-content-newline': 'off',
  'vue/multiline-html-element-content-newline': 'off',
  'vue/name-property-casing': ['error', 'PascalCase'],
  'vue/no-v-html': 'off',
  'accessor-pairs': 1, // 在对象中使用getter/setter
  'arrow-spacing': [1, {
   before: true,
   after: true
  }], // =>的前/后括号
  'block-spacing': [1, 'always'], // 大括号风格
  'brace-style': [1, '1tbs', {
   allowSingleLine: true
  }],
  camelcase: [1, {
   properties: 'always'
  }], // 强制驼峰法命名
  'comma-dangle': [1, 'never'], // 对象字面量项尾必须有逗号
  'comma-spacing': [1, {
   before: false,
   after: true
  }], // 逗号前后的空格
  'comma-style': [1, 'last'], // 逗号风格，换行时在行首还是行尾
  curly: [1, 'multi-line'], // 必须使用 if(){} 中的{}
  'eol-last': 1, // 文件以单一的换行符结束
  eqeqeq: 0, // 必须使用全等
  'generator-star-spacing': [1, {
   before: true,
   after: true
  }], // 生成器函数*的前后空格
  indent: [1, 1, {
   SwitchCase: 1
  }], // 缩进风格
  'jsx-quotes': [0, 'prefer-single'],
  'key-spacing': [1, {
   beforeColon: false,
   afterColon: true
  }], // 对象字面量中冒号的前后空格
  'keyword-spacing': [1, {
   before: true,
   after: true
  }],
  'new-cap': [1, {
   newIsCap: true,
   capIsNew: false
  }], // 函数名首行大写必须使用new方式调用，首行小写必须用不带new方式调用
  'dot-location': [1, 'property'], // 选项要求点与属性位于同一行
  'constructor-super': 1, // 非派生类不能调用super，派生类必须调用super
  'handle-callback-err': [0, '^(err|error)$'], // nodejs 处理错误
  'new-parens': 1, // new时必须加小括号
  'no-array-constructor': 1, // 禁止使用数组构造器
  'no-caller': 1, // 禁止使用arguments.caller或arguments.callee
  'no-console': 'off',
  'no-class-assign': 1, // 禁止给类赋值
  'no-cond-assign': 1, // 禁止在条件表达式中使用赋值语句
  'no-const-assign': 1, // 禁止修改const声明的变量
  'no-control-regex': 0, // 禁止在正则表达式中使用控制字符
  'no-delete-var': 1, // 不能对var声明的变量使用delete操作符
  'no-dupe-args': 1, // 函数参数不能重复
  'no-dupe-class-members': 1,
  'no-dupe-keys': 1, // 在创建对象字面量时不允许键重复 {a:1,a:1}
  'no-duplicate-case': 1, // switch中的case标签不能重复
  'no-empty-character-class': 1, // 正则表达式中的[]内容不能为空
  'no-empty-pattern': 1,
  'no-eval': 1, // 禁止使用eval
  'no-ex-assign': 1, // 禁止给catch语句中的异常参数赋值
  'no-extend-native': 1, // 禁止扩展native对象
  'no-extra-bind': 1, // 禁止不必要的函数绑定
  'no-extra-boolean-cast': 1, // 禁止不必要的bool转换
  'no-extra-parens': [1, 'functions'], // 禁止非必要的括号
  'no-fallthrough': 1, // 禁止switch穿透
  'no-floating-decimal': 1, // 禁止省略浮点数中的0 .5 3.
  'no-func-assign': 1, // 禁止重复的函数声明
  'no-implied-eval': 1, // 禁止使用隐式eval
  'no-inner-declarations': [1, 'functions'], // 禁止在块语句中使用声明（变量或函数）
  'no-invalid-regexp': 1, // 禁止无效的正则表达式
  'no-irregular-whitespace': 1, // 不能有不规则的空格
  'no-iterator': 1, // 禁止使用__iterator__ 属性
  'no-label-var': 1, // label名不能与var声明的变量名相同
  'no-labels': [1, {
   allowLoop: false,
   allowSwitch: false
  }], // 禁止标签声明
  'no-lone-blocks': 1, // 禁止不必要的嵌套块
  'no-mixed-spaces-and-tabs': 1, // 禁止混用tab和空格
  'no-multi-spaces': 1, // 不能用多余的空格
  'no-multi-str': 1, // 字符串不能用\换行
  'no-multiple-empty-lines': [1, {
   max: 1
  }], // 空行最多不能超过2行
  'no-native-reassign': 1, // 不能重写native对象
  'no-negated-in-lhs': 1, // in 操作符的左边不能有!
  'no-new-object': 1, // 禁止使用new Object()
  'no-new-require': 1, // 禁止使用new require
  'no-new-symbol': 1,
  'no-new-wrappers': 1, // 禁止使用new创建包装实例，new String new Boolean new Number
  'no-obj-calls': 1, // 不能调用内置的全局对象，比如Math() JSON()
  'no-octal': 1, // 禁止使用八进制数字
  'no-octal-escape': 1, // 禁止使用八进制转义序列
  'no-path-concat': 1, // node中不能使用__dirname或__filename做路径拼接
  'no-proto': 1, // 禁止使用__proto__属性
  'no-redeclare': 1, // 禁止重复声明变量
  'no-regex-spaces': 1, // 禁止在正则表达式字面量中使用多个空格 /foo bar/
  'no-return-assign': [1, 'except-parens'], // return 语句中不能有赋值表达式
  'no-self-assign': 1,
  'no-self-compare': 1, // 不能比较自身
  'no-sequences': 1, // 禁止使用逗号运算符
  'no-shadow': 0,
  'no-shadow-restricted-names': 1, // 严格模式中规定的限制标识符不能作为声明时的变量名使用
  'no-spaced-func': 1, // 函数调用时 函数名与()之间不能有空格
  'no-sparse-arrays': 1, // 禁止稀疏数组， [1,,2]
  'no-this-before-super': 1, // 在调用super()之前不能使用this或super
  'no-throw-literal': 1, // 禁止抛出字面量错误 throw "error";
  'no-trailing-spaces': 1, // 一行结束后面不要有空格
  'no-undef': 1, // 不能有未定义的变量
  'no-undef-init': 1, // 变量初始化时不能直接给它赋值为undefined
  'no-unexpected-multiline': 1, // 避免多行表达式
  'no-unmodified-loop-condition': 1,
  'no-unneeded-ternary': [1, {
   defaultAssignment: false
  }], // 禁止不必要的嵌套 var isYes = answer === 1 ? true : false;
  'no-unreachable': 1, // 不能有无法执行的代码
  'no-unsafe-finally': 1,
  'no-unused-vars': [0, { // 不能有声明后未被使用的变量或参数
   vars: 'all',
   args: 'none'
  }],
  'no-useless-call': 1, // 禁止不必要的call和apply
  'no-useless-computed-key': 1,
  'no-useless-constructor': 1,
  'no-useless-escape': 0,
  'no-whitespace-before-property': 1,
  'no-with': 1, // 禁用with
  'one-var': [1, {
   initialized: 'never'
  }], // 连续声明
  'operator-linebreak': [1, 'after', {
   overrides: {
    '?': 'before',
    ':': 'before'
   }
  }], // 换行时运算符在行尾还是行首
  'padded-blocks': [1, 'never'], // 块语句内行首行尾是否要空行
  quotes: [1, 'single', {
   avoidEscape: true,
   allowTemplateLiterals: true
  }], // 引号类型 `` "" ''
  semi: [1, 'always'], // 语句强制分号结尾
  'semi-spacing': [1, {
   before: false,
   after: true
  }], // 分号前后空格
  'space-before-blocks': [1, 'always'], // 不以新行开始的块{前面要不要有空格
  'space-before-function-paren': [1, 'never'], // 函数定义时括号前面要不要有空格
  'space-in-parens': [1, 'never'], // 小括号里面要不要有空格
  'space-infix-ops': 1, // 中缀操作符周围要不要有空格
  'space-unary-ops': [1, {
   words: true,
   nonwords: false
  }], // 一元运算符的前/后要不要加空格
  'spaced-comment': [1, 'always', {
   markers: ['global', 'globals', 'eslint', 'eslint-disable', '*package', '!', ',']
  }], // 注释风格要不要有空格什么的
  'template-curly-spacing': [1, 'never'],
  'use-isnan': 1, // 禁止比较时使用NaN，只能用isNaN()
  'valid-typeof': 1, // 必须使用合法的typeof的值
  'wrap-iife': [1, 'any'], // 立即执行函数表达式的小括号风格
  'yield-star-spacing': [1, 'both'],
  yoda: [1, 'never'], // 禁止尤达条件
  'prefer-const': 1, // 首选const
  'no-debugger': process.env.NODE_ENV === 'production' ? 1 : 0, // 禁止使用debugger
  'object-curly-spacing': [1, 'always', {
   objectsInObjects: false
  }], // 大括号内是否允许不必要的空格
  'array-bracket-spacing': [1, 'never'], // 是否允许非空数组里面有多余的空格
  'max-lines-per-function': [1, { max: 200, skipBlankLines: true }], // 函数最大行数
  'no-param-reassign': [0, { props: false }],
  'max-len': 0 // 一行最大代码量
 }
};
