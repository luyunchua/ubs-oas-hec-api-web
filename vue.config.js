'use strict';
const path = require('path');
const defaultSettings = require('./src/settings.js');
const packageJson = require('./package.json');

const currentVersion = packageJson.version || '1.0.0';
const env = process.env.NODE_ENV === 'production' ? 'prod' : 'dev';
const timeStamp = new Date().getTime();
// 构造打包文件前缀
const outPutPreFix = `${env}-${currentVersion}-${timeStamp}`;
function resolve (dir) {
  return path.join(__dirname, dir);
}

const name = defaultSettings.title; // 网址标题
const port = 8010; // 端口配置
module.exports = {
  publicPath: '/jw-web',
  outputDir: 'jw-web',
  assetsDir: 'static',
  lintOnSave: env === 'dev',
  // lintOnSave: false,
  productionSourceMap: false, // 打包时压缩代码为false,不压缩到为true
  css: {
    // extract CSS in components into a single CSS file (only in production)
    // can also be an object of options to pass to extract-text-webpack-plugin
    extract: {
      filename: `css/[name].${outPutPreFix}.css`,
      chunkFilename: `css/[name].${outPutPreFix}.css`
    },
    // enable CSS source maps?
    sourceMap: false,
    loaderOptions: {
      postcss: {
        plugins: [
          require('postcss-pxtorem')({
            rootValue: 100, // 换算的基数
            propList: ['*']
          })
        ]
      }
    }
  },
  configureWebpack: {
    name,
    output: {
      filename: `js/[name].${outPutPreFix}.js`,
      chunkFilename: `js/[name].${outPutPreFix}.js`
    },
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  chainWebpack (config) {
    config.plugins.delete('preload'); // TODO: need test
    config.plugins.delete('prefetch'); // TODO: need test
    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/assets/icons'))
      .end();
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/assets/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end();

    // set preserveWhitespace
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap((options) => {
        options.compilerOptions.preserveWhitespace = true;
        return options;
      })
      .end();

    // config
    // // https://webpack.js.org/configuration/devtool/#development
    //  .when(process.env.NODE_ENV === 'development', config =>
    //   config.devtool('cheap-source-map')
    //  )

    config.when(env !== 'dev', (config) => {
      config
        .plugin('ScriptExtHtmlWebpackPlugin')
        .after('html')
        .use('script-ext-html-webpack-plugin', [
          {
            // `runtime` must same as runtimeChunk name. default is `runtime`
            inline: /runtime\..*\.js$/
          }
        ])
        .end();
      config.optimization.splitChunks({
        chunks: 'all',
        cacheGroups: {
          libs: {
            name: 'chunk-libs',
            test: /[\\/]node_modules[\\/]/,
            priority: 10,
            chunks: 'initial' // only package third parties that are initially dependent
          },
          elementUI: {
            name: 'chunk-elementui', // split elementUI into a single package
            priority: 20,
            test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
          },
          commons: {
            name: 'chunk-commons',
            test: resolve('src/components'), // can customize your rules
            minChunks: 3, //  minimum common number
            priority: 5,
            reuseExistingChunk: true
          }
        }
      });
      config.optimization.runtimeChunk('single');
    });
  },
  // Node Modules 依赖转译防止IE中白板
  transpileDependencies: ['crypto-js'],
  devServer: {
    port,
    open: false,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      '/lfs-system': {
        target: process.env.VUE_APP_BASE_API,
        changeOrigin: true,
        pathRewrite: {
        }
      },
      '/lfs-core': {
        target: process.env.VUE_APP_BASE_API,
        changeOrigin: true,
        pathRewrite: {
          '^/auth': 'auth'
        }
      }
    }
  }
};
