module.exports = {
       title: '哈电境外国有资产监管',
       /**
              * @description 是否只保持一个子菜单的展开
              */
       uniqueOpened: true,
       /**
      
             // /**
             //  * @type {boolean} true | false
             //  * @description Whether show the settings right-panel
             //  */
       // showSettings: true,

       /**
              * @type {boolean} true | false
              * @description Whether need tagsView
              */
       tagsView: true,

       /**
              * @type {boolean} true | false
              * @description Whether fix the header
              */
       fixedHeader: false,

       /**
              * @type {boolean} true | false
              * @description Whether show the logo in sidebar
              */
       sidebarLogo: false,

       /**
              * @type {string | array} 'production' | ['production', 'development']
              * @description Need show err logs component.
              * The default is only used in the production env
              * If you want to also use it in dev, you can pass ['production', 'development']
              */
       errorLog: 'production',

       /**
              * @description 记住密码状态下的密码在Cookie中存储的天数，默认1天s
              */
       passCookieExpires: 1,
       // 是否为模拟请求数据
       isMock: false,
       // 请求参数添加验签
       signSecret: 'qdp-secret'
};
