import router, { resetRouter } from '@/router/index.js';

import store from '@/store';
import Config from '@/settings';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { getToken, setToken } from '@/utils/auth'; // getToken from cookie
import { getUserMenuTree } from '@/api/api-info/system-manage/menu';
import { handelAsyncRoutes, handelDynamicRoutes } from '@/utils/handle-routes';
import menuMock from '@/mock/system-manage/menu-mock';

NProgress.configure({ showSpinner: false });
const whiteList = ['/login', '/ssoLogin'];
// 路由前置守卫
router.beforeEach(async (to, from, next) => {
  if (to.path === '/ssoLogin') {
    setToken('');
  }
  // 设置Title
  document.title = Config.title;
  // 打开进度条
  NProgress.start();
  // 获取Token
  const token = getToken() == 'undefined' ? undefined : getToken();
  // Token存在
  console.log(token);
  if (token) {
    // 登录成功后再次点击登录
    if (to.path === '/login') {
      next({ path: '/' });
      NProgress.done();
    } else {
      // 跳转至认证通过的第一个路由
      const routes = store.getters.permissionRoutes;
      const { isLoadRouters } = store.getters;
      if (routes.length > 0 || isLoadRouters) {
        next();
      } else {
        // 加载菜单
        await loadMenus();
        // 跳转
        next({ ...to, replace: true });
      }
    }
  } else if (whiteList.indexOf(to.path) !== -1) { // 不存在Token
    // 白名单通过
    next();
  } else {
    // 否则全部重定向到登录页
    next('/login');
    NProgress.done();
  }
});
// 加载菜单
export const loadMenus = async () => {
  const res = await getUserMenuTree({ menuGroup: 0, template: menuMock.menuTemplate });
  let asyncRouter = res.data || [];
  // 处理异步路由
  asyncRouter = handelAsyncRoutes(asyncRouter);
  // 处理动态路由
  const dynamicRoutes = handelDynamicRoutes(asyncRouter);
  // 最后添加404页面
  dynamicRoutes.push(
    {
      path: '*',
      redirect: '/404',
      hidden: true,
      meta: { unGuard: true }
    }
  );
  // 重置路由防止重复
  resetRouter();
  // 添加动态路由
  router.addRoutes(dynamicRoutes);
  // 合并路由
  await store.dispatch('permission/generateRoutes', asyncRouter);
  // 获取按钮权限
  store.dispatch('permission/getButtonAuthList');
  // 菜单第一加载后,则不再加载
  store.dispatch('user/updateLoadMenus');
  store.dispatch('permission/getOrganizationList');
  //   密码失效
  store.dispatch('user/passwordAging');
};
// 路由后置守卫
router.afterEach(() => {
  NProgress.done();
});
