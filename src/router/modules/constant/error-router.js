/**
 *create by Jancheng ON 2022/1/6
 *
 **/
export default [
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true,
    meta: { unGuard: true }
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true,
    meta: { unGuard: true }
  }
];
