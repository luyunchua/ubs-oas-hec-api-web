/**
 *create by Jancheng ON 2022/1/6
 *
 **/
export default [
 {
  path: '/login',
  component: () => import('@/views/Login'),
  hidden: true,
  meta: { unGuard: true }
 },
 {
  path: '/ssoLogin',
  component: () => import('@/views/SsoLogin'),
  hidden: true,
  meta: { unGuard: true }
 },
 {
  path: '/user',
  component: () => import('@/layout/Index'),
  hidden: true,
  redirect: 'noredirect',
  children: [{
   path: 'center',
   component: () => import('@/views/system-manage/user/Center'),
   name: '个人中心',
   meta: { title: '个人中心', unGuard: true }
  }]
 }
];
