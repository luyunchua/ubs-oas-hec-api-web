/**
 *create by Jancheng ON 2022/1/6
 *
 **/
export default [
 {
  meta: { title: '大额资金备案管理', icon: 'xitongguanli', noCache: true },
  name: 'recordManage',
  path: '/recordManage',
  redirect: 'autoRedirect',
  hidden: false,
  alwaysShow: true,
  component: () => import('@/layout/Index'),
  children: [
   {
    path: 'recordDeal',
    name: 'recordDeal',
    component: () => import('@/views/record-manage/record-deal/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '大额资金备案处理'
    },
    hidden: false
   },
   {
    path: 'useState',
    name: 'useState',
    component: () => import('@/views/record-manage/use-state/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '已备案大额资金使用情况'
    },
    hidden: true
   },
   {
    path: 'accList',
    name: 'accList',
    component: () => import('@/views/record-manage/use-state/AccList.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '对账单详情'
    },
    hidden: false,
    isPermission: false // 通常为不需要认证的路由（主要用于解决页面中按钮需要跳转路由）
   },
   {
    path: 'recordApp',
    name: 'recordApp',
    component: () => import('@/views/record-manage/record-deal/RecordApp.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '备案审批'
    },
    hidden: false,
    isPermission: false // 通常为不需要认证的路由（主要用于解决页面中按钮需要跳转路由）
   },
   {
    path: 'doneDetail', // 已备案详情
    name: 'doneDetail',
    component: () => import('@/views/record-manage/record-deal/DoneDetail.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '查看详情'
    },
    hidden: false,
    isPermission: false // 通常为不需要认证的路由（主要用于解决页面中按钮需要跳转路由）
   },
   {
    path: 'backDetail', // 已退回详情
    name: 'backDetail',
    component: () => import('@/views/record-manage/record-deal/BackDetail.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '查看详情'
    },
    hidden: false,
    isPermission: false // 通常为不需要认证的路由（主要用于解决页面中按钮需要跳转路由）
   }
  ]
 }
];
