/**
 *create by Jancheng ON 2022/1/6
 *
 **/
export default [
  {
    meta: { title: '金融衍生业务', icon: 'xitongguanli', noCache: true },
    name: 'jrys',
    path: '/jrys',
    redirect: 'autoRedirect', // autoRedirect 自动重定向第一个子节点 noRedirect:不重定向 也可以自定义重新下地址
    hidden: false,
    alwaysShow: true,
    component: () => import('@/layout/Index'),
    children: [{
      path: 'electronicFilling', // 如果菜单为一个单节点，此项path设置为空，防止菜单点击后，出现没选中的情况
      name: 'electronicFilling',
      isPermission: false, // 通常为不需要认证的路由（主要用于解决页面中按钮需要跳转路由）
      component: () => import('@/views/electronic-filling/Index'),
      meta: { icon: 'peoples', noCache: true, title: '电子填报' }
    },
    {
      path: 'riskDaily', // 如果菜单为一个单节点，此项path设置为空，防止菜单点击后，出现没选中的情况
      name: 'riskDaily',
      isPermission: false, // 通常为不需要认证的路由（主要用于解决页面中按钮需要跳转路由）
      component: () => import('@/views/risk-daily/Index'),
      meta: { icon: 'peoples', noCache: true, title: '风控日报' }
    },
    {
      path: 'monthlyReport', // 如果菜单为一个单节点，此项path设置为空，防止菜单点击后，出现没选中的情况
      name: 'monthlyReport',
      isPermission: false, // 通常为不需要认证的路由（主要用于解决页面中按钮需要跳转路由）
      component: () => import('@/views/monthly-report/Index'),
      meta: { icon: 'peoples', noCache: true, title: '业务月报' }
    },
    {
      path: 'statisticalReport', // 如果菜单为一个单节点，此项path设置为空，防止菜单点击后，出现没选中的情况
      name: 'statisticalReport',
      isPermission: false, // 通常为不需要认证的路由（主要用于解决页面中按钮需要跳转路由）
      component: () => import('@/views/statistical-report/Index'),
      meta: { icon: 'peoples', noCache: true, title: '统计报表' }
    }]
  }
];
