/**
 *create by Jancheng ON 2022/1/6
 *
 **/
export default [
 {
  meta: { title: '大额资金预警', icon: 'xitongguanli', noCache: true },
  name: 'warnManage',
  path: '/warnManage',
  redirect: 'autoRedirect',
  hidden: false,
  alwaysShow: true,
  component: () => import('@/layout/Index'),
  children: [
   {
    path: 'expandWarn',
    name: 'expandWarn',
    component: () => import('@/views/warn-manage/expand-warn/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '大额支出预警'
    },
    hidden: false
   },
   {
    path: 'accWarn',
    name: 'accWarn',
    component: () => import('@/views/warn-manage/acc-warn/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '累计笔数超标预警'
    },
    hidden: false
   },
   {
    path: 'moneyWarn',
    name: 'moneyWarn',
    component: () => import('@/views/warn-manage/money-warn/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '累计金额超标预警'
    },
    hidden: false
   },
   {
    path: 'unrecordWarn',
    name: 'unrecordWarn',
    component: () => import('@/views/warn-manage/unrecord-warn/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '未备案支出预警'
    },
    hidden: false
   },
   {
    path: 'debtWarn',
    name: 'debtWarn',
    component: () => import('@/views/warn-manage/debt-warn/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '对外担保预警'
    },
    hidden: false
   },
   {
    path: 'fileWarn',
    name: 'fileWarn',
    component: () => import('@/views/warn-manage/file-warn/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '文件超时未报预警'
    },
    hidden: false
   },
   {
    path: 'periodWarn',
    name: 'periodWarn',
    component: () => import('@/views/warn-manage/period-warn/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '同一账户周期收入预警'
    },
    hidden: false
   },
   {
    path: 'stockWarn',
    name: 'stockWarn',
    component: () => import('@/views/warn-manage/money-safe/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '资金安全存量预警'
    },
    hidden: false
   },
   {
    path: 'warnAcc',
    name: 'warnAcc',
    component: () => import('@/views/warn-manage/WarnAcc.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '预警对账单详情'
    },
    isPermission: false,
    hidden: false
   }
  ]
 }
];
