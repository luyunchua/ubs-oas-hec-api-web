/**
 *create by Jancheng ON 2022/1/6
 *
 **/
export default [
  {
    meta: { title: '安全生产应急', icon: 'xitongguanli', noCache: true },
    name: 'safetyProduction',
    path: '/safetyProduction',
    // redirect: 'autoRedirect', // autoRedirect 自动重定向第一个子节点 noRedirect:不重定向 也可以自定义重新下地址
    hidden: false,
    alwaysShow: true,
    component: () => import('@/layout/Index'),
    children: [{
      path: '', // 如果菜单为一个单节点，此项path设置为空，防止菜单点击后，出现没选中的情况
      name: 'safetyProduction',
      isPermission: false, // 通常为不需要认证的路由（主要用于解决页面中按钮需要跳转路由）
      component: () => import('@/views/safety-production/Index'),
      meta: { icon: 'peoples', noCache: true, title: '安全生产应急' }
    }]
  }
];
