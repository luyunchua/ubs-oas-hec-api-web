/**
 *create by Jancheng ON 2022/1/6
 *
 **/
export default [
  {
    meta: { title: '违规监督追责', icon: 'xitongguanli', noCache: true },
    name: 'supervisionAndAccountability',
    path: '/supervisionAndAccountability',
    redirect: 'autoRedirect',
    hidden: false,
    alwaysShow: true,
    component: () => import('@/layout/Index'),
    children: [
      {
        path: 'dynamicMonitoring',
        name: 'dynamicMonitoring',
        component: () => import('@/views/supervision-and-accountability/dynamic-monitoring/Index.vue'),
        meta: {
          icon: 'configuration',
          noCache: true,
          title: '国有资产损失风险动态监测'
        },
        hidden: false
      },
      {
        path: 'rulesRegulations',
        name: 'rulesRegulations',
        component: () => import('@/views/supervision-and-accountability/rules-regulations/Index.vue'),
        meta: {
          icon: 'configuration',
          noCache: true,
          title: '人员禁入限制及规章制度情况'
        },
        hidden: false
      },
      {
        path: 'workInvestigation',
        name: 'workInvestigation',
        component: () => import('@/views/supervision-and-accountability/work-investigation/Index.vue'),
        meta: {
          icon: 'configuration',
          noCache: true,
          title: '违规经营投资责任追究工作'
        },
        hidden: false
      }
    ]
  }
];
