/**
 *create by Jancheng ON 2022/1/6
 *
 **/
export default [
 {
  meta: { title: '系统管理', icon: 'xitongguanli', noCache: true },
  name: 'systemManage',
  path: '/systemManage',
  redirect: 'autoRedirect',
  hidden: false,
  alwaysShow: true,
  component: () => import('@/layout/Index'),
  children: [
   {
    meta: { title: '权限管理', icon: 'xitongguanli', noCache: true },
    name: 'authManage',
    path: 'authManage',
    redirect: 'autoRedirect',
    hidden: false,
    alwaysShow: true,
    component: () => import('@/layout/SecondLayout.vue'),
    children: [
     {
      meta: { title: '账号关系', icon: 'xitongguanli', noCache: true },
      name: 'account',
      path: 'account',
      redirect: 'autoRedirect',
      hidden: false,
      alwaysShow: true,
      component: () => import('@/layout/SecondLayout.vue'),
      children: [
       {
        path: 'user',
        name: '用户管理',
        component: () => import('@/views/system-manage/user/Index.vue'),
        meta: { icon: 'peoples', noCache: true, title: '用户管理' }
       },
       {
        path: 'role',
        name: '角色管理',
        component: () => import('@/views/system-manage/role/Index.vue'),
        meta: { icon: 'role', noCache: true, title: '角色管理' },
        hidden: false
       },
       {
        path: 'dept',
        name: '组织机构管理',
        component: () => import('@/views/system-manage/dept/Index'),
        meta: { icon: 'dept', noCache: true, title: '组织机构管理' },
        hidden: false
       }
      ]
     },
     {
      meta: { title: '功能权限', icon: 'xitongguanli', noCache: true },
      name: 'functionAuth',
      path: 'functionAuth',
      redirect: 'autoRedirect',
      hidden: false,
      alwaysShow: true,
      component: () => import('@/layout/SecondLayout.vue'),
      children: [
       {
        path: 'menu',
        name: 'menu',
        component: () => import('@/views/system-manage/menu/Index.vue'),
        meta: { icon: 'menu', noCache: true, title: '菜单管理' },
        hidden: false
       },
       {
        path: 'apiManage',
        name: 'apiManage',
        component: () => import('@/views/system-manage/api-manage/Index.vue'),
        meta: { icon: 'database', noCache: true, title: '系统接口管理', isBack: true },
        hidden: false
       }
      ]
     },
     {
      meta: { title: '数据权限', icon: 'xitongguanli', noCache: true },
      name: 'dataAuth',
      path: 'dataAuth',
      redirect: 'autoRedirect',
      hidden: false,
      alwaysShow: true,
      component: () => import('@/layout/SecondLayout.vue'),
      children: [
       {
        path: 'dataModelManage',
        name: 'dataModelManage',
        component: () => import('@/views/system-manage/data-auth/data-model-manage/Index.vue'),
        meta: { icon: 'peoples', noCache: true, title: '权限模型' }
       },
       {
        path: 'dataRuleManage',
        name: 'dataRuleManage',
        component: () => import('@/views/system-manage/data-auth/data-rule-manage/Index.vue'),
        meta: { icon: 'role', noCache: true, title: '权限规则' },
        hidden: false
       }
      ]
     }
    ]
   },
   {
    path: 'process',
    name: 'process',
    meta: { icon: 'database', noCache: true, title: '流程管理' },
    hidden: false,
    redirect: 'autoRedirect',
    component: () => import('@/layout/SecondLayout.vue'),
    children: [
     {
      path: 'processClassify',
      name: 'processClassify',
      meta: { icon: 'database', noCache: true, title: '流程分类' },
      hidden: false,
      component: () => import('@/views/system-manage/process/process-classify/Index.vue')
     },
     {
      path: 'processDraw',
      name: 'processDraw',
      meta: { icon: 'database', noCache: true, title: '流程绘制' },
      hidden: false,
      component: () => import('@/views/system-manage/process/process-draw/Index.vue')
     },
     {
      path: 'processDeploy',
      name: 'processDeploy',
      meta: { icon: 'database', noCache: true, title: '流程部署' },
      hidden: false,
      component: () => import('@/views/system-manage/process/process-deploy/Index.vue')
     },
     {
      path: 'processSet',
      name: 'processSet',
      meta: { icon: 'database', noCache: true, title: '流程配置' },
      hidden: false,
      component: () => import('@/views/system-manage/process/process-set/Index.vue')
     },
     {
      path: 'processEdit',
      name: 'processEdit',
      hidden: true,
      isPermission: false,
      meta: { icon: 'database', noCache: true, title: '新增流程' },
      component: () => import('@/views/system-manage/process/process-draw/ProcessDraw.vue')
     }
    ]
   },
   {
    path: 'logManage',
    name: 'logManage',
    component: () => import('@/views/system-manage/log-manage/Index.vue'),
    meta: { icon: 'dictionary', noCache: true, title: '日志监控' },
    hidden: false
   },
   {
    path: 'dict',
    name: 'dict',
    component: () => import('@/views/system-manage/dict/Index.vue'),
    meta: { icon: 'dictionary', noCache: true, title: '字典管理' },
    hidden: false
   },
   {
    path: 'configuration',
    name: 'configuration',
    component: () => import('@/views/system-manage/configuration/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '系统配置'
    },
    hidden: false
   }
  ]
 }
];
