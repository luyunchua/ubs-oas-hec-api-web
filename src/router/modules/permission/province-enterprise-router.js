/**
 *yani 0309
 *
 **/
export default [
 {
  meta: { title: '监管企业内部参数配置', icon: 'xitongguanli', noCache: true },
  name: 'provinceEnterprise',
  path: '/provinceEnterprise',
  redirect: 'autoRedirect',
  hidden: false,
  alwaysShow: true,
  component: () => import('@/layout/Index'),
  children: [
   {
    path: 'largeSpend',
    name: 'largeSpend',
    component: () => import('@/views/province-enterprise/large-spend/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '大额支出额度设置'
    },
    hidden: false
   },
   {
    path: 'totalAccount',
    name: 'totalAccount',
    component: () => import('@/views/province-enterprise/total-account/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '累计支出笔数设置'
    },
    hidden: false
   },
   {
    path: 'totalLimit',
    name: 'totalLimit',
    component: () => import('@/views/province-enterprise/total-limit/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '累计支出额度设置'
    },
    hidden: false
   },
   {
    path: 'moneySafe',
    name: 'moneySafe',
    component: () => import('@/views/province-enterprise/money-safe/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '资金安全存量设置'
    },
    hidden: false
   },
   {
    path: 'externalGuarantee',
    name: 'externalGuarantee',
    component: () => import('@/views/province-enterprise/external-guarantee/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '对外担保设置'
    },
    hidden: false
   },
   {
    path: 'filePeriod',
    name: 'filePeriod',
    component: () => import('@/views/province-enterprise/file-period/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '报送文件周期设置'
    },
    hidden: false
   },
   {
    path: 'sameAccount',
    name: 'sameAccount',
    component: () => import('@/views/province-enterprise/same-account/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '同一账户周期收入预警'
    },
    hidden: false
   }
  ]
 }
];
