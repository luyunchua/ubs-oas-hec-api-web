/**
 *create by xhq ON 2022/3/9
 *
 **/
export default [
 {
  meta: { title: '监管企业管理', icon: 'list', noCache: true },
  name: 'company-manage',
  path: '/company-manage',
  redirect: 'autoRedirect',
  hidden: false,
  alwaysShow: true,
  component: () => import('@/layout/Index'),
  children: [
   {
    path: 'companyList',
    name: 'companyList',
    component: () => import('@/views/company-manage/company-list/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '监管企业列表'
    },
    hidden: false
   },
   {
    path: 'fundStatement',
    name: 'fundStatement',
    component: () => import('@/views/company-manage/fund-statement/Index.vue'),
    meta: {
     icon: 'configuration',
     noCache: true,
     title: '大额资金报表'
    },
    hidden: true
   },
   {
    path: 'companyDetails',
    name: 'companyDetails',
    component: () => import('@/views/company-manage/company-details/Index.vue'),
    meta: {
     icon: 'companyDetails',
     noCache: true,
     title: '查看详情'
    },
    hidden: false,
    isPermission: false // 通常为不需要认证的路由（主要用于解决页面中按钮需要跳转路由）
   }
  ]
 }
];
