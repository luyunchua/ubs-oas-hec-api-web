import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

// 常量路由文件
const constantFiles = require.context('./modules/constant', true, /\-router.js$/);
// 认证路由文件
const permissionFiles = require.context('./modules/permission', true, /\-router.js$/);

// 加载路由信息
function loadRouterInfo(routerFiles) {
 let routerArray = [];
 routerFiles.keys().forEach((key) => {
  const constantRoutes = routerFiles(key).default;
  routerArray = routerArray.concat(constantRoutes);
 });
 return routerArray;
}

// 常量路由信息
export const constantRoutes = loadRouterInfo(constantFiles);

// 认证路由信息
export const permissionRoutes = loadRouterInfo(permissionFiles);

// 创建路由
const createRouter = () => new Router({
 routes: constantRoutes
});

// 路由信息
const router = createRouter();

// 重置路由
export function resetRouter() {
 const newRouter = createRouter();
 router.matcher = newRouter.matcher;
}

export default router;
