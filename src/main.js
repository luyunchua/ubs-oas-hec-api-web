import Vue from 'vue';

import Cookies from 'js-cookie';

import 'normalize.css/normalize.css'; // a modern alternative to CSS resets
import Element from 'element-ui';
import '@/assets/styles/element-variables.scss';
import moment from 'moment';
import '@/assets/styles/index.scss'; // global css
import '@/assets/styles/icon.css'; // 字体图标

import App from './App';
import store from './store';
import router from './router/index';
import './router/permission'; // permission control
import './assets/icons'; // icon
import './utils/error-log'; // error log

// import * as filters from './filters' // global filters
// 权限指令
import { isHaveBtnAuth } from '@/utils/permission';

// vxe-table 可编辑表格
import 'xe-utils';
import VXETable from 'vxe-table';
import 'vxe-table/lib/style.css';
// Vue.use(EleForm)
// 引入复制
import VueClipboard from 'vue-clipboard2';

import * as echarts from 'echarts';
import htmlToPdf from '@/utils/htmlToPdf';
// import { detectZoom } from '@/utils/detectZoom.js';
// const m = detectZoom();
// document.body.style.zoom = 100 / Number(m);

Vue.use(htmlToPdf);
Vue.use(VXETable);

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
Vue.prototype.$moment = moment;
Vue.prototype.$isHaveBtnAuth = isHaveBtnAuth;
Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
  // locale: enLang // 如果使用中文，无需设置，请删除
});
VueClipboard.config.autoSetContainer = true;
Vue.use(VueClipboard);
Vue.prototype.$echarts = echarts;

Vue.config.productionTip = false;
const vue = new Vue({
  el: '#qdp-frontend',
  router,
  store,
  render: (h) => h(App)
});
