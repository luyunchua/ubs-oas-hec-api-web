/**
 *create by yani ON 2022/2/7
 *
 **/
const todoTableDataTemplate = [
 {
  expendType: '全账户',
  period: '周',
  preMoney: '100万',
  currency: 'CNY',
  netAsset: '1~100000',
  fileType: '大额支出报表',
  deadline: '2022-01-9 16:45:37'
 },
 {
  expendType: '同一账户',
  period: '周',
  preMoney: '200万',
  currency: 'CNY',
  netAsset: '1~200000',
  fileType: '资金存量分布表',
  deadline: '2022-01-9 18:45:30'
 },
 {
  expendType: '全账户',
  period: '日',
  preMoney: '50万',
  currency: 'CNY',
  netAsset: '1~100000',
  fileType: '资金存量分布表',
  deadline: '2022-01-10 09:24:11'
 },
 {
  expendType: '全账户',
  period: '周',
  preMoney: '50万',
  currency: 'CNY',
  netAsset: '1~100000',
  fileType: '大额支出报表',
  deadline: '2022-01-11 16:11:37'
 },
 {
  expendType: '同一账户',
  period: '周',
  preMoney: '200万',
  currency: 'CNY',
  netAsset: '1~150000',
  fileType: '银行开户情况表',
  deadline: '2022-01-12 09:45:36'
 },
 {
  expendType: '全账户',
  period: '日',
  preMoney: '80万',
  currency: 'CNY',
  netAsset: '1~100000',
  fileType: '资金存量分布表',
  deadline: '2022-01-13 16:45:01'
 }
];

const provinceMock = {
 // 添加模板
 todoTableDataTemplate
};
export default provinceMock;
