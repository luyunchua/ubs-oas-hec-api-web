/**
 *create by Jancheng ON 2022/1/14
 *
 **/
const loginTemplate = {
  message: 'success',
  code: 0,
  data: {
    organizationId: 'd86a18e6c52fe718a4693621c28bef8f',
    personName: '系统管理员',
    enterpriseCode: 'XXXX-Company',
    organizationName: 'XXXX集团公司',
    id: '1',
    ssoToken: 'NTQzYmRhNDk0NDU3MzRhZWE1YzZjMThhNTY5MDRlYmM=',
    enterpriseId: 'd86a18e6c52fe718a4693621c28bef8f',
    lfsToken: 'eyJhbGciOiJIUzI1NiJ9.eyJzdWJqZWN0IjoiMSIsInVzZXJOYW1lIjoidGVzdEAxNjMuY29tIiwicm9sZUxpc3QiOlsiQURNSU4iXSwidmFyaWFibGVNYXAiOnsicGVyc29uTmFtZSI6Iuezu-e7n-euoeeQhuWRmCIsImVudGVycHJpc2VDb2RlIjoiWFhYWC1Db21wYW55IiwiZW50ZXJwcmlzZUlkIjoiZDg2YTE4ZTZjNTJmZTcxOGE0NjkzNjIxYzI4YmVmOGYifSwic3ViIjoiMSIsImlhdCI6MTY0MjExODI1NCwiZXhwIjoxNjQyMTYxNDU0fQ.9n5EN7S9qEkZZ0Y1lQnuGm_ZF3eK9PftjZC6qKzbfdA'
  }
};
const passwordAgingTemplate = {
  message: 'success',
  code: 0,
  data: {
    status: 1
  }
};
const loginMock = {
  // 添加模板
  loginTemplate, passwordAgingTemplate
};
export default loginMock;
