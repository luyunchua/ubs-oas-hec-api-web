/**
 *create by yani ON 2022/2/7
 **/
const deptTemplate = {
 message: 'success',
 code: 0,
 data: [
  {
   id: '692e92669c0ca340eff4fdcef32896ee',
   parentId: null,
   children: [
    {
     id: '021e79316927f52a84b37aa6350c2cad',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: [
      {
       id: '80e24c88e9e14cdc0502ed6e42cd690d',
       parentId: '021e79316927f52a84b37aa6350c2cad',
       children: {
       },
       name: '1111111',
       status: '1',
       createTime: '2021-12-21T13:40:21.000+0000',
       code: '7777'
      }
     ],
     name: '国际局',
     status: '1',
     createTime: '2021-04-29T08:02:42.000+0000',
     code: '021e79316927f52a84b37aa6350c2cad'
    },
    {
     id: '04196cd5398038ac293694ce8876cd2b',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '机关党委',
     status: '1',
     createTime: null,
     code: '04196cd5398038ac293694ce8876cd2b'
    },
    {
     id: '06ff36a46be3804948daed5ffd88ecdb',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '企干一局',
     status: '1',
     createTime: null,
     code: '06ff36a46be3804948daed5ffd88ecdb'
    },
    {
     id: '1594eda3bd86772dce2e5ad172813fb0',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '改革办',
     status: '1',
     createTime: null,
     code: '1594eda3bd86772dce2e5ad172813fb0'
    },
    {
     id: '178aa764bf10459cdbaac1d4a5d900e9',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '信息中心',
     status: '1',
     createTime: null,
     code: '178aa764bf10459cdbaac1d4a5d900e9'
    },
    {
     id: '1e437c4b8b7706f56df10bc1f1e30d5b',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '巡视办',
     status: '1',
     createTime: null,
     code: '1e437c4b8b7706f56df10bc1f1e30d5b'
    },
    {
     id: '284beaf804aafdeee3e26ed21d0c8269',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '企干二局',
     status: '1',
     createTime: null,
     code: '284beaf804aafdeee3e26ed21d0c8269'
    },
    {
     id: '28c893cd9ab4865a07adddff782e8440',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '财务监管局',
     status: '1',
     createTime: null,
     code: '28c893cd9ab4865a07adddff782e8440'
    },
    {
     id: '4266ee573ff0ab2f54facdd00be88296',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '规划局',
     status: '1',
     createTime: null,
     code: '4266ee573ff0ab2f54facdd00be88296'
    },
    {
     id: '476fcdb697865aadce6448809726a671',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '研究局',
     status: '1',
     createTime: null,
     code: '476fcdb697865aadce6448809726a671'
    },
    {
     id: '4d9d9ace2e3cc7fa0f765b2cddb8a90c',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '监督追责局',
     status: '1',
     createTime: null,
     code: '4d9d9ace2e3cc7fa0f765b2cddb8a90c'
    },
    {
     id: '5a12c08800cd83f7aafbd4b8e199826e',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '法规局',
     status: '1',
     createTime: null,
     code: '5a12c08800cd83f7aafbd4b8e199826e'
    },
    {
     id: '5d946c387d7fea93862fe5c0de958ae8',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '监督一局',
     status: '1',
     createTime: null,
     code: '5d946c387d7fea93862fe5c0de958ae8'
    },
    {
     id: '60e9805215e221dc23f6bfb0cc3d80a4',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '新闻中心',
     status: '1',
     createTime: null,
     code: '60e9805215e221dc23f6bfb0cc3d80a4'
    },
    {
     id: '6e4a55e20c76176ff252e0732778e6d0',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '产权局',
     status: '1',
     createTime: null,
     code: '6e4a55e20c76176ff252e0732778e6d0'
    },
    {
     id: '88f7a3b66dfa22ffc3bceeb9e2c4cff2',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '监督三局',
     status: '1',
     createTime: null,
     code: '88f7a3b66dfa22ffc3bceeb9e2c4cff2'
    },
    {
     id: '9098d4d1a47e5ecd1c5967b2148699ac',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '监督二局',
     status: '1',
     createTime: null,
     code: '9098d4d1a47e5ecd1c5967b2148699ac'
    },
    {
     id: '981407cc752ca21b97c4a405f0982d76',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '财管运行局',
     status: '1',
     createTime: null,
     code: '981407cc752ca21b97c4a405f0982d76'
    },
    {
     id: '9e6a6dc35fc4244098a28cb0715af3a6',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '党建局',
     status: '1',
     createTime: null,
     code: '9e6a6dc35fc4244098a28cb0715af3a6'
    },
    {
     id: '9eca67225a088dcb2d1999b64575db20',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '考核分配局',
     status: '1',
     createTime: null,
     code: '9eca67225a088dcb2d1999b64575db20'
    },
    {
     id: 'a2f1e4f6cf6ed5609bee8745acabb9c8',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '管理局',
     status: '1',
     createTime: null,
     code: 'a2f1e4f6cf6ed5609bee8745acabb9c8'
    },
    {
     id: 'a7f20a9c23e53401fad12feb1a3f7ef5',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '国际合作局',
     status: '1',
     createTime: null,
     code: 'a7f20a9c23e53401fad12feb1a3f7ef5'
    },
    {
     id: 'ae036610df676c4946139bfa31da4b2d',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '干部教育培训中心',
     status: '1',
     createTime: null,
     code: 'ae036610df676c4946139bfa31da4b2d'
    },
    {
     id: 'b716fb538be0bd3b9a24354d99bb0e58',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '监督局',
     status: '1',
     createTime: null,
     code: 'b716fb538be0bd3b9a24354d99bb0e58'
    },
    {
     id: 'b7fe986190a181b2a390149d9101e22f',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '协会党建局',
     status: '1',
     createTime: null,
     code: 'b7fe986190a181b2a390149d9101e22f'
    },
    {
     id: 'b9b654c51f4ddab4079d57a70465c570',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '宣传局',
     status: '1',
     createTime: null,
     code: 'b9b654c51f4ddab4079d57a70465c570'
    },
    {
     id: 'ce972571a79fc9ac00ae10d32f2bc171',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '改革局',
     status: '1',
     createTime: null,
     code: 'ce972571a79fc9ac00ae10d32f2bc171'
    },
    {
     id: 'eb9eb118cbdf195fc201186e3d9cb1e8',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '资本局',
     status: '1',
     createTime: null,
     code: 'eb9eb118cbdf195fc201186e3d9cb1e8'
    },
    {
     id: 'ed952ba14365ad4c835de4f9991e005f',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '办公厅',
     status: '1',
     createTime: null,
     code: 'ed952ba14365ad4c835de4f9991e005f'
    },
    {
     id: 'faae13c28794180d942e7af98a98a4d1',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '科创局',
     status: '1',
     createTime: null,
     code: 'faae13c28794180d942e7af98a98a4d1'
    },
    {
     id: 'df51db48e17e77542e94106eeb6fe2ca',
     parentId: '692e92669c0ca340eff4fdcef32896ee',
     children: {
     },
     name: '提供方内部部门',
     status: '1',
     createTime: '2021-04-29T08:02:42.000+0000',
     code: '20210429nei'
    }
   ],
   name: '委机关',
   status: '1',
   createTime: null,
   code: '692e92669c0ca340eff4fdcef32896ee'
  }
 ]
};
const deptMock = {
 // 添加模板
 deptTemplate
};
export default deptMock;
