/**
 *create by yani ON 2022/2/7
 **/
const configurationTemplate = {
 message: 'success',
 code: 0,
 data: {
  total: 4,
  data: [
   {
    id: '1',
    createTime: null,
    updateTime: '2021-02-02T08:28:48.000+0000',
    configKey: 'password_expiration',
    configValue: 1215752191,
    note: '密码过期时间(单位:分钟)',
    createId: null,
    updateId: null,
    sort: 0
   },
   {
    id: '2',
    createTime: null,
    updateTime: '2021-02-08T07:19:01.000+0000',
    configKey: 'password_wrong_count',
    configValue: 6,
    note: '密码错误次数',
    createId: null,
    updateId: null,
    sort: 1
   },
   {
    id: '3',
    createTime: null,
    updateTime: '2021-02-02T06:27:28.000+0000',
    configKey: 'user_lock_time',
    configValue: 2,
    note: '账号锁定时间(单位:分钟)',
    createId: null,
    updateId: null,
    sort: 2
   },
   {
    id: '4',
    createTime: '2021-11-26T01:56:16.000+0000',
    updateTime: null,
    configKey: 'login_timeout',
    configValue: 20,
    note: '超时登录时间(单位:分钟)',
    createId: '1',
    updateId: null,
    sort: 3
   }
  ]
 }
};
const configurationMock = {
 // 添加模板
 configurationTemplate
};
export default configurationMock;
