/**
 *create by yani ON 2022/2/8
 **/
const roleTemplate = {
 message: 'success',
 code: 0,
 data: {
  total: 5,
  data: [
   {
    id: '152a90e96a61bb48614a8a2a7c527028',
    createTime: '2021-04-27T06:41:09.000+0000',
    updateTime: null,
    code: 'anquan',
    name: '安全管理员',
    status: 1,
    createId: '1',
    updateId: null
   },
   {
    id: '8ea8a30054b0e6d629e8acb8aedf0df3',
    createTime: '2021-04-27T06:41:22.000+0000',
    updateTime: '2021-04-27T07:30:09.000+0000',
    code: 'sj',
    name: '审计管理员',
    status: 1,
    createId: '1',
    updateId: '1'
   },
   {
    id: 'acea3af77fb9db589d7ddc18b5c8205f',
    createTime: '2021-04-27T07:27:15.000+0000',
    updateTime: null,
    code: 'cs01',
    name: '测试员',
    status: 1,
    createId: '1',
    updateId: null
   },
   {
    id: 'b50597e51849865eb4185391b2bc2984',
    createTime: '2020-09-17T08:02:39.000+0000',
    updateTime: null,
    code: 'ADMIN',
    name: '系统管理员',
    status: 1,
    createId: '1',
    updateId: null
   },
   {
    id: 'd4a3f026aaf48e2b70efff4273d0cc30',
    createTime: '2021-04-27T07:26:59.000+0000',
    updateTime: '2021-04-27T08:30:24.000+0000',
    code: 'xt',
    name: '流量网关',
    status: 1,
    createId: '1',
    updateId: '1'
   }
  ]
 }
};
const roleMock = {
 // 添加模板
 roleTemplate
};
export default roleMock;
