/**
 *create by yani ON 2022/2/7
 *
 **/
const apiManageTemplate = {
 message: 'success',
 code: 0,
 data: [
  {
   children: [
    {
     code: 'dsep-portal',
     id: 'c41976fa1c32fc9c83ea1ec6d28e1708',
     name: '门户全接口',
     parentId: '5999e281f264ff008e9827d6215cf3f7',
     status: 1,
     type: 1,
     url: '/dsep-portal*/*/**'
    }
   ],
   code: '05',
   id: '5999e281f264ff008e9827d6215cf3f7',
   name: '门户首页',
   parentId: null,
   status: 1,
   type: 3,
   url: null
  }
 ]
};
const apiManageMock = {
 // 添加模板
 apiManageTemplate
};
export default apiManageMock;
