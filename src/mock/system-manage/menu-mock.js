/**
 *create by Jancheng ON 2022/1/14
 *
 **/
const menuTemplate = {
  message: 'success',
  code: 0,
  data: [
    {
      id: '6e992f075d64b5d3abb0d14a14a5278e',
      parentId: null,
      children: [],
      name: '首页',
      code: 'tripleAndLarge',
      type: 2,
      status: 1,
      indexNum: 8,
      menuGroup: '0',
      url: '/',
      icon: 'tab',
      subType: 0
    },
    {
      id: '7a4b19fc21bfae3ecb9fba355098482e',
      parentId: null,
      children: [],
      name: '境外组织机构',
      code: 'centralOrganization',
      type: 2,
      status: 1,
      indexNum: 2,
      menuGroup: '0',
      url: '/centralOrganization',
      icon: 'dept',
      subType: 0
    },
    {
      id: '7a4b19fc21bfae3ecb9fba355098482e',
      parentId: null,
      children: [],
      name: '境外三重一大',
      code: 'tripleAndLarge',
      type: 2,
      status: 1,
      indexNum: 3,
      menuGroup: '0',
      url: '/tripleAndLarge',
      icon: 'tab',
      subType: 0
    },
    {
      id: '38e607d3fb647bffa4a824afce009b3b',
      parentId: null,
      children: [],
      name: '境外大额资金',
      code: 'largeFunds',
      type: 2,
      status: 1,
      indexNum: 4,
      menuGroup: '0',
      url: '/largeFunds',
      icon: 'image',
      subType: 0
    },
    {
      id: '4a8ad21183b25599348bc74d0114d12f',
      parentId: null,
      children: [
        {
          id: '6aad9e0739e57b40441db2defb8c8a21',
          parentId: '4a8ad21183b25599348bc74d0114d12f',
          children: [],
          name: '油价日报',
          code: 'blank',
          type: 2,
          status: 1,
          indexNum: 40,
          menuGroup: '0',
          url: 'blank',
          icon: 'tools',
          subType: 0
        },
        {
          id: '6aad9e0739e57b40441db2defb8c8a22',
          parentId: '4a8ad21183b25599348bc74d0114d12f',
          children: [],
          name: '风控日报',
          code: 'blank',
          type: 2,
          status: 1,
          indexNum: 41,
          menuGroup: '0',
          url: 'riskDaily',
          icon: 'tools',
          subType: 0
        },
        {
          id: '6aad9e0739e57b40441db2defb8c8a29',
          parentId: '4a8ad21183b25599348bc74d0114d12f',
          children: [],
          name: '统计报表',
          code: 'blank',
          type: 2,
          status: 1,
          indexNum: 42,
          menuGroup: '0',
          url: 'blank',
          icon: 'tools',
          subType: 0
        },
        {
          id: '6aad9e0739e57b40441db2defb8c8a28',
          parentId: '4a8ad21183b25599348bc74d0114d12f',
          children: [],
          name: '电子报表',
          code: 'electronicFilling',
          type: 2,
          status: 1,
          indexNum: 43,
          menuGroup: '0',
          url: 'electronicFilling',
          icon: 'tools',
          subType: 0
        },
      ],
      name: '金融衍生业务',
      code: 'supervisionAndAccoun',
      type: 2,
      status: 1,
      indexNum: 4,
      menuGroup: '0',
      url: '/jrys',
      icon: 'people',
      subType: 0
    },
    {
      id: '5e139fa18fc2d9e90ceadb0c2e99f72e',
      parentId: null,
      children: [],
      name: '境外舆情',
      code: 'publicOpinion',
      type: 2,
      status: 1,
      indexNum: 9,
      menuGroup: '0',
      url: '/publicOpinion',
      icon: 'date',
      subType: 0
    },
    {
      id: '08e61eb35fc26043c2550cea08ad9268',
      parentId: null,
      children: [
        {
          id: '7bb82b325bcc6ddce0546209826241c4',
          parentId: '08e61eb35fc26043c2550cea08ad9268',
          children: [
            {
              id: 'b0124c172f9a2fc4dc70b03acd09670c',
              parentId: '7bb82b325bcc6ddce0546209826241c4',
              children: [
                {
                  id: '065e3290fcd58e00668862e5fd828180',
                  parentId: 'b0124c172f9a2fc4dc70b03acd09670c',
                  children: [],
                  name: '用户管理',
                  code: '02010101',
                  type: 2,
                  status: 1,
                  indexNum: 2010101,
                  menuGroup: '0',
                  url: 'user',
                  icon: null,
                  subType: 0
                },
                {
                  id: '89f7d472d09d08a756b9237e54870347',
                  parentId: 'b0124c172f9a2fc4dc70b03acd09670c',
                  children: [],
                  name: '角色管理',
                  code: '02010102',
                  type: 2,
                  status: 1,
                  indexNum: 2010102,
                  menuGroup: '0',
                  url: 'role',
                  icon: null,
                  subType: 0
                },
                {
                  id: '51b24cedfa19c560973025a4c7944ee3',
                  parentId: 'b0124c172f9a2fc4dc70b03acd09670c',
                  children: [],
                  name: '组织机构管理',
                  code: '02010103',
                  type: 2,
                  status: 1,
                  indexNum: 2010103,
                  menuGroup: '0',
                  url: 'dept',
                  icon: null,
                  subType: 0
                }
              ],
              name: '账号关系',
              code: '020101',
              type: 2,
              status: 1,
              indexNum: 20101,
              menuGroup: '0',
              url: 'account',
              icon: null,
              subType: 0
            },
            {
              id: '6ea3c34b50f8d37e16395d174355abf8',
              parentId: '7bb82b325bcc6ddce0546209826241c4',
              children: [
                {
                  id: '702ba8aa361c95d52ef32c8fd060b42b',
                  parentId: '6ea3c34b50f8d37e16395d174355abf8',
                  children: [],
                  name: '菜单管理',
                  code: '02010201',
                  type: 2,
                  status: 1,
                  indexNum: 2010201,
                  menuGroup: '0',
                  url: 'menu',
                  icon: null,
                  subType: 0
                },
                {
                  id: 'a3f34dc1ac7b71b82ffec59ac211037e',
                  parentId: '6ea3c34b50f8d37e16395d174355abf8',
                  children: [],
                  name: '系统接口管理',
                  code: '02010202',
                  type: 2,
                  status: 1,
                  indexNum: 2010202,
                  menuGroup: '0',
                  url: 'apiManage',
                  icon: null,
                  subType: 0
                }
              ],
              name: '功能权限',
              code: '020102',
              type: 2,
              status: 1,
              indexNum: 20102,
              menuGroup: '0',
              url: 'functionAuth',
              icon: null,
              subType: 0
            },
            {
              id: 'ef7247c36e9ca621495e42f01bb518a6',
              parentId: '7bb82b325bcc6ddce0546209826241c4',
              children: [
                {
                  id: '3fe888abe33558a69dac697932fd72ed',
                  parentId: 'ef7247c36e9ca621495e42f01bb518a6',
                  children: [],
                  name: '权限模型',
                  code: '02010301',
                  type: 2,
                  status: 1,
                  indexNum: 2010301,
                  menuGroup: '0',
                  url: 'dataModelManage',
                  icon: null,
                  subType: 0
                },
                {
                  id: '74858792f81afbf8a134bdecbfab712d',
                  parentId: 'ef7247c36e9ca621495e42f01bb518a6',
                  children: [],
                  name: '权限规则',
                  code: '02010302',
                  type: 2,
                  status: 1,
                  indexNum: 2010302,
                  menuGroup: '0',
                  url: 'dataRuleManage',
                  icon: null,
                  subType: 0
                }
              ],
              name: '数据权限',
              code: '020103',
              type: 2,
              status: 1,
              indexNum: 20103,
              menuGroup: '0',
              url: 'dataAuth',
              icon: null,
              subType: 0
            }
          ],
          name: '权限管理',
          code: '0201',
          type: 2,
          status: 1,
          indexNum: 201,
          menuGroup: '0',
          url: 'authManage',
          icon: 'Steve-Jobs',
          subType: 0
        },
        {
          id: 'c1bc23edc3110b38dbeb4435421eceef',
          parentId: '08e61eb35fc26043c2550cea08ad9268',
          children: [],
          name: '日志监控',
          code: '0203',
          type: 2,
          status: 1,
          indexNum: 203,
          menuGroup: '0',
          url: 'logManage',
          icon: null,
          subType: 0
        },
        {
          id: '72ac233beb5c8cc8f9ac1dc977e65dd3',
          parentId: '08e61eb35fc26043c2550cea08ad9268',
          children: [],
          name: '字典管理',
          code: '0204',
          type: 2,
          status: 1,
          indexNum: 204,
          menuGroup: '0',
          url: 'dict',
          icon: null,
          subType: 0
        },
        {
          id: '9d16d289e616808b71494151c487c693',
          parentId: '08e61eb35fc26043c2550cea08ad9268',
          children: [],
          name: '数据抓取工具',
          code: '0205',
          type: 2,
          status: 1,
          indexNum: 205,
          menuGroup: '0',
          url: 'http://113.207.43.93:38111/lfs-core/pull/batchUpdate',
          icon: null,
          subType: 2
        }
      ],
      name: '系统管理',
      code: '02',
      type: 2,
      status: 1,
      indexNum: 9999,
      menuGroup: '0',
      url: '/systemManage',
      icon: 'tab',
      subType: 0
    }
  ]
  //  data: [
  //   {
  //    id: '073030392219d7143cdcfb9347e05531',
  //    parentId: null,
  //    children: [],
  //    name: '首页',
  //    code: '01',
  //    type: 2,
  //    status: 1,
  //    indexNum: 1,
  //    menuGroup: '0',
  //    url: '/',
  //    icon: 'app',
  //    subType: 0
  //   },
  //   {
  //    id: '08e61eb35fc26043c2550cea08ad9268',
  //    parentId: null,
  //    children: [
  //     {
  //      id: '7bb82b325bcc6ddce0546209826241c4',
  //      parentId: '08e61eb35fc26043c2550cea08ad9268',
  //      children: [
  //       {
  //        id: 'b0124c172f9a2fc4dc70b03acd09670c',
  //        parentId: '7bb82b325bcc6ddce0546209826241c4',
  //        children: [
  //         {
  //          id: '065e3290fcd58e00668862e5fd828180',
  //          parentId: 'b0124c172f9a2fc4dc70b03acd09670c',
  //          children: [],
  //          name: '用户管理',
  //          code: '02010101',
  //          type: 2,
  //          status: 1,
  //          indexNum: 2010101,
  //          menuGroup: '0',
  //          url: 'user',
  //          icon: null,
  //          subType: 0
  //         },
  //         {
  //          id: '89f7d472d09d08a756b9237e54870347',
  //          parentId: 'b0124c172f9a2fc4dc70b03acd09670c',
  //          children: [],
  //          name: '角色管理',
  //          code: '02010102',
  //          type: 2,
  //          status: 1,
  //          indexNum: 2010102,
  //          menuGroup: '0',
  //          url: 'role',
  //          icon: null,
  //          subType: 0
  //         },
  //         {
  //          id: '51b24cedfa19c560973025a4c7944ee3',
  //          parentId: 'b0124c172f9a2fc4dc70b03acd09670c',
  //          children: [],
  //          name: '组织机构管理',
  //          code: '02010103',
  //          type: 2,
  //          status: 1,
  //          indexNum: 2010103,
  //          menuGroup: '0',
  //          url: 'dept',
  //          icon: null,
  //          subType: 0
  //         }
  //        ],
  //        name: '账号关系',
  //        code: '020101',
  //        type: 2,
  //        status: 1,
  //        indexNum: 20101,
  //        menuGroup: '0',
  //        url: 'account',
  //        icon: null,
  //        subType: 0
  //       },
  //       {
  //        id: '6ea3c34b50f8d37e16395d174355abf8',
  //        parentId: '7bb82b325bcc6ddce0546209826241c4',
  //        children: [
  //         {
  //          id: '702ba8aa361c95d52ef32c8fd060b42b',
  //          parentId: '6ea3c34b50f8d37e16395d174355abf8',
  //          children: [],
  //          name: '菜单管理',
  //          code: '02010201',
  //          type: 2,
  //          status: 1,
  //          indexNum: 2010201,
  //          menuGroup: '0',
  //          url: 'menu',
  //          icon: null,
  //          subType: 0
  //         },
  //         {
  //          id: 'a3f34dc1ac7b71b82ffec59ac211037e',
  //          parentId: '6ea3c34b50f8d37e16395d174355abf8',
  //          children: [],
  //          name: '系统接口管理',
  //          code: '02010202',
  //          type: 2,
  //          status: 1,
  //          indexNum: 2010202,
  //          menuGroup: '0',
  //          url: 'apiManage',
  //          icon: null,
  //          subType: 0
  //         }
  //        ],
  //        name: '功能权限',
  //        code: '020102',
  //        type: 2,
  //        status: 1,
  //        indexNum: 20102,
  //        menuGroup: '0',
  //        url: 'functionAuth',
  //        icon: null,
  //        subType: 0
  //       },
  //       {
  //        id: 'ef7247c36e9ca621495e42f01bb518a6',
  //        parentId: '7bb82b325bcc6ddce0546209826241c4',
  //        children: [
  //         {
  //          id: '3fe888abe33558a69dac697932fd72ed',
  //          parentId: 'ef7247c36e9ca621495e42f01bb518a6',
  //          children: [],
  //          name: '权限模型',
  //          code: '02010301',
  //          type: 2,
  //          status: 1,
  //          indexNum: 2010301,
  //          menuGroup: '0',
  //          url: 'dataModelManage',
  //          icon: null,
  //          subType: 0
  //         },
  //         {
  //          id: '74858792f81afbf8a134bdecbfab712d',
  //          parentId: 'ef7247c36e9ca621495e42f01bb518a6',
  //          children: [],
  //          name: '权限规则',
  //          code: '02010302',
  //          type: 2,
  //          status: 1,
  //          indexNum: 2010302,
  //          menuGroup: '0',
  //          url: 'dataRuleManage',
  //          icon: null,
  //          subType: 0
  //         }
  //        ],
  //        name: '数据权限',
  //        code: '020103',
  //        type: 2,
  //        status: 1,
  //        indexNum: 20103,
  //        menuGroup: '0',
  //        url: 'dataAuth',
  //        icon: null,
  //        subType: 0
  //       }
  //      ],
  //      name: '权限管理',
  //      code: '0201',
  //      type: 2,
  //      status: 1,
  //      indexNum: 201,
  //      menuGroup: '0',
  //      url: 'authManage',
  //      icon: '',
  //      subType: 0
  //     },
  //     {
  //      id: 'a5607c677532d614706a2f7cdb39ab98',
  //      parentId: '08e61eb35fc26043c2550cea08ad9268',
  //      children: [
  //       {
  //        id: '49f42f37bac5134192f9acf67310edc7',
  //        parentId: 'a5607c677532d614706a2f7cdb39ab98',
  //        children: [],
  //        name: '流程分类',
  //        code: '020201',
  //        type: 2,
  //        status: 1,
  //        indexNum: 20201,
  //        menuGroup: '0',
  //        url: 'processClassify',
  //        icon: null,
  //        subType: 0
  //       },
  //       {
  //        id: '0a39615d0732eed149633156a5f23b5b',
  //        parentId: 'a5607c677532d614706a2f7cdb39ab98',
  //        children: [],
  //        name: '流程绘制',
  //        code: '020202',
  //        type: 2,
  //        status: 1,
  //        indexNum: 20202,
  //        menuGroup: '0',
  //        url: 'processDraw',
  //        icon: null,
  //        subType: 0
  //       },
  //       {
  //        id: '42514eb414ea36d1284a4cf0e6ee0e26',
  //        parentId: 'a5607c677532d614706a2f7cdb39ab98',
  //        children: [],
  //        name: '流程部署',
  //        code: '020203',
  //        type: 2,
  //        status: 1,
  //        indexNum: 20203,
  //        menuGroup: '0',
  //        url: 'processDeploy',
  //        icon: null,
  //        subType: 0
  //       },
  //       {
  //        id: '6914ae2b8180797126cc480252dec892',
  //        parentId: 'a5607c677532d614706a2f7cdb39ab98',
  //        children: [],
  //        name: '流程配置',
  //        code: '020204',
  //        type: 2,
  //        status: 1,
  //        indexNum: 20204,
  //        menuGroup: '0',
  //        url: 'processSet',
  //        icon: null,
  //        subType: 0
  //       }
  //      ],
  //      name: '流程管理',
  //      code: '0202',
  //      type: 2,
  //      status: 1,
  //      indexNum: 202,
  //      menuGroup: '0',
  //      url: 'process',
  //      icon: '',
  //      subType: 0
  //     },
  //     {
  //      id: 'c1bc23edc3110b38dbeb4435421eceef',
  //      parentId: '08e61eb35fc26043c2550cea08ad9268',
  //      children: [],
  //      name: '日志监控',
  //      code: '0203',
  //      type: 2,
  //      status: 1,
  //      indexNum: 203,
  //      menuGroup: '0',
  //      url: 'logManage',
  //      icon: null,
  //      subType: 0
  //     },
  //     {
  //      id: '72ac233beb5c8cc8f9ac1dc977e65dd3',
  //      parentId: '08e61eb35fc26043c2550cea08ad9268',
  //      children: [],
  //      name: '字典管理',
  //      code: '0204',
  //      type: 2,
  //      status: 1,
  //      indexNum: 204,
  //      menuGroup: '0',
  //      url: 'dict',
  //      icon: null,
  //      subType: 0
  //     },
  //     {
  //      id: 'ab6dedf7b276ef45bb47aa0e8818f83e',
  //      parentId: '08e61eb35fc26043c2550cea08ad9268',
  //      children: [],
  //      name: '系统配置',
  //      code: '0205',
  //      type: 2,
  //      status: 1,
  //      indexNum: 205,
  //      menuGroup: '0',
  //      url: 'configuration',
  //      icon: null,
  //      subType: 0
  //     }
  //    ],
  //    name: '系统管理',
  //    code: '02',
  //    type: 2,
  //    status: 1,
  //    indexNum: 9999,
  //    menuGroup: '0',
  //    url: '/systemManage',
  //    icon: 'tab',
  //    subType: 0
  //   },
  //   {
  //    id: '4282bc26e24ff16a40e5c2570059f140',
  //    parentId: null,
  //    children: [],
  //    name: '远程连接QDP',
  //    code: 'BAI-DU',
  //    type: 2,
  //    status: 1,
  //    indexNum: 100000,
  //    menuGroup: '0',
  //    url: 'http://10.88.0.4:8888/',
  //    icon: 'web',
  //    subType: 2
  //   }
  //  ]
};
const menuMock = {
  // 添加模板
  menuTemplate
};
export default menuMock;
