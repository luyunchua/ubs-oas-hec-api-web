/**
 *create by yani ON 2022/2/7
 **/
const dictTemplate = {
 message: 'success',
 code: 0,
 data: {
  total: 5,
  data: [
   {
    id: 'fa27b29a9dc71da69c53467065092610',
    createTime: '2022-01-11T07:03:16.000+0000',
    updateTime: '2022-01-11T07:03:16.000+0000',
    code: 'State ',
    name: '启用状态 ',
    type: 'State',
    description: '启用状态 ',
    status: null,
    createId: '1',
    updateId: '1',
    remark: '启用状态 '
   },
   {
    id: 'fa27b29a9dc71da69c53467065092611',
    createTime: '2022-01-11T07:03:16.000+0000',
    updateTime: '2022-01-11T07:03:16.000+0000',
    code: 'ThemeClass ',
    name: '视角 ',
    type: 'ThemeClass',
    description: '视角 ',
    status: null,
    createId: '1',
    updateId: '1',
    remark: '视角 '
   },
   {
    id: 'fa27b29a9dc71da69c53467065092612',
    createTime: '2022-01-11T07:03:16.000+0000',
    updateTime: '2022-01-11T07:03:16.000+0000',
    code: 'MetaClass',
    name: '元数据类型',
    type: 'MetaClass',
    description: '元数据类型',
    status: null,
    createId: '1',
    updateId: '1',
    remark: '元数据类型'
   },
   {
    id: 'fa27b29a9dc71da69c53467065092613',
    createTime: '2022-01-11T07:03:16.000+0000',
    updateTime: '2022-01-11T07:03:16.000+0000',
    code: 'IndicatorType',
    name: '指标项目',
    type: 'IndicatorType',
    description: '指标项目',
    status: null,
    createId: '1',
    updateId: '1',
    remark: '指标项目'
   },
   {
    id: 'ddf48202a256250a2949dfafd4f6ec43',
    createTime: '2021-10-31T05:39:43.000+0000',
    updateTime: '2021-10-31T05:39:58.000+0000',
    code: null,
    name: null,
    type: 'isSend_type',
    description: '企业指标管理-状态',
    status: null,
    createId: '1',
    updateId: '1',
    remark: null
   }
  ]
 }
};
const dictMock = {
 // 添加模板
 dictTemplate
};
export default dictMock;
