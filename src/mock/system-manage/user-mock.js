/**
 * create by yani on 2022/02/08
 */
const userTemplate = {
 message: 'success',
 code: 0,
 data: {
  total: 5,
  data: [
   {
    code: null,
    gender: '1',
    mail: '32323@qq.com',
    organizationName: '新疆维吾尔自治区人民政府国有资产监国资委',
    mobile: '15876540889',
    oid: 'f0b4c1bde16d7f5a2d9d7ae69508e291',
    initPassword: 'mrhdum',
    roleList: {
    },
    personName: '夏雨荷',
    createTime: '2021-07-06T06:25:24.000+0000',
    name: '32323',
    creater: 'admin',
    id: '89a3c433b12253215e6fbb3a73d0dd11',
    status: 1
   },
   {
    code: '001',
    gender: '0',
    mail: '934029568@qq.com',
    organizationName: '新疆交通投资（集团）有限责任公司',
    mobile: '17529786788',
    oid: 'b5fba776429669e84420c04eaa3e5080',
    initPassword: 't4w3qs',
    roleList: [
     {
      id: 'acea3af77fb9db589d7ddc18b5c8205f',
      createTime: '2021-04-27T07:27:15.000+0000',
      updateTime: null,
      code: 'cs01',
      name: '测试员',
      status: 1,
      createId: null,
      updateId: null
     }
    ],
    personName: '夏明',
    createTime: '2021-07-06T06:22:02.000+0000',
    name: 'caicaiwo',
    creater: 'admin',
    id: '1cacba5e9f9f89ba9675b1a7b03ce882',
    status: 1
   },
   {
    code: '1',
    gender: '0',
    mail: '18912312312@123.com',
    organizationName: '新疆交通投资（集团）有限责任公司',
    mobile: '18912312312',
    oid: 'b5fba776429669e84420c04eaa3e5080',
    initPassword: 'wud0um',
    roleList: [
     {
      id: '8ea8a30054b0e6d629e8acb8aedf0df3',
      createTime: '2021-04-27T06:41:22.000+0000',
      updateTime: '2021-04-27T07:30:09.000+0000',
      code: 'sj',
      name: '审计管理员',
      status: 1,
      createId: null,
      updateId: null
     }
    ],
    personName: '郝建',
    createTime: '2021-04-27T08:27:11.000+0000',
    name: 'admin002',
    creater: 'admin',
    id: '6c9453db6ce5f388a943cec15d775aa4',
    status: 1
   },
   {
    code: null,
    gender: null,
    mail: '17812312312@123.com',
    organizationName: '新疆地矿投资（集团）有限责任公司',
    mobile: '17812312312',
    oid: 'b2ac60ca32b0f784b5efde9ef87b63c8',
    initPassword: 'r9yw6s',
    roleList: [
     {
      id: 'acea3af77fb9db589d7ddc18b5c8205f',
      createTime: '2021-04-27T07:27:15.000+0000',
      updateTime: null,
      code: 'cs01',
      name: '测试员',
      status: 1,
      createId: null,
      updateId: null
     }
    ],
    personName: '郝仁',
    createTime: '2021-04-27T08:26:42.000+0000',
    name: 'admin001',
    creater: 'admin',
    id: '83995f1cd3f5360e4832ac382111b281',
    status: 1
   },
   {
    code: '0021',
    gender: '0',
    mail: 'test@163.com',
    organizationName: null,
    mobile: '13222222222',
    oid: null,
    initPassword: '123456',
    roleList: [
     {
      id: 'b50597e51849865eb4185391b2bc2984',
      createTime: '2020-09-17T08:02:39.000+0000',
      updateTime: null,
      code: 'ADMIN',
      name: '系统管理员',
      status: 1,
      createId: null,
      updateId: null
     }
    ],
    personName: '系统管理员',
    createTime: '2020-09-17T08:03:42.000+0000',
    name: 'admin',
    creater: 'admin',
    id: '1',
    status: 1
   }
  ]
 }
};
const userMock = {
 // add template
 userTemplate
};
export default userMock;
