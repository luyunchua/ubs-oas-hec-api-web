/**
 *create by yani ON 2022/2/7
 **/
const logManageTemplate = {
 message: 'success',
 code: 0,
 data: {
  total: 17981,
  data: [
   {
    id: '543e7548-c23c-4938-ba55-578e6a91260c',
    ip: '119.84.70.237',
    name: 'eqddddeqe',
    roleName: 'eqeqe',
    createTime: '2021-05-08 13:54:38',
    remark: '获取权限分配菜单选择树',
    resource: '0'
   },
   {
    id: '6e7f7eb1-8875-4330-9b08-96b36efd2abd',
    ip: '119.84.70.237',
    name: null,
    roleName: null,
    createTime: '2021-05-08 13:54:38',
    remark: '获取权限分配菜单选择树',
    resource: '0'
   },
   {
    id: '6aec75b5-f943-4b1c-92c6-e2031bd7b48a',
    ip: '119.84.70.237',
    name: null,
    roleName: null,
    createTime: '2021-05-08 13:51:08',
    remark: '获取权限分配菜单选择树',
    resource: '0'
   },
   {
    id: '62a009b3-52ec-4893-b2e6-c6ddba6aded8',
    ip: '119.84.70.237',
    name: null,
    roleName: null,
    createTime: '2021-05-08 13:51:08',
    remark: '获取权限分配菜单选择树',
    resource: '0'
   },
   {
    id: 'd20663ce-51a4-40b3-9e72-0a8cda35d081',
    ip: '119.84.70.237',
    name: null,
    roleName: null,
    createTime: '2021-05-08 13:51:08',
    remark: '分页获取有效组织机构树',
    resource: '0'
   },
   {
    id: '39f8e5d6-dc15-4b48-b111-3eae8dc184a2',
    ip: '119.84.70.237',
    name: null,
    roleName: null,
    createTime: '2021-05-08 13:51:08',
    remark: '分页获取有效组织机构树',
    resource: '0'
   },
   {
    id: '1bf7a017-a8be-42ef-b17f-cdf4355f8397',
    ip: '119.84.70.237',
    name: null,
    roleName: null,
    createTime: '2021-05-08 13:51:08',
    remark: '获取角色下拉框',
    resource: '0'
   },
   {
    id: '4d0da962-683a-429e-964d-1e53ec23d5d3',
    ip: '119.84.70.237',
    name: null,
    roleName: null,
    createTime: '2021-05-08 13:51:08',
    remark: '分页获取用户角色列表',
    resource: '0'
   },
   {
    id: '1d603358-b1ba-4d56-9192-6e8c382fa9dc',
    ip: '119.84.70.237',
    name: null,
    roleName: null,
    createTime: '2021-05-08 13:51:08',
    remark: '获取权限分配菜单选择树',
    resource: '0'
   },
   {
    id: '9c705374-8897-4738-a5ff-2d824da046fa',
    ip: '119.84.70.237',
    name: null,
    roleName: null,
    createTime: '2021-05-08 13:50:58',
    remark: '获取权限分配菜单选择树',
    resource: '0'
   }
  ]
 }
};
const logManageMock = {
 // 添加模板
 logManageTemplate
};
export default logManageMock;
