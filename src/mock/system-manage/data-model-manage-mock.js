/**
 *create by yani ON 2022/2/7
 **/
const dataModelManageTemplate = {
 message: 'success',
 code: 0,
 data: {
  total: 8,
  data: [
   {
    id: 'd8ddca9090904a93eb11149b4eefb31a',
    createTime: '2021-11-09T03:30:19.314+0000',
    updateTime: null,
    code: 'app_admin',
    name: '应用管理',
    status: 1,
    remark: null
   },
   {
    id: 'd062bb06899affada52d294a964ecf3c',
    createTime: '2021-10-13T01:00:20.234+0000',
    updateTime: null,
    code: 'app_edit_auth',
    name: '应用管理-修改记录',
    status: 1,
    remark: null
   },
   {
    id: 'f627e81ef666678d9a2a49a698923431',
    createTime: '2021-09-11T06:49:31.465+0000',
    updateTime: '2021-09-11T06:49:33.854+0000',
    code: 'qwq',
    name: '11',
    status: 0,
    remark: null
   },
   {
    id: '1d06b990dc1492824f61c72116c6d644',
    createTime: '2021-01-12T06:25:01.559+0000',
    updateTime: '2021-09-11T06:49:34.396+0000',
    code: '0123',
    name: '1234',
    status: 0,
    remark: ''
   },
   {
    id: '6e3dad2a4e99481cbcfb5eb5c924fdd8',
    createTime: '2020-12-16T09:34:41.554+0000',
    updateTime: '2021-09-11T06:49:34.890+0000',
    code: 'adsf',
    name: 'test_g',
    status: 0,
    remark: 'adsf'
   },
   {
    id: 'afd37cc7aef23e347b7141f44d1b14e8',
    createTime: '2020-12-16T09:34:21.248+0000',
    updateTime: '2021-09-11T06:49:35.712+0000',
    code: 'adssf',
    name: 'test_g',
    status: 0,
    remark: 'asd'
   },
   {
    id: '3964ff34032d67b349bd8af19ba4932e',
    createTime: '2020-12-16T09:28:10.159+0000',
    updateTime: '2021-09-11T06:49:35.987+0000',
    code: 'asdf',
    name: 'test_g',
    status: 0,
    remark: 'asdf'
   },
   {
    id: '36352cce7986108dc27500830a59d74a',
    createTime: '2020-12-16T03:13:36.282+0000',
    updateTime: '2021-09-11T06:49:36.708+0000',
    code: '12342',
    name: '1234',
    status: 0,
    remark: '1234asdfgh'
   }
  ]
 }
};
const dataModelManageMock = {
 // 添加模板
 dataModelManageTemplate
};
export default dataModelManageMock;
