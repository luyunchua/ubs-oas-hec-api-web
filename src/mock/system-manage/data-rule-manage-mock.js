/**
 *create by yani ON 2022/2/7
 **/
const dataRuleManageTemplate = {
 message: 'success',
 code: 0,
 data: {
  total: 2,
  data: [
   {
    modelName: '应用管理-修改记录',
    // eslint-disable-next-line no-template-curly-in-string
    rule: "saia.create_id='${userId}'",
    modelId: 'd062bb06899affada52d294a964ecf3c',
    remark: null,
    status: 1,
    createId: null,
    updateId: null,
    id: '042ff8ebaac34456376b6edefcb7ed91',
    createTime: '2021-10-13T01:02:06.000+0000',
    updateTime: '2022-01-06T05:56:53.000+0000',
    roleList: [
     {
      id: '0c89f6e843b466bf863e10a4fef43932',
      createTime: '2020-12-01T09:51:20.000+0000',
      updateTime: '2021-09-26T08:04:51.000+0000',
      code: 'normal',
      name: '普通用户',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: '0445a1f893f792bc40cda425ee459c2c',
      createTime: '2020-12-01T08:29:50.000+0000',
      updateTime: '2021-04-15T07:50:44.000+0000',
      code: 'secadmin',
      name: '安全保密员',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: '0c89f6e843b466bf863e10a4fef43982',
      createTime: '2020-12-01T09:51:20.000+0000',
      updateTime: '2021-09-26T08:04:51.000+0000',
      code: 'auditadmin',
      name: '安全审计员',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: 'b3d3b8326809b333b2e92ba3e6f6cb1b',
      createTime: '2021-11-09T02:46:57.000+0000',
      updateTime: '2021-11-22T07:39:42.000+0000',
      code: 'tjzer',
      name: '厅局负责人',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: '4989607e9312d02c51fb4560f674e896',
      createTime: '2021-11-09T02:46:40.000+0000',
      updateTime: '2021-11-21T02:34:37.000+0000',
      code: 'cz',
      name: '处长',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: '0c89f6e843b466bf863e10a4fef43942',
      createTime: '2020-12-01T09:51:20.000+0000',
      updateTime: '2021-09-26T08:04:51.000+0000',
      code: 'sjgly',
      name: '数据管理员',
      status: 1,
      createId: null,
      updateId: null
     }
    ]
   },
   {
    modelName: '应用管理',
    // eslint-disable-next-line no-template-curly-in-string
    rule: "sabi.create_id='${userId}'",
    modelId: 'd8ddca9090904a93eb11149b4eefb31a',
    remark: null,
    status: 1,
    createId: null,
    updateId: null,
    id: '90b86bd950231371a196220514273c41',
    createTime: '2021-11-09T03:39:00.000+0000',
    updateTime: '2021-11-21T08:27:20.000+0000',
    roleList: [
     {
      id: '0c89f6e843b466bf863e10a4fef43982',
      createTime: '2020-12-01T09:51:20.000+0000',
      updateTime: '2021-09-26T08:04:51.000+0000',
      code: 'auditadmin',
      name: '安全审计员',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: 'b3d3b8326809b333b2e92ba3e6f6cb1b',
      createTime: '2021-11-09T02:46:57.000+0000',
      updateTime: '2021-11-22T07:39:42.000+0000',
      code: 'tjzer',
      name: '厅局负责人',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: '0c89f6e843b466bf863e10a4fef43912',
      createTime: '2020-12-01T09:51:20.000+0000',
      updateTime: '2021-09-26T08:04:51.000+0000',
      code: 'sysadmin',
      name: '系统管理员',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: '0445a1f893f792bc40cda425ee459c2c',
      createTime: '2020-12-01T08:29:50.000+0000',
      updateTime: '2021-04-15T07:50:44.000+0000',
      code: 'secadmin',
      name: '安全保密员',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: '4989607e9312d02c51fb4560f674e896',
      createTime: '2021-11-09T02:46:40.000+0000',
      updateTime: '2021-11-21T02:34:37.000+0000',
      code: 'cz',
      name: '处长',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: '0c89f6e843b466bf863e10a4fef43942',
      createTime: '2020-12-01T09:51:20.000+0000',
      updateTime: '2021-09-26T08:04:51.000+0000',
      code: 'sjgly',
      name: '数据管理员',
      status: 1,
      createId: null,
      updateId: null
     },
     {
      id: '0c89f6e843b466bf863e10a4fef43932',
      createTime: '2020-12-01T09:51:20.000+0000',
      updateTime: '2021-09-26T08:04:51.000+0000',
      code: 'normal',
      name: '普通用户',
      status: 1,
      createId: null,
      updateId: null
     }
    ]
   }
  ]
 }
};
const dataRuleManageMock = {
 // 添加模板
 dataRuleManageTemplate
};
export default dataRuleManageMock;
