/**
 *create by yani ON 2022/2/7
 *
 **/
const todoTableDataTemplate = [
 {
  name: '西藏建工建材集团有限公司',
  bloc: '西藏建工建材集团',
  outlayType: '投资性业务',
  outlayProject: '项目管理投资支出',
  createTime: '2022-01-9 16:45:37',
  amount: '200万',
  coinType: 'CNY',
  remark: ''
 },
 {
  name: '西藏航空有限公司',
  bloc: '西藏航空',
  outlayType: '经营性业务',
  outlayProject: '工资支出',
  createTime: '2021-01-10 14:44:33',
  amount: '50万',
  coinType: 'CNY',
  remark: '企业人员工资'
 },
 {
  name: '西藏国有资本投资运营有限公司',
  bloc: '国投集团',
  outlayType: '筹资性业务',
  outlayProject: '发展新项目筹资',
  createTime: '2021-01-10 17:24:32',
  amount: '150万',
  coinType: 'CNY',
  remark: ''
 },
 {
  name: '西藏中兴商贸物流产业发展集团有限公司',
  bloc: '中兴商贸',
  outlayType: '经营性业务',
  outlayProject: '工资支出',
  createTime: '2021-01-11 09:11:50',
  amount: '80万',
  coinType: 'CNY',
  remark: ''
 },
 {
  name: '西藏国际旅游文化投资集团有限公司',
  bloc: '旅投资集团',
  outlayType: '投资性业务',
  outlayProject: '项目管理投资支出',
  createTime: '2022-01-11 16:45:37',
  amount: '100万',
  coinType: 'CNY',
  remark: '本年度项目投资'
 },
 {
  name: '西藏甘露藏药股份有限公司',
  bloc: '甘露藏药',
  outlayType: '经营性业务',
  outlayProject: '工资支出',
  createTime: '2021-01-12 14:44:33',
  amount: '70万',
  coinType: 'CNY',
  remark: ''
 },
 {
  name: '西藏农牧产业投资集团有限公司',
  bloc: '农投集团',
  outlayType: '投资性业务',
  outlayProject: '产业投资支出',
  createTime: '2021-01-13 09:46:31',
  amount: '80万',
  coinType: 'CNY',
  remark: ''
 }
];
const doneTableDataTemplate = [
 {
  name: '西藏建工建材集团有限公司',
  bloc: '西藏建工建材集团',
  outlayType: '投资性业务',
  outlayProject: '项目管理投资支出',
  createTime: '2022-01-9 16:45:37',
  amount: '200万',
  coinType: 'CNY',
  remark: '',
  recordTime: '2022-02-01 09:36:31',
  recordPerson: '张益华',
  reportNumber: 'DEBA2022020145720145'
 },
 {
  name: '西藏航空有限公司',
  bloc: '西藏航空',
  outlayType: '经营性业务',
  outlayProject: '工资支出',
  createTime: '2021-01-10 14:44:33',
  amount: '50万',
  coinType: 'CNY',
  remark: '企业人员工资',
  recordTime: '2022-02-01 09:52:34',
  recordPerson: '王明',
  reportNumber: 'DEBA2022020145720146'
 },
 {
  name: '西藏国有资本投资运营有限公司',
  bloc: '国投集团',
  outlayType: '筹资性业务',
  outlayProject: '发展新项目筹资',
  createTime: '2021-01-10 18:24:32',
  amount: '150万',
  coinType: 'CNY',
  remark: '',
  recordTime: '2022-02-02 11:24:01',
  recordPerson: '李佳佳',
  reportNumber: 'DEBA2022020245720147'
 },
 {
  name: '西藏中兴商贸集团有限公司',
  bloc: '中兴商贸',
  outlayType: '经营性业务',
  outlayProject: '工资支出',
  createTime: '2021-01-11 09:11:50',
  amount: '80万',
  coinType: 'CNY',
  remark: '',
  recordTime: '2022-02-03 15:46:24',
  recordPerson: '张益华',
  reportNumber: 'DEBA2022020345720149'
 },
 {
  name: '西藏国际旅游文化投资集团有限公司',
  bloc: '旅投资集团',
  outlayType: '投资性业务',
  outlayProject: '项目管理投资支出',
  createTime: '2022-01-11 16:45:37',
  amount: '100万',
  coinType: 'CNY',
  remark: '本年度项目投资',
  recordTime: '2022-02-04 18:36:30',
  recordPerson: '李佳佳',
  reportNumber: 'DEBA2022020445720133'
 },
 {
  name: '西藏甘露藏药股份有限公司',
  bloc: '甘露藏药',
  outlayType: '经营性业务',
  outlayProject: '工资支出',
  createTime: '2021-01-12 14:44:33',
  amount: '70万',
  coinType: 'CNY',
  remark: '',
  recordTime: '2022-02-05 10:23:21',
  recordPerson: '赵为昱',
  reportNumber: 'DEBA2022020545720151'
 },
 {
  name: '西藏农牧产业投资集团有限公司',
  bloc: '农投集团',
  outlayType: '投资性业务',
  outlayProject: '产业投资支出',
  createTime: '2021-01-13 09:46:31',
  amount: '80万',
  coinType: 'CNY',
  remark: '',
  recordTime: '2022-02-06 13:24:21',
  recordPerson: '张益华',
  reportNumber: 'DEBA2022020645720125'
 }
];
const accListMockTemplate = [
 {
  name: '西藏建工建材集团有限公司',
  bloc: '西藏建工建材集团',
  outlayType: '投资性业务',
  outlayProject: '项目管理投资支出',
  createTime: '2022-01-9 16:45:37',
  amount: '200万',
  coinType: 'CNY',
  remark: '',
  recordTime: '2022-02-01 09:36:31',
  recordPerson: '张益华',
  reportNumber: 'DEBA2022020145720145',
  transactionDate: '2022-02-01 09:36:31',
  outlayAccount: '西藏乳业',
  outlayName: '西藏银行',
  incomeAccount: '西藏银行',
  incomeName: '赛迪信息',
  summary: '100万',
  balance: '50万'
 }
];
const recordDealMock = {
 // 添加模板
 todoTableDataTemplate,
 doneTableDataTemplate,
 accListMockTemplate
};
export default recordDealMock;
