/**
 *create by yani ON 2022/2/7
 *
 **/
const infos = {
 name: '西藏格格乳业有限公司',
 bloc: '西藏格格乳业集团',
 outlayType: '类型01',
 outlayProject: '事项01',
 createTime: '2021/09/20 08:47:50',
 outlayAccount: '9558880200001667777',
 outlayBank: '中国银行西藏支行',
 incomeAccount: '9558880200001668888',
 incomeBank: '中国银行西藏支行',
 incomeName: '西藏格格乳业有限公司',
 amount: '100万元',
 coinType: 'CNY',
 useWay: '投资',
 remark: '投资项目'
};

const fileInfos = [
 {
  name: '经营性业务',
  data: [{
   name: '尽责调查报告、合同招标或竞争性谈判资料、合同及合同评审表',
   fileList: [
    {
     name: '尽职报告.doc',
     type: 1
    },
    {
     name: 'xxx招标文件.doc',
     type: 1
    },
    {
     name: 'xxx调查报告.doc',
     type: 1
    },
    {
     name: 'xxx调查报告.doc',
     type: 1
    },
    {
     name: 'xxx调查报告.doc',
     type: 1
    },
    {
     name: '尽职报告.doc',
     type: 1
    }
   ]
  },
  {
   name: '结算单、验收单等、签字齐全的请款单',
   fileList: [
    {
     name: '单据01.xls',
     type: 3
    },
    {
     name: '单据02.xls',
     type: 3
    },
    {
     name: '单据03.xls',
     type: 3
    },
    {
     name: '单据04.xls',
     type: 3
    },
    {
     name: '单据05.xls',
     type: 3
    },
    {
     name: '单据06.xls',
     type: 3
    }
   ]
  },
  {
   name: '企业相关会议纪要、文件、说明等决策资料',
   fileList: [
    {
     name: '相关资料01.xls',
     type: 3
    },
    {
     name: '相关资料02.doc',
     type: 1
    }
   ]
  }
  ]
 },
 {
  name: '投资性业务',
  data: [{
   name: '可行性报告、风险评估报告、投资收益测算、项目预算、合同及合同评审表',
   fileList: [
    {
     name: '可行性报告.doc',
     type: 1
    },
    {
     name: '风险评估报告.doc',
     type: 1
    },
    {
     name: '附件01.doc',
     type: 1
    },
    {
     name: '附件02.doc',
     type: 1
    },
    {
     name: '附件03.doc',
     type: 1
    },
    {
     name: '附件04.doc',
     type: 1
    }
   ]
  },
  {
   name: '结算单、签字齐全的请款单',
   fileList: [
    {
     name: '相关资料01.xls',
     type: 3
    },
    {
     name: '相关资料02.doc',
     type: 1
    }
   ]
  },
  {
   name: '企业相关会议纪要、文件、说明等决策资料',
   fileList: [
    {
     name: '相关资料01.xls',
     type: 3
    },
    {
     name: '相关资料02.doc',
     type: 1
    }
   ]
  }
  ]
 },

 {
  name: '筹资性业务',
  data: [{
   name: '风险评估报告、合同及合同评审表',
   fileList: [
    {
     name: '相关资料01.xls',
     type: 3
    },
    {
     name: '相关资料02.doc',
     type: 2
    }
   ]
  },
  {
   name: '签字齐全的请款单',
   fileList: [
    {
     name: '相关资料01.xls',
     type: 3
    },
    {
     name: '相关资料02.doc',
     type: 1
    }
   ]
  },
  {
   name: '企业相关会议纪要、文件、说明等决策资料',
   fileList: [
    {
     name: '相关资料01.xls',
     type: 3
    },
    {
     name: '相关资料02.doc',
     type: 1
    }
   ]
  }
  ]
 }
];
// 已备案操作信息数据mock
const doneInfo = {
 status: '已备案',
 recordPerson: '张益文',
 recordTime: '2021/09/20 08:47:50',
 reportNumber: 'DEBA2022020145720145'
};
// 已退回操作信息mock
const backInfo = {
 status: '已备案',
 recordPerson: '张益文',
 recordTime: '2021/09/20 08:47:50',
 reportNumber: 'DEBA2022020145720145'
};
const remindData = {
 reminder: '根据监管调查发现，部分支付机构通过与聚合技术服务商签订服务协议，将商户审核责任及其带来的风险损失转嫁至聚合技术服务商，部分大型聚合技术服务商通过其自身展业APP实现商户申请资料在线录入，远程发展商户，并未实地对商户进行确认。部分聚合技术服务商与支付机构商户管理系统对接，实现申请资料在线批量传输，支付机构系统自动接收申请资料，既无实地核验，也未核查商户营业执照的真实性，造成出现大量虚假商户'
};
const recordAppMock = {
 infos, // 待处理备案审批详情数据mock
 fileInfos,
 doneInfo, // 已备案操作信息数据mock
 backInfo, // 已退回操作信息详情mock
 remindData // 已退回提示函详情mock
};
export default recordAppMock;
