
const top = {
 message: 'success',
 code: 0,
 system: {
  time: '2021/09/26 | 至今日',
  companyNum: 116,
  accountNum: 120,
  fundNum: 172,
  dealNum: 172
 },
 weekData: [200, 300, 500, 580, 610, 580, 740],
 CNY: '121,120,340,134',
 HKD: '61,310,204,101',
 USD: '72,230,102,312',
 Euro: '121,000,000,123',
 GBP: '341,210,512,120',
 JPY: '213,210,000,321',
 put: {
  fundPercentage: 60,
  declarationTotal: '72,230,102,312',
  fundForthputting: '61,310,204,101',
  fundBudget: '72,230,102'
 }
};

const middle = {
 message: 'success',
 code: 0,
 horData: [
  { num: '32', info: '待处理大额支出备案' },
  { num: '13', info: '大额支出预警' },
  { num: '11', info: '累计金额超标预警' },
  { num: '7', info: '累计笔数超标预警' },
  { num: '17', info: '未备案支出超期预警' },
  { num: '71', info: '对外担保预警' },
  { num: '6', info: '文件超时未报预警' },
  { num: '12', info: '同一账户周期收入预警' },
  { num: '41', info: '资金安全存量预警' }
 ],
 putData: [
  {
   name: '西藏建工建材集团有限公司',
   status: '0',
   outlayType: '经营性业务',
   amount: '120,000 CNY',
   createTime: '2022-3-2 10:00:00',
   recordTime: '---',
   reportNumber: '---'
  },
  {
   name: '西藏航空有限公司',
   status: '0',
   outlayType: '经营性业务',
   amount: '150,000 CNY',
   createTime: '2022-2-2 10:00:00',
   recordTime: '---',
   reportNumber: '---'
  }, {
   name: '西藏国有资本投资运营有限公司',
   status: '1',
   outlayType: '经营性业务',
   amount: '110,000 CNY',
   createTime: '2022-1-2',
   recordTime: '2021-9-19 09:00:00',
   reportNumber: '2021-9-29 09:00:00'
  },
  {
   name: '西藏甘露藏药股份有限公司',
   status: '2',
   outlayType: '经营性业务',
   amount: '130,405 CNY',
   createTime: '2022-2-12',
   recordTime: '2021-10-20 09:00:00',
   reportNumber: '2021-10-21 09:00:00'
  }
 ],
 lineData: [1370, 1410, 1400, 1480, 1460, 1600, 1650, 1660, 1610, 1600, 1620, 1760]
};

const foot = [
 {
  name: '西藏建工建材集团有限公司',
  bloc: '西藏建工建材集团',
  transactionDate: '2021/09/1 09:46:50',
  outlayAccount: '1191882810021755',
  outlayName: '西藏建工建材集团有限公司',
  incomeAccount: '1882317188',
  incomeName: '西藏格格乳业有限公司',
  summary: '投资性业务',
  amount: '10,100,100',
  reportNumber: '12301025',
  balance: '658',
  coinType: 'CNY'
 },
 {
  name: '西藏航空有限公司',
  bloc: '西藏航空',
  transactionDate: '2021/10/1 09:46:50',
  outlayAccount: '1191882810033665',
  outlayName: '西藏航空有限公司',
  incomeAccount: '1882313211',
  incomeName: '西藏国有资本投资运营有限公司',
  summary: '投资性业务',
  amount: '10,300,100',
  reportNumber: '14301025',
  balance: '1658',
  coinType: 'CNY'
 },
 {
  name: '西藏中兴商贸物流产业发展集团有限公司',
  bloc: '中兴商贸',
  transactionDate: '2021/08/1 06:46:50',
  outlayAccount: '1231882610721785',
  outlayName: '西藏中兴商贸物流产业发展集团有限公司',
  incomeAccount: '1552314186',
  incomeName: '西藏国际旅游文化投资集团有限公司',
  summary: '投资性业务',
  amount: '13,300,445',
  reportNumber: '34303025',
  balance: '638',
  coinType: 'CNY'
 },
 {
  name: '西藏甘露藏药股份有限公司',
  bloc: '甘露藏药',
  transactionDate: '2021/12/1 05:42:10',
  outlayAccount: '1231884810521655',
  outlayName: '西藏甘露藏药股份有限公司',
  incomeAccount: '1882317188',
  incomeName: '西藏农牧产业投资集团有限公司',
  summary: '投资性业务',
  amount: '10,300,100',
  reportNumber: '12321245',
  balance: '58',
  coinType: 'CNY'
 }
];

const homeMock = {
 top,
 middle,
 foot
};
export default homeMock;
