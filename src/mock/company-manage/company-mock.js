const listData = [
 {
  name: '西藏格格乳业有限公司',
  bloc: '西藏格格乳业集团',
  createTime: '2021/04/1 09:46:50',
  registerCapital: '110万元',
  industry: '畜牧业',
  area: '西藏地区',
  location: '西藏'
 },
 {
  name: '西藏建工建材集团有限公司',
  bloc: '西藏建工建材集团',
  createTime: '2021/09/1 09:46:50',
  registerCapital: '100万元',
  industry: '建材',
  area: '西藏地区',
  location: '西藏'
 },
 {
  name: '西藏航空有限公司',
  bloc: '西藏航空',
  createTime: '2021/10/1 09:46:50',
  registerCapital: '150万元',
  industry: '航空',
  area: '西藏地区',
  location: '西藏'
 },
 {
  name: '西藏中兴商贸物流产业发展集团有限公司',
  bloc: '中兴商贸',
  createTime: '2021/08/1 06:46:50',
  registerCapital: '120万元',
  industry: '商贸',
  area: '西藏地区',
  location: '西藏'
 },
 {
  name: '西藏甘露藏药股份有限公司',
  bloc: '甘露藏药',
  createTime: '2021/12/1 05:42:10',
  registerCapital: '200万元',
  industry: '药业',
  area: '西藏地区',
  location: '西藏'
 },
 {
  name: '西藏交通发展集团有限公司',
  bloc: '交发集团',
  createTime: '2021/12/1 07:42:10',
  registerCapital: '100万元',
  industry: '交通',
  area: '西藏地区',
  location: '西藏'
 },
 {
  name: '西藏幸福家园产业集团有限公司',
  bloc: '幸福家园',
  createTime: '2021/12/1 08:42:10',
  registerCapital: '200万元',
  industry: '物业',
  area: '西藏地区',
  location: '西藏'
 },
 {
  name: '西藏农牧产业投资集团有限公司',
  bloc: '农投集团',
  createTime: '2021/11/11 08:42:10',
  registerCapital: '100万元',
  industry: '农牧业',
  area: '西藏地区',
  location: '西藏'
 }
];

const fundList = [
 {
  name: '西藏建工建材集团有限公司',
  bloc: '西藏建工建材集团',
  transactionDate: '2021/09/1 09:46:50',
  outlayAccount: '1191882810021755',
  outlayName: '西藏建工建材集团有限公司',
  incomeAccount: '1882317188',
  incomeName: '西藏格格乳业有限公司',
  summary: '投资性业务',
  amount: '10,100,100',
  reportNumber: '12301025',
  balance: '658',
  coinType: 'CNY'
 },
 {
  name: '西藏航空有限公司',
  bloc: '西藏航空',
  transactionDate: '2021/10/1 09:46:50',
  outlayAccount: '1191882810033665',
  outlayName: '西藏航空有限公司',
  incomeAccount: '1882313211',
  incomeName: '西藏国有资本投资运营有限公司',
  summary: '投资性业务',
  amount: '10,300,100',
  reportNumber: '14301025',
  balance: '1658',
  coinType: 'CNY'
 },
 {
  name: '西藏中兴商贸物流产业发展集团有限公司',
  bloc: '中兴商贸',
  transactionDate: '2021/08/1 06:46:50',
  outlayAccount: '1231882610721785',
  outlayName: '西藏中兴商贸物流产业发展集团有限公司',
  incomeAccount: '1552314186',
  incomeName: '西藏国际旅游文化投资集团有限公司',
  summary: '投资性业务',
  amount: '13,300,445',
  reportNumber: '34303025',
  balance: '638',
  coinType: 'CNY'
 },
 {
  name: '西藏甘露藏药股份有限公司',
  bloc: '甘露藏药',
  transactionDate: '2021/12/1 05:42:10',
  outlayAccount: '1231884810521655',
  outlayName: '西藏甘露藏药股份有限公司',
  incomeAccount: '1882317188',
  incomeName: '西藏农牧产业投资集团有限公司',
  summary: '投资性业务',
  amount: '10,300,100',
  reportNumber: '12321245',
  balance: '58',
  coinType: 'CNY'
 }
];

const first = {
 basicList: {
  name: '西藏建工建材集团有限公司',
  bloc: '建工建材',
  assetsScale: '1000万元',
  registerCapital: '100万元',
  industry: '建工建材',
  area: '西藏地区',
  location: '西藏'
 },
 amountData: [
  {
   currency: 'CNY',
   unify: '900,000',
   companyArguments: '900,000'
  },
  {
   currency: 'CNY',
   unify: '1,100,000',
   companyArguments: '1,100,000'
  },
  {
   currency: 'CNY',
   unify: '1,200,000',
   companyArguments: '1,200,000'
  },
  {
   currency: 'CNY',
   unify: '1,300,000',
   companyArguments: '1,300,000'
  }
 ],
 countSetData: [
  {
   type: 'CNY',
   week: '周',
   unify: '12',
   companyArguments: '12'
  },
  {
   type: 'CNY',
   week: '周',
   unify: '7',
   companyArguments: '7'
  },
  {
   type: 'CNY',
   week: '周',
   unify: '8',
   companyArguments: '8'
  },
  {
   type: 'USD',
   week: '月',
   unify: '11',
   companyArguments: '11'
  },
  {
   type: 'CNY',
   week: '月',
   unify: '10',
   companyArguments: '10'
  }
 ],
 addAountData: [
  {
   type: '全账户',
   week: '月',
   currency: 'CNY',
   unify: '10,000',
   companyArguments: '10,000'
  },
  {
   type: '全账户',
   week: '周',
   currency: 'CNY',
   unify: '9,000',
   companyArguments: '9,000'
  },
  {
   type: '全账户',
   week: '月',
   currency: 'CNY',
   unify: '8,000',
   companyArguments: '8,000'
  },
  {
   type: '同一账户',
   week: '周',
   currency: 'USD',
   unify: '8,800',
   companyArguments: '8,800'
  },
  {
   type: '同一账户',
   week: '周',
   currency: 'USD',
   unify: '9,800',
   companyArguments: '9,800'
  }
 ],
 secureData: [
  {
   currency: 'CNY',
   unify: '10%',
   companyArguments: '10%'
  },
  {
   currency: 'USD',
   unify: '15%',
   companyArguments: '15%'
  }
 ],
 foreignData: [
  {
   currency: 'CNY',
   unify: '10%',
   companyArguments: '10%'
  },
  {
   currency: 'USD',
   unify: '15%',
   companyArguments: '15%'
  }
 ]
};

const second = {
 basicList: {
  name: '西藏建工建材集团有限公司',
  bloc: '建工建材',
  assetsScale: '1000万元',
  registerCapital: '100万元',
  industry: '建工建材',
  area: '西藏地区',
  location: '西藏'
 },
 expendData: [
  {
   outlayType: '类型01',
   outlayProject: '1,000,000',
   createTime: '2021/09/20 08:47:50',
   status: '待处理',
   amount: '100万元',
   coinType: 'CNY',
   remark: '----',
   reportNumber: 'JUHH19021'
  },
  {
   outlayType: '类型02',
   outlayProject: '1,100,000',
   createTime: '2021/08/20 08:47:50',
   status: '待处理',
   amount: '110万元',
   coinType: 'CNY',
   remark: '----',
   reportNumber: 'JUHH11021'
  }
 ],
 useData: [
  {
   outlayType: '类型01',
   outlayProject: '1,000,000',
   recordTime: '2021/09/20 08:47:50',
   amount: '100万元',
   amountSum: '100万元',
   coinType: 'CNY',
   reportNumber: 'JUHH19021'
  },
  {
   outlayType: '类型02',
   outlayProject: '1,100,000',
   recordTime: '2021/08/20 08:47:50',
   amount: '110万元',
   amountSum: '110万元',
   coinType: 'CNY',
   reportNumber: 'JUHH11021'
  }
 ]
};

const third = {
 basicList: {
  name: '西藏建工建材集团有限公司',
  bloc: '建工建材',
  assetsScale: '1000万元',
  registerCapital: '100万元',
  industry: '建工建材',
  area: '西藏地区',
  location: '西藏'
 },
 tableData: [
  {
   openNumber: 'JJUNN929312',
   openBank: '中国银行西藏支行',
   updateTime: '2021/09/20 08:47:50',
   amount: '100万元',
   currency: 'CNY'
  },
  {
   openNumber: 'JJUNN929662',
   openBank: '农业银行西藏分行',
   updateTime: '2021/10/20 08:47:50',
   amount: '200万元',
   currency: 'CNY'
  },
  {
   openNumber: 'JJUNN989312',
   openBank: '建设银行西藏分行',
   updateTime: '2021/12/20 08:47:50',
   amount: '160万元',
   currency: 'CNY'
  },
  {
   openNumber: 'JJUNN989912',
   openBank: '交通银行西藏分行',
   updateTime: '2021/12/22 08:47:50',
   amount: '170万元',
   currency: 'CNY'
  },
  {
   openNumber: 'JJUNN989992',
   openBank: '招商银行西藏分行',
   updateTime: '2021/12/28 08:47:50',
   amount: '178万元',
   currency: 'CNY'
  }
 ]
};

const fourth = {
 basicList: {
  name: '西藏建工建材集团有限公司',
  bloc: '建工建材',
  assetsScale: '1000万元',
  registerCapital: '100万元',
  industry: '建工建材',
  area: '西藏地区',
  location: '西藏'
 },
 tableData: [
  {
   transactionDate: '2021/09/5 09:46:50',
   createTime: '2021/09/1 09:46:50',
   outlayAccount: '1191882810021755',
   outlayName: '西藏建工建材集团有限公司',
   incomeAccount: '1882317188',
   incomeName: '西藏格格乳业有限公司',
   summary: '投资性业务',
   amount: '10,100,100',
   reportNumber: '12301025',
   balance: '658',
   coinType: 'CNY'
  },
  {
   transactionDate: '2021/10/5 09:46:50',
   createTime: '2021/10/1 09:46:50',
   outlayAccount: '1191882810033665',
   outlayName: '西藏航空有限公司',
   incomeAccount: '1882313211',
   incomeName: '西藏国有资本投资运营有限公司',
   summary: '投资性业务',
   amount: '10,300,100',
   reportNumber: '14301025',
   balance: '1658',
   coinType: 'CNY'
  },
  {
   transactionDate: '2021/08/5 09:46:50',
   createTime: '2021/08/1 06:46:50',
   outlayAccount: '1231882610721785',
   outlayName: '西藏中兴商贸物流产业发展集团有限公司',
   incomeAccount: '1552314186',
   incomeName: '西藏国际旅游文化投资集团有限公司',
   summary: '投资性业务',
   amount: '13,300,445',
   reportNumber: '34303025',
   balance: '638',
   coinType: 'CNY'
  },
  {
   transactionDate: '2021/12/5 09:46:50',
   createTime: '2021/12/1 05:42:10',
   outlayAccount: '1231884810521655',
   outlayName: '西藏甘露藏药股份有限公司',
   incomeAccount: '1882317188',
   incomeName: '西藏农牧产业投资集团有限公司',
   summary: '投资性业务',
   amount: '10,300,100',
   reportNumber: '12321245',
   balance: '58',
   coinType: 'CNY'
  }
 ]
};

const companyMock = {
 listData,
 fundList,
 first,
 second,
 third,
 fourth
};
export default companyMock;
