/**
 *create by yani ON 2022/2/7
 *
 **/
const todoTableDataTemplate = [
 {
  entName: '西藏建工建材集团有限公司',
  group: '西藏建工建材集团',
  warnTime: '2022-01-9 16:45:37',
  accType: '同一账户',
  preMoney: '30',
  triMoney: '20'
 },
 {
  entName: '西藏航空有限公司',
  group: '西藏航空',
  warnTime: '2022-01-9 16:45:37',
  accType: '全账户',
  preMoney: '10',
  triMoney: '20'
 },
 {
  entName: '西藏国有资本投资运营有限公司',
  group: '国投集团',
  warnTime: '2022-01-12 08:40:24',
  accType: '同一账户',
  preMoney: '10',
  triMoney: '30'
 },
 {
  entName: '西藏中兴商贸物流产业发展集团有限公司',
  group: '中兴商贸',
  warnTime: '2022-01-12 16:11:15',
  accType: '同一账户',
  preMoney: '30',
  triMoney: '40'
 },
 {
  entName: '西藏国际旅游文化投资集团有限公司',
  group: '旅投资集团',
  warnTime: '2022-01-13 15:46:52',
  accType: '全账户',
  preMoney: '50',
  triMoney: '50'
 },
 {
  entName: '西藏甘露藏药股份有限公司',
  group: '甘露藏药',
  warnTime: '2022-01-14 14:31:41',
  accType: '同一账户',
  preMoney: '20',
  triMoney: '30'
 }
];

const accWarnMock = {
 // 添加模板
 todoTableDataTemplate
};
export default accWarnMock;
