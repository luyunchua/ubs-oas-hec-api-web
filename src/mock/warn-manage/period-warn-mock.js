/**
 *create by yani ON 2022/2/7
 *
 **/
const todoTableDataTemplate = [
 {
  entName: '西藏建工建材集团有限公司',
  group: '西藏建工建材集团',
  warnTime: '2022-01-9 16:45:37',
  preMoney: '150万',
  triMoney: '200万',
  currency: 'CNY',
  payAcc: '西藏建工建材集团',
  payBank: '中国银行西藏分行',
  recAcc: '赛迪信息有限公司',
  recBank: '中国银行重庆分行',
  recName: '赛迪信息',
  period: '月'
 },
 {
  entName: '西藏航空有限公司',
  group: '西藏航空',
  warnTime: '2022-01-9 16:45:37',
  preMoney: '100万',
  triMoney: '130万',
  currency: 'CNY',
  payAcc: '西藏航空',
  payBank: '西藏银行',
  recAcc: '赛迪咨询有限公司',
  recBank: '重庆银行',
  recName: '赛迪咨询',
  period: '月'
 },
 {
  entName: '西藏国有资本投资运营有限公司',
  group: '国投集团',
  warnTime: '2022-01-12 08:40:24',
  preMoney: '100万',
  triMoney: '100万',
  currency: 'CNY',
  payAcc: '国投集团',
  payBank: '交通银行西藏分行',
  recAcc: '赛迪信息有限公司',
  recBank: '中国银行重庆分行',
  recName: '赛迪信息',
  period: '年'
 },
 {
  entName: '西藏中兴商贸物流产业发展集团有限公司',
  group: '中兴商贸',
  warnTime: '2022-01-12 16:11:15',
  preMoney: '80万',
  triMoney: '100万',
  currency: 'CNY',
  payAcc: '中兴商贸',
  payBank: '中国银行西藏分行',
  recAcc: '中冶赛迪集团',
  recBank: '中国银行重庆分行',
  recName: '赛迪集团',
  period: '月'
 },
 {
  entName: '西藏国际旅游文化投资集团有限公司',
  group: '旅投资集团',
  warnTime: '2022-01-13 15:46:52',
  preMoney: '150万',
  triMoney: '200万',
  currency: 'CNY',
  payAcc: '西藏建工建材集团',
  payBank: '中国银行西藏分行',
  recAcc: '中冶赛迪集团',
  recBank: '重庆银行',
  recName: '赛迪集团',
  period: '年'
 },
 {
  entName: '西藏甘露藏药股份有限公司',
  group: '甘露藏药',
  warnTime: '2022-01-14 14:31:41',
  preMoney: '50万',
  triMoney: '80万',
  currency: 'CNY',
  payAcc: '甘露藏药',
  payBank: '中国银行西藏分行',
  recAcc: '赛迪信息有限公司',
  recBank: '中国银行重庆分行',
  recName: '赛迪信息',
  period: '月'
 }
];

const expandWarnMock = {
 // 添加模板
 todoTableDataTemplate
};
export default expandWarnMock;
