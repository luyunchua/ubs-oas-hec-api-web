/**
 *create by yani ON 2022/2/7
 *
 **/
const todoTableDataTemplate = [
 {
  entName: '西藏建工建材集团有限公司',
  group: '西藏建工建材集团',
  warnTime: '2022-01-9 16:45:37',
  period: '月',
  reportTime: '2022-01-9 16:45:41',
  fileType: '大额支出报表'
 },
 {
  entName: '西藏航空有限公司',
  group: '西藏航空',
  warnTime: '2022-01-9 16:57:37',
  period: '月',
  reportTime: '2022-01-9 16:45:03',
  fileType: '资金存量分布表'
 },
 {
  entName: '西藏国有资本投资运营有限公司',
  group: '国投集团',
  warnTime: '2022-01-12 08:40:24',
  period: '年',
  reportTime: '2022-01-12 08:40:24',
  fileType: '银行开户情况表'
 },
 {
  entName: '西藏中兴商贸物流产业发展集团有限公司',
  group: '中兴商贸',
  warnTime: '2022-01-12 16:11:15',
  period: '月',
  reportTime: '2022-01-12 16:11:15',
  fileType: '大额支出报表'
 },
 {
  entName: '西藏国际旅游文化投资集团有限公司',
  group: '旅投资集团',
  warnTime: '2022-01-13 15:46:52',
  period: '年',
  reportTime: '2022-01-13 15:46:15',
  fileType: '大额支出报表'
 },
 {
  entName: '西藏甘露藏药股份有限公司',
  group: '甘露藏药',
  warnTime: '2022-01-14 14:31:41',
  period: '月',
  reportTime: '2022-01-14 14:46:15',
  fileType: '资金存量分布表'
 }
];

const expandWarnMock = {
 // 添加模板
 todoTableDataTemplate
};
export default expandWarnMock;
