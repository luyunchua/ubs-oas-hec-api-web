import request from '@/utils/request';
import recordUrl from '@/api/url-info/index';

export function instrumentPanel (params) {
  return request({
    url: recordUrl.instrumentPanel,
    method: 'get',
    params
  });
}
export function annualSize (params) {
  return request({
    url: recordUrl.annualSize,
    method: 'get',
    params
  });
}
export function cumulativeProfitLossCurrentYear (params) {
  return request({
    url: recordUrl.cumulativeProfitLossCurrentYear,
    method: 'get',
    params
  });
}
export function hedgeTotalProfitLossCurrentYear (params) {
  return request({
    url: recordUrl.hedgeTotalProfitLossCurrentYear,
    method: 'get',
    params
  });
}
export function currencyHoldings (params) {
  return request({
    url: recordUrl.currencyHoldings,
    method: 'get',
    params
  });
}
export function completion (params) {
  return request({
    url: recordUrl.completion,
    method: 'get',
    params
  });
}
export function details (data) {
  return request({
    url: recordUrl.monthDetails,
    method: 'post',
    data
  });
}