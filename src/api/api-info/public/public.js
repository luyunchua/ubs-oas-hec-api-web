import url from '@/api/url-info/index'
import request from '@/utils/request'
// 文件上传
export function fileUpload(data) {
 return request({
  url: url.fileUpload,
  method: 'post',
  data
 })
}
// 附件下载
export function downloadFile(params) {
 return request({
  url: url.downloadFile,
  responseType: 'blob',
  method: 'get',
  params
 })
}
// 文件删除
export function deleteFile(ids) {
 return request({
  url: url.deleteFile,
  method: 'delete',
  data: ids
 })
}
// 文件批量上传
export function uploadFileBoth(data) {
 return request({
  url: url.uploadFileBoth,
  method: 'post',
  data: data
 })
}

export function regulationsListUpload(data) {
 const result = request({
  url: url.regulationsListUpload,
  method: 'post',
  data
 }).then(
  data => {
   return data
  },
  error => {
   return {}
  }
 )
 return result
}

export function previewFile(params) {
 return request({
  url: url.previewFile,
  method: 'get',
  params
 })
}

// 字典
export function getDictSelect(params) {
 return request({
  url: url.getDictSelect,
  method: 'get',
  params
 })
}

// 字典 批量
export function getDictSelectList(params) {
 const result = request({
  url: url.getDictSelectList + '?type=' + params,
  method: 'get'
 }).then(
  data => {
   return data
  },
  error => {
   return {}
  }
 )
 return result
}
// 字典 模糊
export function getDictList1(params) {
 const result = request({
  url: url.getDictList1,
  method: 'get',
  params
 }).then(
  data => {
   return data
  },
  error => {
   return {}
  }
 )
 return result
}
// 文件导入
export function regulationsListImport(data) {
 return request({
  url: url.regulationsListImport,
  method: 'post',
  data
 })
}
