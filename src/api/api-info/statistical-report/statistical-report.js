import request from '@/utils/request';
import recordUrl from '@/api/url-info/index';

export function derivativeDetail (params) {
  return request({
    url: recordUrl.derivativeDetail,
    method: 'get',
    params
  });
}
export function commodityBusinessTypeCollect (params) {
  return request({
    url: recordUrl.commodityBusinessTypeCollect,
    method: 'get',
    params
  });
}
export function tableExport (params) {
  return request({
    url: recordUrl.tableExport,
    method: 'get',
    params
  });
}