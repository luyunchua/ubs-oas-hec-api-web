import request from '@/utils/request';
import recordUrl from '@/api/url-info/index';

export function getRecordDealList (params) {
  return request({
    url: recordUrl.getJwDictItem,
    method: 'get',
    params
  });
}
export function publicPageList (params) {
  return request({
    url: recordUrl.pageList,
    method: 'get',
    params
  });
}
export function delArticle (data) {
  return request({
    url: recordUrl.delArticle,
    method: 'delete',
    data
  });
}
export function addArticle (data) {
  return request({
    url: recordUrl.addArticle,
    method: 'post',
    data
  });
}
export function editArticle (data) {
  return request({
    url: recordUrl.editArticle,
    method: 'put',
    data
  });
}