import request from '@/utils/request';
import url from '@/api/url-info/index';

export function totalPercentage (params) {
  return request({
    url: url.totalPercentage,
    method: 'GET',
    params
  });
}
export function listOfTotalFunds (params) {
  return request({
    url: url.listOfTotalFunds,
    method: 'GET',
    params
  });
}
export function fundExpenditureStatistics (params) {
  return request({
    url: url.fundExpenditureStatistics,
    method: 'GET',
    params
  });
}
export function useFundsExpenditureRanking (params) {
  return request({
    url: url.useFundsExpenditureRanking,
    method: 'GET',
    params
  });
}
export function fundDailyList (params) {
  return request({
    url: url.fundDailyList,
    method: 'GET',
    params
  });
}
export function fiveDays (params) {
  return request({
    url: url.fiveDays,
    method: 'GET',
    params
  });
}
export function fiveMonths (params) {
  return request({
    url: url.fiveMonths,
    method: 'GET',
    params
  });
}
export function fiveQuarters (params) {
  return request({
    url: url.fiveQuarters,
    method: 'GET',
    params
  });
}
export function fiveYears (params) {
  return request({
    url: url.fiveYears,
    method: 'GET',
    params
  });
}