import request from '@/utils/request';
import url from '@/api/url-info/index';

export function login(data) {
 return request({
  url: url.login,
  method: 'post',
  data
 });
}
// 密码过期
export function passwordAging() {
 return request({
  url: url.passwordAging,
  method: 'get'
 });
}
// 登录错误提示
export function getCodeImg() {
 return request({
  url: 'auth/code',
  method: 'get'
 });
}
export function getInfo(token) {
 return request({
  url: url.getInfo,
  method: 'get',
  params: { token }
 });
}

export function logout() {
 return request({
  url: url.logout,
  method: 'delete'
 });
}
// 获取按钮权限
export function getButtonAuth() {
 return request({
  url: url.getButtonAuth,
  method: 'get'
 });
}

export function ssoLogin(data) {
 return request({
  url: url.ssoLogin,
  method: 'post',
  data
 });
}

export function ssoLogin1(params) {
 return request({
  url: url.ssoLogin1,
  method: 'post',
  params
 });
}
