import request from '@/utils/request';
import url from '@/api/url-info/index';

export function getDataModelList(params) {
 return request({
  url: url.getDataModelList,
  method: 'get',
  params
 });
}

export function updateDdataModelStatus(data) {
 return request({
  url: url.updateDdataModelStatus,
  method: 'put',
  data
 });
}

export function editDataModel(data) {
 return request({
  url: url.editDataModel,
  method: 'put',
  data
 });
}

export function addDataModel(data) {
 return request({
  url: url.addDataModel,
  method: 'post',
  data
 });
}
