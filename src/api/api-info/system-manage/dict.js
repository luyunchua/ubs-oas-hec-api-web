/**
 * Create by: XXX  overwriter by:沈银雪
 * Time: 2020/12/04
 * Name: 字典管理接口
 * */
import request from '@/utils/request';
import systemUrl from '@/api/url-info/index';

// 获取字典表格
export function getList(params) {
 return request({
  url: systemUrl.getList,
  method: 'get',
  params
 });
}
// 新增字典表格
export function addList(data) {
 return request({
  url: systemUrl.hanldeList,
  method: 'post',
  data
 });
}
// 修改字典表格
export function editList(data) {
 return request({
  url: systemUrl.hanldeList,
  method: 'put',
  data
 });
}
// 删除字典表格
export function deleteList(data) {
 return request({
  url: systemUrl.hanldeList,
  method: 'delete',
  data
 });
}

// 判重
export function checkoutType(params) {
 return request({
  url: systemUrl.checkoutType,
  method: 'get',
  params
 });
}

// 获取字典项表格
export function getDictList(params) {
 return request({
  url: systemUrl.getDictList,
  method: 'get',
  params
 });
}
// 新增字典项表格
export function addDictList(data) {
 return request({
  url: systemUrl.hanldeDictList,
  method: 'post',
  data
 });
}
// 修改字典项表格
export function editDictList(data) {
 return request({
  url: systemUrl.hanldeDictList,
  method: 'put',
  data
 });
}
// 删除字典项表格
export function deleteDictList(data) {
 return request({
  url: systemUrl.hanldeDictList,
  method: 'delete',
  data
 });
}

// 判重字典项
export function checkoutDictType(params) {
 return request({
  url: systemUrl.checkoutDictType,
  method: 'get',
  params
 });
}
