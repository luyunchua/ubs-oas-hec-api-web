import request from '@/utils/request';
import url from '@/api/url-info/index';

export function getDataModelSelect(params) {
 return request({
  url: url.getDataModelSelect,
  method: 'get',
  params
 });
}

export function getRoleSelect(params) {
 return request({
  url: url.getRoleSelect,
  method: 'get',
  params
 });
}

export function updateDataRuleStatus(data) {
 return request({
  url: url.updateDataRuleStatus,
  method: 'put',
  data
 });
}

export function getDataRuleList(params) {
 return request({
  url: url.getDataRuleList,
  method: 'get',
  params
 });
}

export function addDataRule(data) {
 return request({
  url: url.addDataRule,
  method: 'post',
  data
 });
}

export function editDataRule(data) {
 return request({
  url: url.editDataRule,
  method: 'put',
  data
 });
}
