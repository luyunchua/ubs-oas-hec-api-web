/**
 * Create by: 沈银雪  overwriter by:沈银雪
 * Time: 2020/12/04
 * Name: 系统接口管理接口
 * */
import request from '@/utils/request';
import systemUrl from '@/api/url-info/index';
// 新增
export function addInterface(data) {
 return request({
  url: systemUrl.hanldeInterface,
  method: 'post',
  data
 });
}
// 修改
export function editInterface(data) {
 return request({
  url: systemUrl.hanldeInterface,
  method: 'put',
  data
 });
}
// 修改状态
export function updateInterfaceStatus(data) {
 return request({
  url: systemUrl.updateInterfaceStatus,
  method: 'put',
  data
 });
}

// 获取表格
export function getInterfaceTree(params) {
 return request({
  url: systemUrl.getInterfaceTree,
  method: 'get',
  params
 });
}

