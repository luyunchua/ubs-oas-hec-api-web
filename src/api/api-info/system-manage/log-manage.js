/**
 * Create by: 沈银雪  overwriter by:沈银雪
 * Time: 2020/12/04
 * Name: 日志监控接口
 * */
import request from '@/utils/request';
import systemUrl from '@/api/url-info/index';

// 获取列表
export function getLogList(params) {
 return request({
  url: systemUrl.getLogList,
  method: 'get',
  params
 });
}
// 系统下拉框
export function getSelect(params) {
 return request({
  url: systemUrl.getSelectSystem,
  method: 'get',
  params
 });
}
