/**
 * Create by: XXXX  overwriter by:沈银雪
 * Time: 2020/12/03
 * Name: 菜单管理接口
 * */

import request from '@/utils/request';
import systemUrl from '@/api/url-info/index';

// 获取菜单分组下拉框值列表
export function getSelect(params) {
 return request({
  url: systemUrl.getSelectMenu,
  method: 'get',
  params
 });
}
// 菜单修改状态
export function updateMenuStatus(data) {
 return request({
  url: systemUrl.updateMenuStatus,
  method: 'put',
  data
 });
}

// 新增菜单
export function addMenu(data) {
 return request({
  url: systemUrl.hanldeMenu,
  method: 'post',
  data
 });
}
// 修改菜单
export function editMenu(data) {
 return request({
  url: systemUrl.hanldeMenu,
  method: 'put',
  data
 });
}

// 获取树表格
export function getMenuTree(params) {
 return request({
  url: systemUrl.getMenuTree,
  method: 'get',
  params
 });
}

// 获取树表格
export async function getUserMenuTree(params) {
 return await request({
  url: systemUrl.getUserMenuTree,
  method: 'get',
  params
 });
}

