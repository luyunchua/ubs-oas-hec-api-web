/**
 *create by Jancheng ON 2022/1/12
 *
 **/
import request from '@/utils/request';
import systemUrl from '@/api/url-info/index';
// 列表
export function configurationList(params) {
 return request({
  url: systemUrl.configurationList,
  method: 'get',
  params
 });
}
// 修改
export function handleConfiguration(data) {
 return request({
  url: systemUrl.handleConfiguration,
  method: 'put',
  data
 });
}

