import request from '@/utils/request';
import url from '@/api/url-info/index';
// 获取流程列表
// 编辑用户
export function getCategoryList(params) {
 return request({
  url: url.getCategoryList,
  method: 'get',
  params
 });
}
// 验证名字是否重复
export function isNamed(params) {
 return request({
  url: url.isNamed,
  method: 'get',
  params
 });
}

// 删除流程
export function delCategory(data) {
 return request({
  url: url.delCategory,
  method: 'delete',
  data
 });
}
// 新增流程
export function addCategory(data) {
 return request({
  url: url.addCategory,
  method: 'post',
  data
 });
}
// 编辑流程
export function editCategory(data) {
 return request({
  url: url.editCategory,
  method: 'put',
  data
 });
}

// 流程部署
// 流程部署下拉框

export function bpmCategorySelect(parasm) {
 return request({
  url: url.bpmCategorySelect,
  method: 'get',
  parasm
 });
}
// 获取流程部署列表
export function bpmDeployList(params) {
 return request({
  url: url.bpmDeployList,
  method: 'get',
  params
 });
}

// 获取流程部署发布
export function publishDeploy(data) {
 return request({
  url: url.publishDeploy,
  method: 'post',
  data
 });
}
// 激活
export function activeDeploy(data) {
 return request({
  url: url.activeDeploy,
  method: 'put',
  data
 });
}
// 挂起
export function suspend(data) {
 return request({
  url: url.suspend,
  method: 'put',
  data
 });
}

// 配置列表
export function bpmSetList(params) {
 return request({
  url: url.bpmSetList,
  method: 'get',
  params
 });
}

// 获取配置信息
export function editBpmConfigRule(params) {
 return request({
  url: url.editBpmConfigRule,
  method: 'get',
  params
 });
}
// 保存编辑配置
export function saveEditBpmConfigRule(data) {
 return request({
  url: url.saveEditBpmConfigRule,
  method: 'put',
  data
 });
}
// 保存新增配置
export function saveAddBpmConfigRule(data) {
 return request({
  url: url.saveAddBpmConfigRule,
  method: 'post',
  data
 });
}
// 流程任务列表
export function bpmTaskList(params) {
 return request({
  url: url.bpmTaskList,
  method: 'get',
  params
 });
}

// 流程绘制
// 删除流程绘制的版本
export function delModelVersion(data) {
 return request({
  url: url.delModelVersion,
  method: 'delete',
  data
 });
}
// 删除流程
export function delBpmModel(data) {
 return request({
  url: url.delBpmModel,
  method: 'delete',
  data
 });
}
// 获取流程绘制列表
export function bpmModelList(params) {
 return request({
  url: url.bpmModelList,
  method: 'get',
  params
 });
}

// 编辑工作流
export function editBpmModel(params) {
 return request({
  url: url.editBpmModel,
  method: 'get',
  params
 });
}

// 新建流程/bpm/model
export function firstSavebpm(data) {
 return request({
  url: url.firstSavebpm,
  method: 'post',
  data
 });
}
// 更新工作流
export function reditBpmModel(data) {
 return request({
  url: url.reditBpmModel,
  method: 'put',
  data
 });
}
