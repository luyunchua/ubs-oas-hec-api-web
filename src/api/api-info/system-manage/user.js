import request from '@/utils/request';
import { getCrypAes } from '@/utils/rsa-encrypt';
import url from '@/api/url-info/index';

// 获取列表
export function getUserList(params) {
 return request({
  url: url.getUserList,
  method: 'get',
  params
 });
}

// 获取树
export function getOrganizationTree(params) {
 return request({
  url: url.getOrganizationTree,
  method: 'get',
  params
 });
}

// 判断邮箱是否重复
export function isRepeat(params) {
 return request({
  url: url.isRepeat,
  method: 'get',
  params
 });
}
// 重置密码
export function updatePassword(data) {
 return request({
  url: url.updatePassword,
  method: 'put',
  data
 });
}
export function editPass(data) {
 return request({
  url: url.editPass,
  method: 'put',
  data
 });
}
// 判读名字是否重复
export function existName(params) {
 return request({
  url: url.existName,
  method: 'get',
  params
 });
}
// 获取角色
export function getRoleSelect(params) {
 return request({
  url: url.getRoleSelectList,
  method: 'get',
  params
 });
}
// 修改用户状态

export function updateUserStatus(data) {
 return request({
  url: url.updateUserStatus,
  method: 'put',
  data
 });
}
// 添加用户
export function addUser(data) {
 return request({
  url: url.addUser,
  method: 'post',
  data
 });
}

// 编辑用户
export function editUser(data) {
 return request({
  url: url.editUser,
  method: 'put',
  data
 });
}

export function updateEmail(form) {
 const data = {
  password: getCrypAes(form.pass),
  email: form.email
 };
 return request({
  url: `${url.updateEmail}${form.code}`,
  method: 'post',
  data
 });
}

export default {
 getUserList,
 getOrganizationTree,
 getRoleSelect,
 isRepeat,
 existName,
 updatePassword,
 addUser,
 editUser,
 updateUserStatus,
 updateEmail
};

