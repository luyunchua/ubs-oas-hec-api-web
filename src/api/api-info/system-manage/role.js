import request from '@/utils/request';
import api from '@/api/url-info/index';

// 获取列表
export function getRoleList(params) {
 return request({
  url: api.getRoleList,
  method: 'get',
  params
 });
}

// 角色名称是否重复
export function roleNameRepeat(params) {
 return request({
  url: api.roleNameRepeat,
  method: 'get',
  params
 });
}
// 添加角色
export function addRole(data) {
 return request({
  url: api.addRole,
  method: 'post',
  data
 });
}
// 编辑角色
export function editRole(data) {
 return request({
  url: api.editRole,
  method: 'put',
  data
 });
}
// 修改角色状态
export function updateRoleStatus(data) {
 return request({
  url: api.updateRoleStatus,
  method: 'put',
  data
 });
}

// 获取接口树
export function getInterfaceSelect(params) {
 return request({
  url: api.getInterfaceSelect,
  method: 'get',
  params
 });
}

// 获取菜单树
export function getMenuTreeSelect(params) {
 return request({
  url: api.getMenuTreeSelect,
  method: 'get',
  params
 });
}
// 获取用户的权限
export function authMenuList(params) {
 return request({
  url: api.authMenuList,
  method: 'get',
  params
 });
}
// 分配权限
export function allotRoleAuth(data) {
 return request({
  url: api.allotRoleAuth,
  method: 'post',
  data
 });
}

export default {
 getRoleList,
 roleNameRepeat,
 editRole,
 addRole,
 updateRoleStatus,
 getInterfaceSelect,
 getMenuTreeSelect,
 authMenuList,
 allotRoleAuth };
