/**
 * Name: 修改用户信息接口
 * */
import request from '@/utils/request';
import systemUrl from '@/api/url-info/index';

export function resetEmail(data) {
 return request({
  url: systemUrl.resetEmail,
  method: 'post',
  data
 });
}
