/**
 * Create by: XXXX  overwriter by:沈银雪
 * Time: 2020/12/03
 * Name: 组织机构接口
 * */

import request from '@/utils/request';
import systemUrl from '@/api/url-info/index';

// 组织机构树表格
export function getOrganizationList(params) {
 return request({
  url: systemUrl.getOrganizationList,
  method: 'get',
  params
 });
}
// 检查组织机构代码是否重复
export function isCodeRepeat(params) {
 return request({
  url: systemUrl.isCodeRepeat,
  method: 'get',
  params
 });
}
// 修改状态
export function updateOrganizationStatus(data) {
 return request({
  url: systemUrl.updateOrganizationStatus,
  method: 'put',
  data
 });
}
// 上报组织机构信息
export function organizationReport(params) {
 return request({
  url: systemUrl.organizationReport,
  method: 'get',
  params
 });
}
// 修改组织机构--------------------
export function editOrganization(data) {
 return request({
  url: systemUrl.hanldeOrganization,
  method: 'put',
  data
 });
}
// 新增组织机构  -------------
export function addOrganization(data) {
 return request({
  url: systemUrl.hanldeOrganization,
  method: 'post',
  data
 });
}
// 组织机构联系人信息  -----
export function organizationContactsList(params) {
 return request({
  url: systemUrl.organizationContactsList,
  method: 'get',
  params
 });
}
// 组织机构详情 -----
export function organizationDetails(params) {
 return request({
  url: systemUrl.organizationDetails,
  method: 'get',
  params
 });
}
// 组织机构属性得下拉框
export function getOrganizationSelect(params) {
 return request({
  url: systemUrl.getOrganizationSelect,
  method: 'get',
  params
 });
}
// 获取用户所在组织机构信息
export function getUserOrganization(params) {
 return request({
  url: systemUrl.getUserOrganization,
  method: 'get',
  params
 });
}

// 企业类型下拉框
export function getDictList(params = { type: 'enterprise_type' }) {
 return request({
  url: systemUrl.getDictSelect,
  method: 'get',
  params
 });
}

