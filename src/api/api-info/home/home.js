import request from '@/utils/request';
import url from '@/api/url-info/index';

// 系统检测概况
export function dashboardSystemMonitor(params) {
 return request({
  url: url.dashboardSystemMonitor,
  method: 'GET',
  params
 });
}
// 监管账户资金存量七日情况
export function dashboardFundSevenDayStatistics(params) {
 return request({
  url: url.dashboardFundSevenDayStatistics,
  method: 'GET',
  params
 });
}
// 大额资金申报备案使用情况
export function fundDeclaration(params) {
 return request({
  url: url.fundDeclaration,
  method: 'GET',
  params
 });
}
// 企业大额资金预警
export function dashboardFundWarning(params) {
 return request({
  url: url.dashboardFundWarning,
  method: 'GET',
  params
 });
}
// // 企业已备案大额资金使用
// export function outlayProjectInfoList(params) {
//  return request({
//   url: url.outlayProjectInfoList,
//   method: 'GET',
//   params
//  });
// }
// 企业大额资金支出备案情况
export function enterpriseFundDeclaration(params) {
 return request({
  url: url.enterpriseFundDeclaration,
  method: 'GET',
  params
 });
}
// 企业月度支出概况
export function dashboardMonthlyExpenditure(params) {
 return request({
  url: url.dashboardMonthlyExpenditure,
  method: 'GET',
  params
 });
}
// 大额资金报表
export function dashboardFundReport(params) {
 return request({
  url: url.dashboardFundReport,
  method: 'GET',
  params
 });
}

// 大额资金报表
export function fundReport(params) {
 return request({
  url: url.fundReport,
  method: 'GET',
  params
 });
}

// 企业主要货币资金存量统计
export function dashboardMonetaryStatistics(params) {
 return request({
  url: url.dashboardMonetaryStatistics,
  method: 'GET',
  params
 });
}

// 监管企业大额资金支出备案情况
export function dashboardProjectRecordStauts(params) {
 return request({
  url: url.dashboardProjectRecordStauts,
  method: 'GET',
  params
 });
}

// // 企业分页列表
// export function enterpriseInfoList(params) {
//  return request({
//   url: url.enterpriseInfoList,
//   method: 'GET',
//   params
//  });
// }
// // 企业详情
// export function enterpriseInfoDetails(params) {
//  return request({
//   url: url.enterpriseInfoDetails,
//   method: 'GET',
//   params
//  });
// }
// // 大额资金备案处理列表页
// export function outlayProjectInfoPage(params) {
//  return request({
//   url: url.outlayProjectInfoPage,
//   method: 'GET',
//   params
//  });
// }
