import request from '@/utils/request';
import recordUrl from '@/api/url-info/index';

export function singaporeRiskLimitIndicator (params) {
  return request({
    url: recordUrl.singaporeRiskLimitIndicator,
    method: 'get',
    params
  });
}
export function proportionLimit (params) {
  return request({
    url: recordUrl.proportionLimit,
    method: 'get',
    params
  });
}
export function currentYear (params) {
  return request({
    url: recordUrl.currentYear,
    method: 'get',
    params
  });
}
export function depositOccupancy (params) {
  return request({
    url: recordUrl.depositOccupancy,
    method: 'get',
    params
  });
}
export function ratioHedging (data) {
  return request({
    url: recordUrl.ratioHedging,
    method: 'post',
    data
  });
}