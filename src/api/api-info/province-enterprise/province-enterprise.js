import request from '@/utils/request';
import provinceEnterpriseUrl from '@/api/url-info/index';
// *************************大额支出额度配置*************************
// 大额支出额度配置信息分页
export function getLargeSpendList(params) {
 return request({
  url: provinceEnterpriseUrl.getLargeSpendList,
  method: 'get',
  params
 });
}
// 新增大额支出额度配置信息
export function addLargeSpend(data) {
 return request({
  url: provinceEnterpriseUrl.handleLargeSpend,
  method: 'post',
  data
 });
}
// 编辑大额支出额度配置信息
export function editLargeSpend(data) {
 return request({
  url: provinceEnterpriseUrl.handleLargeSpend,
  method: 'put',
  data
 });
}
// 删除大额支出额度配置信息
export function delLargeSpend(params) {
 return request({
  url: provinceEnterpriseUrl.handleLargeSpend,
  method: 'delete',
  params
 });
}
// *************************累计支出笔数设置*************************
// 累计支出笔数设置信息分页
export function getTotalAccountList(params) {
 return request({
  url: provinceEnterpriseUrl.getTotalAccountList,
  method: 'get',
  params
 });
}
// 新增累计支出笔数设置
export function addTotalAccount(data) {
 return request({
  url: provinceEnterpriseUrl.handleTotalAccount,
  method: 'post',
  data
 });
}
// 编辑累计支出笔数设置
export function editTotalAccount(data) {
 return request({
  url: provinceEnterpriseUrl.handleTotalAccount,
  method: 'put',
  data
 });
}
// 删除累计支出笔数设置
export function delTotalAccount(params) {
 return request({
  url: provinceEnterpriseUrl.handleTotalAccount,
  method: 'delete',
  params
 });
}
// *************************累计支出额度设置*************************
// 累计支出额度设置信息分页
export function getTotalLimitList(params) {
 return request({
  url: provinceEnterpriseUrl.getTotalLimitList,
  method: 'get',
  params
 });
}
// 新增累计支出额度设置
export function addTotalLimit(data) {
 return request({
  url: provinceEnterpriseUrl.handleTotalLimit,
  method: 'post',
  data
 });
}
// 编辑累计支出额度
export function editTotalLimit(data) {
 return request({
  url: provinceEnterpriseUrl.handleTotalLimit,
  method: 'put',
  data
 });
}
// 删除累计支出额度设置
export function delTotalLimit(params) {
 return request({
  url: provinceEnterpriseUrl.handleTotalLimit,
  method: 'delete',
  params
 });
}
// *************************资金安全存量设置*************************
// 资金安全存量信息分页
export function getMoneySafeList(params) {
 return request({
  url: provinceEnterpriseUrl.getMoneySafeList,
  method: 'get',
  params
 });
}
// 新增资金安全存量
export function addMoneySafe(data) {
 return request({
  url: provinceEnterpriseUrl.handleMoneySafe,
  method: 'post',
  data
 });
}
// 编辑资金安全存量
export function editMoneySafe(data) {
 return request({
  url: provinceEnterpriseUrl.handleMoneySafe,
  method: 'put',
  data
 });
}
// 删除资金安全存量
export function delMoneySafe(params) {
 return request({
  url: provinceEnterpriseUrl.handleMoneySafe,
  method: 'delete',
  params
 });
}
// *************************对外担保设置*************************
// 对外担保分页
export function getExternalGuaranteeList(params) {
 return request({
  url: provinceEnterpriseUrl.getExternalGuaranteeList,
  method: 'get',
  params
 });
}
// 新增对外担保
export function addExternalGuarantee(data) {
 return request({
  url: provinceEnterpriseUrl.handleExternalGuarantee,
  method: 'post',
  data
 });
}
// 编辑对外担保
export function editExternalGuarantee(data) {
 return request({
  url: provinceEnterpriseUrl.handleExternalGuarantee,
  method: 'put',
  data
 });
}
// 删除对外担保
export function delExternalGuarantee(params) {
 return request({
  url: provinceEnterpriseUrl.handleExternalGuarantee,
  method: 'delete',
  params
 });
}
// *************************报送文件周期设置*************************
// 报送文件周期分页
export function getFilePeriodList(params) {
 return request({
  url: provinceEnterpriseUrl.getFilePeriodList,
  method: 'get',
  params
 });
}
// 新增报送文件周期
export function addFilePeriod(data) {
 return request({
  url: provinceEnterpriseUrl.handleFilePeriod,
  method: 'post',
  data
 });
}
// 编辑报送文件周期
export function editFilePeriod(data) {
 return request({
  url: provinceEnterpriseUrl.handleFilePeriod,
  method: 'put',
  data
 });
}
// 删除报送文件周期
export function delFilePeriod(params) {
 return request({
  url: provinceEnterpriseUrl.handleFilePeriod,
  method: 'delete',
  params
 });
}
// *************************同一账户周期收入预警*************************
// 同一账户周期收入预警分页
export function getSameAccountList(params) {
 return request({
  url: provinceEnterpriseUrl.getSameAccountList,
  method: 'get',
  params
 });
}
// 新增同一账户周期收入预警
export function addSameAccount(data) {
 return request({
  url: provinceEnterpriseUrl.handleSameAccount,
  method: 'post',
  data
 });
}
// 编辑同一账户周期收入预警
export function editSameAccount(data) {
 return request({
  url: provinceEnterpriseUrl.handleSameAccount,
  method: 'put',
  data
 });
}
// 删除同一账户周期收入预警
export function delSameAccount(params) {
 return request({
  url: provinceEnterpriseUrl.handleSameAccount,
  method: 'delete',
  params
 });
}
