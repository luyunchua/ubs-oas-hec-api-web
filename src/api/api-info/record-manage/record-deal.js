import request from '@/utils/request';
import recordUrl from '@/api/url-info/index';
// 大额资金备案处理列表页
export function getRecordDealList(params) {
 return request({
  url: recordUrl.getRecordDealList,
  method: 'get',
  params
 });
}
// 大额资金备案处理-查看
export function getRecordDealDetail(params) {
 return request({
  url: recordUrl.getRecordDealDetail,
  method: 'get',
  params
 });
}
// 备案处理备案退回/审批通过
export function recordPassBack(data) {
 return request({
  url: recordUrl.recordPassBack,
  method: 'put',
  data
 });
}
// 企业已备案大额资金使用情况列表
export function getUseStateList(params) {
 return request({
  url: recordUrl.getUseStateList,
  method: 'get',
  params
 });
}
// 已备案大额资金项目对账单（报表）
export function getUseStateDetail(params) {
 return request({
  url: recordUrl.getUseStateDetail,
  method: 'get',
  params
 });
}
