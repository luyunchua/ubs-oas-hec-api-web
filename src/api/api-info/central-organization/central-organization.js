import request from '@/utils/request';
import recordUrl from '@/api/url-info/index';

export function getQuantity (params) {
  return request({
    url: recordUrl.quantity,
    method: 'get',
    params
  });
}
export function unitTypeQuantity (params) {
  return request({
    url: recordUrl.unitTypeQuantity,
    method: 'get',
    params
  });
}
export function levelQuantity (params) {
  return request({
    url: recordUrl.levelQuantity,
    method: 'get',
    params
  });
}
export function enterpriseQuantity (params) {
  return request({
    url: recordUrl.enterpriseQuantity,
    method: 'get',
    params
  });
}
export function details (params) {
  return request({
    url: recordUrl.details,
    method: 'get',
    params
  });
}
export function enterpriseType (params) {
  return request({
    url: recordUrl.enterpriseType,
    method: 'get',
    params
  });
}
export function listingSituation (params) {
  return request({
    url: recordUrl.listingSituation,
    method: 'get',
    params
  });
}
export function settlementSituation (params) {
  return request({
    url: recordUrl.settlementSituation,
    method: 'get',
    params
  });
}
export function auditStatus (params) {
  return request({
    url: recordUrl.auditStatus,
    method: 'get',
    params
  });
}
export function occupationStatistics (params) {
  return request({
    url: recordUrl.occupationStatistics,
    method: 'get',
    params
  });
}
export function natureOfShareholders (params) {
  return request({
    url: recordUrl.natureOfShareholders,
    method: 'get',
    params
  });
}
export function managementForms (params) {
  return request({
    url: recordUrl.managementForms,
    method: 'get',
    params
  });
}
export function enterpriseDetails (params) {
  return request({
    url: recordUrl.enterpriseDetails,
    method: 'get',
    params
  });
}
export function particularsNatureShareholders (params) {
  return request({
    url: recordUrl.particularsNatureShareholders,
    method: 'get',
    params
  });
}
export function getJwDictItem (params) {
  return request({
    url: recordUrl.getJwDictItem,
    method: 'get',
    params
  });
}