import request from '@/utils/request';
import url from '@/api/url-info/index';

// 企业分页列表
export function enterpriseInfoList(params) {
 return request({
  url: url.enterpriseInfoList,
  method: 'GET',
  params
 });
}
// 大额资金报表
export function fundReport(params) {
 return request({
  url: url.fundReport,
  method: 'GET',
  params
 });
}
// 企业详情
export function enterpriseInfoDetails(params) {
 return request({
  url: url.enterpriseInfoDetails,
  method: 'GET',
  params
 });
}
// 大额资金备案处理列表页
export function outlayProjectInfoPage(params) {
 return request({
  url: url.outlayProjectInfoPage,
  method: 'GET',
  params
 });
}
// 企业大额支出备案
export function outlayProjectInfoList(params) {
 return request({
  url: url.outlayProjectInfoList,
  method: 'GET',
  params
 });
}
// 删除企业信息
export function enterpriseInfoDel(params) {
 return request({
  url: url.enterpriseInfoDel,
  method: 'DELETE',
  params
 });
}
// 新增企业信息
export function enterpriseInfoAdd(data) {
 return request({
  url: url.enterpriseInfoAdd,
  method: 'post',
  data
 });
}
// 编辑企业信息
export function enterpriseInfoEdit(data) {
 return request({
  url: url.enterpriseInfoEdit,
  method: 'put',
  data
 });
}

// 大额支出额度配置信息分页
export function outlayAmountConfigList(params) {
 return request({
  url: url.outlayAmountConfigList,
  method: 'GET',
  params
 });
}
// 新增大额支出额度配置信息
export function outlayAmountConfigtAdd(data) {
 return request({
  url: url.outlayAmountConfigtAdd,
  method: 'POST',
  data
 });
}
// 编辑大额支出额度配置信息
export function outlayAmountConfigtEdit(data) {
 return request({
  url: url.outlayAmountConfigtEdit,
  method: 'PUT',
  data
 });
}
// 删除大额支出额度配置信息
export function outlayAmountConfigtDel(params) {
 return request({
  url: url.outlayAmountConfigtDel,
  method: 'DELETE',
  params
 });
}
// ///////////////
// 累计支出笔数配置信息分页
export function outlayNumberConfigList(params) {
 return request({
  url: url.outlayNumberConfigList,
  method: 'GET',
  params
 });
}
// 新增累计支出笔数配置信息
export function outlayNumberConfigAdd(data) {
 return request({
  url: url.outlayNumberConfigAdd,
  method: 'POST',
  data
 });
}
// 编辑累计支出笔数配置信息
export function outlayNumberConfigEdit(data) {
 return request({
  url: url.outlayNumberConfigEdit,
  method: 'PUT',
  data
 });
}
// 删除累计支出笔数配置信息
export function outlayNumberConfigDel(params) {
 return request({
  url: url.outlayNumberConfigDel,
  method: 'DELETE',
  params
 });
}
// ////
// 累计支出额度配置信息
export function outlayAmountTotalConfigList(params) {
 return request({
  url: url.outlayAmountTotalConfigList,
  method: 'GET',
  params
 });
}
// 新增累计支出额度配置信息
export function outlayAmountTotalConfigAdd(data) {
 return request({
  url: url.outlayAmountTotalConfigAdd,
  method: 'POST',
  data
 });
}
// 编辑累计支出额度配置信息
export function outlayAmountTotalConfigEdit(data) {
 return request({
  url: url.outlayAmountTotalConfigEdit,
  method: 'PUT',
  data
 });
}
// 删除累计支出额度配置信息
export function outlayAmountTotalConfigDel(params) {
 return request({
  url: url.outlayAmountTotalConfigDel,
  method: 'DELETE',
  params
 });
}
// ////
// 累计资金安全存量配置信息
export function stockSafeConfigList(params) {
 return request({
  url: url.stockSafeConfigList,
  method: 'GET',
  params
 });
}
// 新增资金安全存量配置信息
export function stockSafeConfigAdd(data) {
 return request({
  url: url.stockSafeConfigAdd,
  method: 'POST',
  data
 });
}
// 编辑资金安全存量配置信息
export function stockSafeConfigListEdit(data) {
 return request({
  url: url.stockSafeConfigListEdit,
  method: 'PUT',
  data
 });
}
// 删除资金安全存量配置信息
export function stockSafeConfigListDel(params) {
 return request({
  url: url.stockSafeConfigListDel,
  method: 'DELETE',
  params
 });
}
// ////
// 累计对外担保配置信息
export function guaranteeConfigList(params) {
 return request({
  url: url.guaranteeConfigList,
  method: 'GET',
  params
 });
}
// 新增对外担保配置信息
export function guaranteeConfigAdd(data) {
 return request({
  url: url.guaranteeConfigAdd,
  method: 'POST',
  data
 });
}
// 编辑对外担保配置信息
export function guaranteeConfigEdit(data) {
 return request({
  url: url.guaranteeConfigEdit,
  method: 'PUT',
  data
 });
}
// 删除对外担保配置信息
export function guaranteeConfigDel(params) {
 return request({
  url: url.guaranteeConfigDel,
  method: 'DELETE',
  params
 });
}
// 预览
export function filePreview(params) {
 return request({
  url: url.filePreview,
  method: 'GET',
  params
 });
}
// 现金账户信息  /cashAccount/list
export function cashAccountList(params) {
 return request({
  url: url.cashAccountList,
  method: 'GET',
  params
 });
}
