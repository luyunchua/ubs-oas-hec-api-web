import request from '@/utils/request';
import warnManageUrl from '@/api/url-info/index';
// *************************大额资金预警模块*************************
// 大额支出预警列表
export function getExpandWarnList(params) {
 return request({
  url: warnManageUrl.getExpandWarnList,
  method: 'get',
  params
 });
}
// 累计笔数超标预警列表
export function getAccWarnList(params) {
 return request({
  url: warnManageUrl.getAccWarnList,
  method: 'get',
  params
 });
}
// 累计金额超标预警列表
export function getMoneyWarnList(params) {
 return request({
  url: warnManageUrl.getMoneyWarnList,
  method: 'get',
  params
 });
}
// 未备案支出预警列表
export function getUnrecordWarnList(params) {
 return request({
  url: warnManageUrl.getUnrecordWarnList,
  method: 'get',
  params
 });
}
// 对外担保预警
export function getDebtWarnList(params) {
 return request({
  url: warnManageUrl.getDebtWarnList,
  method: 'get',
  params
 });
}
// 文件超时未报预警列表
export function getFileWarnList(params) {
 return request({
  url: warnManageUrl.getFileWarnList,
  method: 'get',
  params
 });
}
// 同一账户周期收入预警列表
export function getPeriodWarnList(params) {
 return request({
  url: warnManageUrl.getPeriodWarnList,
  method: 'get',
  params
 });
}
// 资金安全存量预警列表
export function getStockWarnList(params) {
 return request({
  url: warnManageUrl.getStockWarnList,
  method: 'get',
  params
 });
}
// 预警消除
export function warningClear(data) {
 return request({
  url: warnManageUrl.warningClear,
  method: 'post',
  data
 });
}
