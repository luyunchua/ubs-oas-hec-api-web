/**
 * Create by: zhangJie
 * Time:  2020/12/9
 * Name: 流程模块接口
 * */
import path from './module-path';

const { workflowUrl } = path;

const url = {
 companyName: `${workflowUrl}/base/index/list`,
 getCategoryList: `${workflowUrl}/bpm/category/pageList`,
 isNamed: `${workflowUrl}/bpm/category/check`,
 delCategory: `${workflowUrl}/bpm/category`,
 addCategory: `${workflowUrl}/bpm/category`,
 editCategory: `${workflowUrl}/bpm/category`,
 bpmCategorySelect: `${workflowUrl}/bpm/category/select`,
 bpmDeployList: `${workflowUrl}/bpm/deploy/pageList`,
 publishDeploy: `${workflowUrl}/bpm/deploy`,
 activeDeploy: `${workflowUrl}/bpm/deploy/activate`,
 suspend: `${workflowUrl}/bpm/deploy/suspend`,
 bpmSetList: `${workflowUrl}/bpm/deploy/activate/pageList`,
 editBpmConfigRule: `${workflowUrl}/bpm/config/rule`,
 saveEditBpmConfigRule: `${workflowUrl}/bpm/config/rule`,
 saveAddBpmConfigRule: `${workflowUrl}/bpm/config/rule`,
 bpmTaskList: `${workflowUrl}/bpm/config/taskList`,
 delModelVersion: `${workflowUrl}/bpm/model/version`,
 delBpmModel: `${workflowUrl}/bpm/model`,
 bpmModelList: `${workflowUrl}/bpm/model/pageList`,
 editBpmModel: `${workflowUrl}/bpm/model`,
 firstSavebpm: `${workflowUrl}/bpm/model`,
 reditBpmModel: `${workflowUrl}/bpm/model`,
 deployImage: `${workflowUrl}/bpm/deploy/image`,
 /** *********审批*************/
 getProcessTaskList: `${workflowUrl}/process/task/list`, // 任务验收-审批节点信息-xww
 getProcessUserList: `${workflowUrl}/process/task/user/list`, // 获取流程任务审批人列表
 getTaskImage: `${workflowUrl}/process/task/image`, // 任务验收-获取审批节点图片(新)
 getTaskPass: `${workflowUrl}/process/task/pass`, // 任务验收-审批通过(新)
 getTaskReject: `${workflowUrl}/process/task/reject`, // 任务验收-审批驳回(新)
 getHistoryTaskList: `${workflowUrl}/process/historyTask/list` // 流程任务流转记录列表
};
export default url;
