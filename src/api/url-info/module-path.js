/**
 *create by Jancheng ON 2022/1/10
 *
 **/
const path = {
 core: '/lfs-core',
 systemUrl: '/lfs-system'
 // 不走网关调试
 // core: 'http://10.102.42.231:6065',
 // systemUrl: 'http://10.102.42.231:6066'
//  core: 'http://192.168.43.248:6065',
//  systemUrl: 'http://192.168.43.248:6066'
};
export default path;

