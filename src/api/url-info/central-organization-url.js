// 境外组织机构
import path from './module-path';

const { systemUrl } = path;

const recordUrl = {
  quantity: `${systemUrl}/overseas/organization/domesticAndOverseas/quantity`, // 境内境外组织机构数量统计
  unitTypeQuantity: `${systemUrl}/overseas/organization/unitType/quantity`, // 境外单位类型数量
  levelQuantity: `${systemUrl}/overseas/organization/enterprise/level/quantity`, // 境外企业层级数量
  enterpriseQuantity: `${systemUrl}/overseas/organization/enterprise/quantity`, // 境外企业数量
  details: `${systemUrl}/overseas/organization/enterprise/details`, // 企业详情
  enterpriseType: `${systemUrl}/overseas/organization/enterprise/type`, // 企业类型
  listingSituation: `${systemUrl}/overseas/organization/listing/situation`, // 上市情况
  settlementSituation: `${systemUrl}/overseas/organization/settlement/situation`, // 结算情况
  auditStatus: `${systemUrl}/overseas/organization/audit/status`, // 审核情况
  occupationStatistics: `${systemUrl}/overseas/organization/occupation/statistics`, // 所属行业统计
  natureOfShareholders: `${systemUrl}/overseas/organization/natureOfShareholders`, // 企业股东性质
  managementForms: `${systemUrl}/overseas/organization/managementForms`, // 精英状态
  enterpriseDetails: `${systemUrl}/overseas/organization/enterprise/details`, // 企业详情
  particularsNatureShareholders: `${systemUrl}/overseas/organization/particularsNatureShareholders`, // 企业详情
};
export default recordUrl;