// 风控日报
import path from './module-path';

const { systemUrl } = path;

const recordUrl = {
  singaporeRiskLimitIndicator: `${systemUrl}/derivatives/riskDaily/singaporeRiskLimitIndicator`, // 新加坡风险限额指标
  proportionLimit: `${systemUrl}/derivatives/riskDaily/proportionLimit`, // 限额占比
  currentYear: `${systemUrl}/derivatives/riskDaily/proportionLimit/currentYear`, // 限额占比本年
  ratioHedging: `${systemUrl}/derivatives/riskDaily/ratioHedging/currentYear`, // 套保比例本年
  depositOccupancy: `${systemUrl}/derivatives/riskDaily/depositOccupancy` // 保证金占用
};
export default recordUrl;