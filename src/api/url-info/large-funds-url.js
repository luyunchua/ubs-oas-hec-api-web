// 境外组织机构
import path from './module-path';

const { systemUrl } = path;

const recordUrl = {
  totalPercentage: `${systemUrl}/largeAmountFunds/fundExpenditureStatistics/totalPercentage`, // 资金支出统计-总额与百分比
  listOfTotalFunds: `${systemUrl}/largeAmountFunds/fundExpenditureStatistics/totalPercentage/listOfTotalFunds`, // 资金支出统计-总额与百分比-下钻页面资金总额列表
  fundExpenditureStatistics: `${systemUrl}/largeAmountFunds/fundExpenditureStatistics/fundExpenditureStatistics`, // 资金支出统计-资金支出统计
  useFundsExpenditureRanking: `${systemUrl}/largeAmountFunds/useFundsExpenditureRanking`, // 资金用途支出排名
  fundDailyList: `${systemUrl}/largeAmountFunds/useFundsExpenditureRanking/fundDailyList`, // 资金用途支出排名-下钻页面资金日报列表
  fiveDays: `${systemUrl}/largeAmountFunds/changesInTotalFunds/fiveDays`, // 资金总额变化情况-资金总额近5日变化情况
  fiveMonths: `${systemUrl}/largeAmountFunds/changesInTotalFunds/fiveMonths`, // 资金总额变化情况-资金总额近5个月变化情况
  fiveQuarters: `${systemUrl}/largeAmountFunds/changesInTotalFunds/fiveQuarters`, // 资金总额变化情况-资金总额近5个季度变化情况
  fiveYears: `${systemUrl}/largeAmountFunds/changesInTotalFunds/fiveYears`, // 资金总额变化情况-资金总额近5年变化情况
};
export default recordUrl;