// 业务月报
import path from './module-path';

const { systemUrl } = path;

const recordUrl = {
  instrumentPanel: `${systemUrl}/derivatives/businessMonthly/instrumentPanel`, // 仪表图
  annualSize: `${systemUrl}/derivatives/businessMonthly/annualSize`, // 年度规模
  cumulativeProfitLossCurrentYear: `${systemUrl}/derivatives/businessMonthly/cumulativeProfitLossCurrentYear`, // 本年累计盈亏
  hedgeTotalProfitLossCurrentYear: `${systemUrl}/derivatives/businessMonthly/hedgeTotalProfitLossCurrentYear`, // 套保本年累计总盈亏
  currencyHoldings: `${systemUrl}/derivatives/businessMonthly/currencyHoldings`, // 货币类持仓量
  completion: `${systemUrl}/derivatives/businessMonthly/annualSize/completion`, // 业务月报-年度规模-下钻页面-年度计划完成情况
  monthDetails: `${systemUrl}/derivatives/businessMonthly/hedgeTotalProfitLossCurrentYear/details` // 业务月报-年度规模-下钻页面-年度计划完成情况
};
export default recordUrl;