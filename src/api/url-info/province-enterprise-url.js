/**
 * 监管企业内部参数配置模块api url
 * By yani 20220406
 */
import path from './module-path';

const { core } = path;

const provinceEnterpriseUrl = {
 // ************大额支出额度设置************
 getLargeSpendList: `${core}/outlayAmountConfig/list`, // 大额支出额度配置信息分页
 handleLargeSpend: `${core}/outlayAmountConfig`, // 新增、编辑、删除大额支出额度配置信息
 // ************累计支出笔数设置************
 getTotalAccountList: `${core}/outlayNumberConfig/list`, // 累计支出笔数配置信息分页
 handleTotalAccount: `${core}/outlayNumberConfig`, // 新增、编辑、删除累计支出笔数
 // ************累计支出额度设置************
 getTotalLimitList: `${core}/outlayAmountTotalConfig/list`, // 累计支出额度配置分页信息分页
 handleTotalLimit: `${core}/outlayAmountTotalConfig`, // 新增、编辑、删除累计支出额度
 // ************资金安全存量设置************
 getMoneySafeList: `${core}/stockSafeConfig/list`, // 资金安全存量配置分页信息分页
 handleMoneySafe: `${core}/stockSafeConfig`, // 新增、编辑、删除资金安全存量配置
 // ************对外担保设置************
 getExternalGuaranteeList: `${core}/guaranteeConfig/list`, // 对外担保配置分页信息分页
 handleExternalGuarantee: `${core}/guaranteeConfig`, // 新增、编辑、删除对外担保配置
 // ************报送文件周期设置************
 getFilePeriodList: `${core}/fileFrequencyConfig/list`, // 报送文件周期设置分页
 handleFilePeriod: `${core}/fileFrequencyConfig`, // 新增、编辑、删除对外担保配置
 // ************同一账户周期收入预警************
 getSameAccountList: `${core}/accountIncomeConfig/list`, // 同一账户周期收入预警分页
 handleSameAccount: `${core}/accountIncomeConfig` // 新增、编辑、删除同一账户周期收入预警
};
export default provinceEnterpriseUrl;
