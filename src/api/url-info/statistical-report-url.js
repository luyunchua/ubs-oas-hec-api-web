// 统计报表
import path from './module-path';

const { systemUrl } = path;

const recordUrl = {
  derivativeDetail: `${systemUrl}/derivatives/statisticalStatement/derivativeDetail`, // 衍生品明细表
  commodityBusinessTypeCollect: `${systemUrl}/derivatives/statisticalStatement/commodityBusinessTypeCollect`, // 商品类业务品种汇总表
  tableExport: `${systemUrl}/derivatives/statisticalStatement/tableExport`, // 表格导出
};
export default recordUrl;