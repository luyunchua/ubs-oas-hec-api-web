/**
 * Create by: zhangJie
 * Time:  2020/12/9
 * Name: 系统管理模块接口
//  * */
import path from './module-path';

const { systemUrl } = path;

const url = {
  getUserInfo: `${systemUrl}/user/info`,
  getUserMenuTree: `${systemUrl}/auth/menu/list`, // 获取菜单
  getButtonAuth: `${systemUrl}/auth/menu/button/list`, // 获取按钮权限
  /** *********获取字典下拉框*************/
  getDictSelect: `${systemUrl}/dict/item/select`,
  getDictSelectList: `${systemUrl}/dict/item/list`,
  getDictList1: `${systemUrl}/dict/item/list`,
  /** *********系统接口管理接口模块*************/
  hanldeInterface: `${systemUrl}/interface`, // 新增,修改
  getInterfaceTree: `${systemUrl}/interface/tree`, // 获取表格
  updateInterfaceStatus: `${systemUrl}/interface/status`, // 修改状态
  /** *********组织机构接口模块*************/
  organizationReport: `${systemUrl}/organizationSubmit/report`, // 上报组织机构信息
  hanldeOrganization: `${systemUrl}/organization`, // 新增,修改组织机构
  addOrganization: `${systemUrl}/organization`, // 新增组织机构
  editOriganization: `${systemUrl}/organization`, // 编辑组织机构
  organizationDetails: `${systemUrl}/organization/id`, // 编辑组织机构
  organizationContactsList: `${systemUrl}/organization/contacts/page`, // 组织机构联系人信息-2.1版
  updateOrganizationStatus: `${systemUrl}/organization/status`, // 修改状态
  getOrganizationList: `${systemUrl}/organization/list`, // 组织机构树表格
  isCodeRepeat: `${systemUrl}/organization/check`, // 检查组织机构代码是否重复
  getOrganizationSelect: `${systemUrl}/dict/item/select?type=organization_attribute`, // 获取组织机构属性下拉框
  getUserOrganization: `${systemUrl}/user/organization`, // 获取用户所在组织机构信息
  getUserOrgList: `${systemUrl}/organization/privilegeList`, // 获取组织机构列表（数据模型权限）
  /** *********字典管理接口模块*************/
  getList: `${systemUrl}/dict/pageList`, // 获取字典表格
  hanldeList: `${systemUrl}/dict`, // 新增,修改,删除字典表格
  checkoutType: `${systemUrl}/dict/check`, // 判重
  getDictList: `${systemUrl}/dict/item/pageList`, // 获取字典项表格
  hanldeDictList: `${systemUrl}/dict/item`, // 新增,修改,删除字典项表格
  checkoutDictType: `${systemUrl}/dict/item/check`, // 判重字典项
  /** *********日志监控接口模块*************/
  getLogList: `${systemUrl}/log/logger/list`, // 获取表格数据
  getSelectSystem: `${systemUrl}/dict/item/select?type=dsep_system`, // 系统下拉框
  /** *********菜单管理接口模块*************/
  getMenuTree: `${systemUrl}/menu/tree`, // 获取树表格
  hanldeMenu: `${systemUrl}/menu`, // 修改新增
  updateMenuStatus: `${systemUrl}/menu/status`, // 菜单修改状态
  getSelectMenu: `${systemUrl}/dict/item/select?type=menu_group`, // 获取菜单分组下拉框值列表
  selectScope: `${systemUrl}/dict/item/select?type=index_scope`, // 指标适用范围下拉框
  /** ***********督办事项管理模块*************** */
  getSuperviseItemList: `${systemUrl}/dict/item/select?type=plate`, // 所属方案重点任务项
  getSuperviseDeptsList: `${systemUrl}/organization/parent/list`, // 获取企业类型
  getChildrenEnterpriseList: `${systemUrl}/dict/item/select?type=sub_enterprise_type`, // 获取子企业分类清单
  getDeptsList: `${systemUrl}/organization/list?status=1`, // 获取督办单位
  getDeptTreeList: `${systemUrl}/organization/tree`, // 获取责任单位
  getDicList: `${systemUrl}/dict/item/select?type=supervise_type`, // 督办事项类型
  pointerCollect: `${systemUrl}/dict/item/select?type=collect_rate`, // 指标采集频率
  contrast: `${systemUrl}/dict/item/select?type=contrast_mode`,
  /** *********登录*************/
  login: `${systemUrl}/user/login`, // 登录
  ssoLogin: `${systemUrl}/p01/user/single/login`, // 单点
  ssoLogin1: `${systemUrl}/sso/mh/login`, // 单点
  getInfo: `/vue-element-admin/user/info`,
  logout: `${systemUrl}/user/logout`,
  passwordAging: `${systemUrl}/user/expiration`, // 密码过期

  /** *********数据模型*************/
  getDataModelList: `${systemUrl}/dataModel/list`,
  updateDdataModelStatus: `${systemUrl}/dataModel/status`,
  editDataModel: `${systemUrl}/dataModel`,
  addDataModel: `${systemUrl}/dataModel`,

  /** *********权限规则*************/
  getDataModelSelect: `${systemUrl}/dataModel/select`,
  getRoleSelect: `${systemUrl}/role/select`,
  updateDataRuleStatus: `${systemUrl}/dataRule/status`,
  getDataRuleList: `${systemUrl}/dataRule/list`,
  addDataRule: `${systemUrl}/dataRule`,
  editDataRule: `${systemUrl}/dataRule`,

  /** *********用户管理*************/
  getUserList: `${systemUrl}/user/pageList`,
  existName: `${systemUrl}/user/exist`,
  getOrganizationTree: `${systemUrl}/organization/tree`,
  getRoleSelectList: `${systemUrl}/role/select`,
  updateUserStatus: `${systemUrl}/user/status`,
  addUser: `${systemUrl}/user`,
  editUser: `${systemUrl}/user`,
  isRepeat: `${systemUrl}/user/check`,
  updatePassword: `${systemUrl}/user/password/reset`,
  editPass: `${systemUrl}/user/password`,
  getUserListSelect: `${systemUrl}/user/list`,
  /** *********用户个人信息*************/
  resetEmail: `${systemUrl}/api/code/resetEmail`,
  updateEmail: `${systemUrl}/api/users/updateEmail`,
  /** *********角色管理*************/
  getRoleList: `${systemUrl}/role/pageList`,
  roleNameRepeat: `${systemUrl}/role/check`,
  addRole: `${systemUrl}/role`,
  editRole: `${systemUrl}/role`,
  updateRoleStatus: `${systemUrl}/role/status`,
  getInterfaceSelect: `${systemUrl}/interface/tree/select`,
  getMenuTreeSelect: `${systemUrl}/menu/tree/select`,
  authMenuList: `${systemUrl}/role/authMenu/list`,
  allotRoleAuth: `${systemUrl}/role/auth`,
  getUserSelect: `${systemUrl}/user/users/by/roleList`,
  /** *********督办反馈*************/
  getDelayStatusSelect: `${systemUrl}/dict/item/select?type=approval_status`, // 获取审核状态下拉框

  /** *********密钥管理*************/
  getSecretSelect: `${systemUrl}/dict/item/select?type=secret_key_type`, // 获取密钥管理

  /** ***************汇总分析-中央企业实施方案********************* */
  getOrganizationEnterpriseList: `${systemUrl}/organization/enterprise/list`, // 获取企业名称

  /** ***************改革任务********************* */
  getTaskByOption: `${systemUrl}/dict/item/select?type=reform_task_by`, // 获取改革任务来源下拉框
  getTaskOptions: `${systemUrl}/dict/item/select?type=plate`, // 获取任务大类下拉框
  getTaskItemOptions: `${systemUrl}/dict/item/select`, // 获取任务中类下拉框
  /* 组织机构人员*/
  getSelectOrgMemberList: `${systemUrl}/user/organizationMember/list`, // 人员选择-获取组织机构人员列表
  getDepartSelect: `${systemUrl}/organization/department`, // 获取部门下拉框

  /** ***************系统配置********************* */
  configurationList: `${systemUrl}/systemConfig/list`,
  handleConfiguration: `${systemUrl}/systemConfig`,
  systemConfigCheck: `${systemUrl}/systemConfig/check`

};
export default url;
