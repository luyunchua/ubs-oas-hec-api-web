/**
 * 大额资金预警模块api url
 * By yani 20220414
 */
import path from './module-path';

const { core } = path;

const warnManageUrl = {
 // LIST
 getExpandWarnList: `${core}/warningOutlayAmount/page`, // 大额支出预警
 getAccWarnList: `${core}/warningOutlayNumber/page`, // 累计笔数超标预警
 getMoneyWarnList: `${core}/warningOutlayAmountTotal/page`, // 累计金额超标预警
 getUnrecordWarnList: `${core}/warningNotRecord/page`, // 未备案支出预警
 getDebtWarnList: `${core}/warningGuarantee/page`, // 对外担保预警
 getFileWarnList: `${core}/warningFileNotReport/page`, // 文件超时未报预警
 getPeriodWarnList: `${core}/warningAccountIncome/page`, // 同一账户周期收入预警
 getStockWarnList: `${core}/warningStockSafe/page`, // 资金安全存量预警
 // 预警消除
 warningClear: `${core}/warningClear` // 预警消除
};
export default warnManageUrl;
