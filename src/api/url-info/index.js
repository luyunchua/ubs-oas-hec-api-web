/**
 * Create by: shenyinxue
 * Time:  2021/2/1
 * Name: 接口
 * */

// 加载地址信息
function loadUrlInfo() {
 let urlInfo = {};
 // 接口地址文件
 const urlFiles = require.context('./', true, /\-url.js$/);
 urlFiles.keys().forEach((key) => {
  const url = urlFiles(key).default;
  urlInfo = Object.assign(url, urlInfo);
 });
 return urlInfo;
}
const url = loadUrlInfo();

export default url;

