/**
 * 大额资金备案管理--备案处理模块接口
 * By yani 20220314
 */
import path from './module-path';

const { core } = path;

const recordUrl = {
 getRecordDealList: `${core}/outlayProjectInfo/page`, // 大额资金备案处理列表页
 getRecordDealDetail: `${core}/outlayProjectInfo`, // 大额资金备案处理-查看
 recordPassBack: `${core}/outlayProjectInfo/status`, // 备案处理备案退回/审批通过
 getUseStateList: `${core}/outlayProjectInfo/list`, // 企业已备案大额资金使用qingkuang列表
 getUseStateDetail: `${core}/fundsFlow/list` // 已备案大额资金项目对账单（报表）
};
export default recordUrl;
