// 境外舆情
import path from './module-path';

const { systemUrl } = path;

const recordUrl = {
  getJwDictItem: `${systemUrl}/article/getJwDictItem`, // 查询下拉框内容
  pageList: `${systemUrl}/article/pageList`, // 分页查询
  delArticle: `${systemUrl}/article`, // 删除文章
  addArticle: `${systemUrl}/article`, // 修改文章
  editArticle: `${systemUrl}/article` // 新增文章
};
export default recordUrl;