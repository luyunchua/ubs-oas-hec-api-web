/**
 * Create by: zhangJie
 * Time:  2020/12/9
 * Name: 基础模块接口
 * */

import path from './module-path';

const modulePath = window.serverAPI.VUE_APP_API_URL ? window.serverAPI.VUE_APP_API_URL : '/';
const baseUrl = window.serverAPI.VUE_APP_API_URL;

const url = {
 /** *********文件上传/下载/删除*************/
 fileUpload: `${path.core}/file/upload`, // 文件上传
 regulationsListUpload: `${path.core}/regulationsList/file/upload`, // 附件文件上传
 deleteFile: `${path.core}/file/delete`, // 附件删除上传的文件
 searchFile: `${path.core}/file/list`, // 通过关联id查询附件
 downloadFile: `${path.core}/file/download`, // 附件下载文件
 downloadFile1: `${path.core}/tempFile/download`, // 零时文件下载
 queryFile: `${path.core}/tempFile/download?`, // 下载临时附件
 fileExportTemplate: `${path.core}/file/export/template?`, // 下载临时附件
 previewFile: `${path.core}/file/preview`, // 文件预览

 /** *********首页************/
 dashboardSystemMonitor: `${path.core}/dashboard/systemMonitor`, // 系统检测概况
 dashboardFundSevenDayStatistics: `${path.core}/dashboard/fundSevenDayStatistics`, // 监管账户资金存量七日情况
 fundDeclaration: `${path.core}/dashboard/fundDeclaration`, // 大额资金申报备案使用情况
 dashboardFundWarning: `${path.core}/dashboard/fundWarning`, // 企业大额资金预警
 enterpriseFundDeclaration: `${path.core}/outlayProjectInfo/page`, // 企业大额资金支出备案情况
 dashboardProjectRecordStauts: `${path.core}/dashboard/projectRecordStauts`, // 监管企业大额资金支出备案情况
 dashboardMonthlyExpenditure: `${path.core}/dashboard/monthlyExpenditure`, // 企业月度支出概况
 dashboardFundReport: `${path.core}/dashboard/fundReport`, // 大额资金报表
 dashboardMonetaryStatistics: `${path.core}/dashboard/monetaryStatistics`, // 企业主要货币资金存量统计

 /** *********企业管理************/
 enterpriseInfoList: `${path.core}/enterpriseInfo/list`, // 监管企业列表
 fundReport: `${path.core}/fundsFlow/list`, // 大额资金报表
 cashAccountList: `${path.core}/cashAccount/list`, // 现金账户信息
 enterpriseInfoDetails: `${path.core}/enterpriseInfo`, // 企业详情
 outlayProjectInfoList: `${path.core}/outlayProjectInfo/list`, // 企业已备案大额资金使用
 enterpriseInfoDel: `${path.core}/enterpriseInfo`, // 删除企业信息
 enterpriseInfoAdd: `${path.core}/enterpriseInfo`, // 新增企业信息
 enterpriseInfoEdit: `${path.core}/enterpriseInfo`, // 编辑企业信息
 outlayProjectInfoPage: `${path.core}/outlayProjectInfo/page`, // 大额资金备案处理列表页

 outlayAmountConfigList: `${path.core}/outlayAmountConfig/list`, // 大额支出额度配置信息分页
 outlayAmountConfigtAdd: `${path.core}/outlayAmountConfig`, // 新增大额支出额度配置信息
 outlayAmountConfigtEdit: `${path.core}/outlayAmountConfig`, // 编辑大额支出额度配置信息
 outlayAmountConfigtDel: `${path.core}/outlayAmountConfig`, // 删除大额支出额度配置信息

 outlayNumberConfigList: `${path.core}/outlayNumberConfig/list`, // 累计支出笔数配置信息分页
 outlayNumberConfigAdd: `${path.core}/outlayNumberConfig`, // 新增累计支出笔数配置信息
 outlayNumberConfigEdit: `${path.core}/outlayNumberConfig`, // 编辑累计支出笔数配置信息
 outlayNumberConfigDel: `${path.core}/outlayNumberConfig`, // 删除累计支出笔数配置信息

 outlayAmountTotalConfigList: `${path.core}/outlayAmountTotalConfig/list`, // 累计支出额度配置分页信息
 outlayAmountTotalConfigAdd: `${path.core}/outlayAmountTotalConfig`, // 新增累计支出额度配置信息
 outlayAmountTotalConfigEdit: `${path.core}/outlayAmountTotalConfig`, // 编辑累计支出额度配置信息
 outlayAmountTotalConfigDel: `${path.core}/outlayAmountTotalConfig`, // 删除累计支出额度配置信息

 stockSafeConfigList: `${path.core}/stockSafeConfig/list`, // 资金安全存量配置分页信息
 stockSafeConfigAdd: `${path.core}/stockSafeConfig`, // 新增资金安全存量配置信息
 stockSafeConfigListEdit: `${path.core}/stockSafeConfig`, // 编辑资金安全存量配置信息
 stockSafeConfigListDel: `${path.core}/stockSafeConfig`, // 删除资金安全存量配置信息

 guaranteeConfigList: `${path.core}/guaranteeConfig/list`, // 对外担保配置分页信息
 guaranteeConfigAdd: `${path.core}/guaranteeConfig`, // 新增对外担保配置信息
 guaranteeConfigEdit: `${path.core}/guaranteeConfig`, // 编辑对外担保配置信息
 guaranteeConfigDel: `${path.core}/guaranteeConfig`, // 删除对外担保配置信息

 enterpriseInfoDownloadTemplate: `${modulePath}${path.core}/enterpriseInfo/downloadTemplate`, // 下载模板
 enterpriseInfoUpload: `${baseUrl}${path.core}/enterpriseInfo/upload`, // 批量导入
 fileKeyDownload: `${baseUrl}${path.core}/file/dc/fileKey/download`, // 下载文件
 recordDownload: `${baseUrl}${path.core}/outlayProjectInfo/downloadReport`, // 备案文件下载
 filePreview: `${path.core}/file/dc/preview` // 预览
};
export default url;
