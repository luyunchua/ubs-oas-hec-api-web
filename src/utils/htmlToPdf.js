
// 导出页面为PDF格式
import html2Canvas from 'html2canvas';
import JsPDF from 'jspdf';

export default {
 install(Vue, options) {
  Vue.prototype.getPdf = function(num, type) {
   // 当下载pdf时，若不在页面顶部会造成PDF样式不对,所以先回到页面顶部再下载
   let top = document.getElementById('pdfDom');
   if (top != null) {
    top.scrollIntoView();
    top = null;
   }
   const title = this.htmlTitle;
   html2Canvas(document.querySelector('#pdfDom'), {
    allowTaint: true,
    scrollY: 0,
    scrollX: 0,
    scale: 2
   }).then((canvas) => {
    // 获取canvas画布的宽高
    const contentWidth = canvas.width;
    const contentHeight = canvas.height;
    if (type == 'fy') {
     // 一页pdf显示html页面生成的canvas高度;
     const pageHeight = contentWidth / 841.89 * 592.28;
     // 未生成pdf的html页面高度
     let leftHeight = contentHeight;
     // 页面偏移
     let position = 0;
     // html页面生成的canvas在pdf中图片的宽高（本例为：横向a4纸[841.89,592.28]，纵向需调换尺寸）
     const imgWidth = 841.89;
     const imgHeight = 841.89 / contentWidth * contentHeight;
     const pageData = canvas.toDataURL('image/jpeg', 1.0);
     console.log(pageData);
     const PDF = new JsPDF('l', 'pt', 'a4');
     // 两个高度需要区分: 一个是html页面的实际高度，和生成pdf的页面高度
     // 当内容未超过pdf一页显示的范围，无需分页
     if (leftHeight < pageHeight) {
      PDF.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHeight);
     } else {
      while (leftHeight > 0) {
       PDF.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight);
       leftHeight -= pageHeight;
       position -= 592.28;
       // 避免添加空白页
       if (leftHeight > 0) {
        PDF.addPage();
       }
      }
     }
     PDF.save(`${title}.pdf`);
    } else {
     const pageData = canvas.toDataURL('image/jpeg', 1.0);

     // 设置pdf的尺寸，pdf要使用pt单位 已知 1pt/1px = 0.75   pt = (px/scale)* 0.75
     // 2为上面的scale 缩放了2倍
     const pdfX = (contentWidth + 10) / 2 * 0.75;
     const pdfY = (contentHeight + 500) / num * 0.75; // 500为底部留白

     // 设置内容图片的尺寸，img是pt单位
     const imgX = pdfX;
     const imgY = (contentHeight / 2 * 0.75); // 内容图片这里不需要留白的距离

     // 初始化jspdf 第一个参数方向：默认''时为纵向，第二个参数设置pdf内容图片使用的长度单位为pt，第三个参数为PDF的大小，单位是pt
     const PDF = new JsPDF('', 'pt', [pdfX, pdfY]);

     // 将内容图片添加到pdf中，因为内容宽高和pdf宽高一样，就只需要一页，位置就是 0,0
     PDF.addImage(pageData, 'jpeg', 0, 0, imgX, imgY);
     // PDF.save('download.pdf')
     PDF.save(`${title}.pdf`);
    }
   });
  };
 }
};
