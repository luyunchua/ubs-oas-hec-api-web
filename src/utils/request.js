import axios from 'axios';
import { Notification } from 'element-ui';
import store from '@/store';
import { getToken } from '@/utils/auth';
import settings from '@/settings';
import mock from 'mockjs';
import common from '@/utils/common';

const tokenKey = window.serverAPI.VUE_APP_TOKEN_KEY;
const service = axios.create({
  baseURL: window.serverAPI.VUE_APP_API_URL ? window.serverAPI.VUE_APP_API_URL : '/',
  timeout: 30000 // request timeout
});

// request interceptor
service.interceptors.request.use(
  (config) => {
    const { isMock } = settings;
    const newConfig = config;
    const isAbUrl = common.isAbsoluteURL(config.url);
    const fullUrl = isAbUrl ? config.url : common.combineURLs(config.baseURL, config.url);
    let template = null;
    let params = {};
    if (config.data) {
      template = newConfig.data.template || null;
      // 直接删除该参数【此处template必须作为关键字】
      delete newConfig.data.template;
      params = newConfig.data;
    }
    if (config.params) {
      template = newConfig.params.template || null;
      // 直接删除该参数【此处template必须作为关键字】
      delete newConfig.params.template;
      params = newConfig.params;
    }
    // 加密参数签名
    const signBodyData = newConfig.data || {};
    const signGetData = newConfig.params || {};
    const signUrl = fullUrl || '';
    const signInfo = common.buildSign({ signBodyData, signGetData, signUrl }, settings.signSecret);
    const dsepSign = `${signInfo.sign},${signInfo.buildTime}`;
    // mock 必须存在，并且isMock为True 进行拦截
    if (template !== null && isMock) {
      newConfig.params = {};
      newConfig.data = {};
      mock.mock(fullUrl, template);
    }
    // do something before request is sent
    newConfig.headers['Content-Type'] = 'application/json';
    // 请求头加上验签
    newConfig.headers['Dsep-Sign'] = dsepSign;
    // VUE_APP_IS_HTTP_METHOD为true时改变请求方式 并把原请求方式传给后台
    if (window.serverAPI.VUE_APP_IS_HTTP_METHOD) {
      // 拷贝config
      const newConfig = JSON.parse(JSON.stringify(config));
      // 添加原请求方式
      newConfig.headers['Gateway-Http-Method'] = newConfig.method;
      // 改变put、delete为post请求方式
      newConfig.method = config.method === 'put' || config.method === 'delete' ? 'post' : config.method;
    }
    // console.info(`请求地址：${fullUrl}\n请求类型：${newConfig.method}\n请求参数:${JSON.stringify(params)}\n验签密文:${dsepSign}`);
    if (getToken()) {
      newConfig.headers[tokenKey] = getToken();
    }
    return newConfig;
  },
  (error) => {
    // console.log(error);
    // do something with request error
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  /**
    * If you want to get http information such as headers or status
    * Please return  response => response
    */

  /**
    * Determine the request status by custom code
    * Here is just an example
    * You can also judge the status by HTTP Status Code
    */
  (response) => {
    // console.log(response);
    const res = response.data;
    if (res.code !== 200 && res.code !== 0 && res.code !== 40300 && res.code !== 40200 && response.config.url.indexOf('export') === -1 && response.config.url.indexOf('file/download') === -1 && response.config.url.indexOf('improvement/report') === -1) {
      Notification.warning({
        title: '警告',
        dangerouslyUseHTMLString: true,
        message: response.data.message || '系统错误，请联系管理员！'
      });
      return Promise.reject(new Error(res.message || '系统错误，请联系管理员！'));
    } else if (res.code == 40300 || res.code == 40200) {
      Notification.warning({
        title: '警告',
        message: 'token已失效，请重新登陆！',
        duration: 3000
      });
      store.dispatch('user/logout').then(() => {
        setTimeout(() => {
          window.history.go(0);
        }, 800);
      });
    } else {
      return res;
    }
  },
  (error) => {
    // 兼容blob下载出错json提示
    if (error.response.data instanceof Blob && error.response.data.type.toLowerCase().indexOf('json') !== -1) {
      const reader = new FileReader();
      reader.readAsText(error.response.data, 'utf-8');
      reader.onload = function (e) {
        const errorMsg = JSON.parse(reader.result).message;
        Notification.error({
          title: errorMsg,
          duration: 5000
        });
      };
    } else {
      let code = 0;
      try {
        code = error.response.data.status || error.response.data.code;
      } catch (e) {
        if (error.toString().indexOf('Error: timeout') !== -1) {
          Notification.error({
            title: '网络请求超时',
            duration: 5000
          });
          return Promise.reject(error);
        }
      }
      if (code) {
        if (code === 401) {
          store.dispatch('user/logout').then(() => {
            location.reload();
          });
        } else if (code === 403) {
          Notification.error({
            title: `403 ${error.response.data.error}`,
            duration: 5000
          });
        } else if (code === 404) {
          Notification.error({
            title: '404 Not Found',
            duration: 5000
          });
        } else {
          const errorMsg = error.response.data.message;
          if (errorMsg !== undefined) {
            Notification.error({
              title: errorMsg,
              duration: 5000
            });
          }
        }
      } else {
        Notification.error({
          title: error.response.data.error,
          duration: 5000
        });
      }
    }
    return Promise.reject(error);
  }
);

export default service;
