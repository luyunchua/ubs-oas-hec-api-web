/**
 *create by Jancheng ON 2022/1/10
 *
 **/
import _ from 'lodash';
import crypto from 'crypto-js';
// 获取或者设置设备宽度
function getOrSetDeviceWidth() {
 const deviceWidth = document.documentElement.clientWidth > 2560 ||
 document.documentElement.clientWidth < 1280
  ? 1920
  : document.documentElement.clientWidth;
 return deviceWidth;
}
// 计算字体比列
function getFontSize() {
 return getOrSetDeviceWidth() / 19.2;
}

// 是否为绝对路径
function isAbsoluteURL(url) {
 // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
 // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
 // by any combination of letters, digits, plus, period, or hyphen.
 return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
}

// 路径合并
function combineURLs(baseURL, relativeURL) {
 return relativeURL
  ? `${baseURL.replace(/\/+$/, '')}/${relativeURL.replace(/^\/+/, '')}`
  : baseURL;
}

// 生成签名
function buildSign(params = {}, signSecret = 'qdp-secret', requireTime = true) {
 const signArray = [];
 const currentTimeStamp = _.now();
 const { signBodyData, signGetData } = params;
 const { signUrl } = params;
 const urlJson = `url=${signUrl.replace(/^https?:\/\/[^/]+\/+/i, '')}`;
 const splitUrl = urlJson.split('?');
 signArray.push(splitUrl[0]);
 if (splitUrl[1]) {
  const parameter = splitUrl[1].split('&');
  parameter.forEach((item) => {
   signArray.push(item);
  });
 }
 // POST请求参数
 const bodyJson = `body=${JSON.stringify(signBodyData)}`;
 signArray.push(bodyJson);
 // GET请求参数
 Object.keys(signGetData).forEach((key) => {
  const value = signGetData[key];
  const itemJson = `${key}=${value}`;
  signArray.push(itemJson);
 });
 // 签名时间戳
 if (requireTime) {
  const timeStampJson = `timestamp=${currentTimeStamp}`;
  signArray.push(timeStampJson);
 }
 // 所有签名字符串排序
 signArray.sort();
 // 字符串排序合并
 const sortString = signArray.join('&');
 // 加盐混淆
 const secretString = `${signSecret}${sortString}`;
 // MD5加密
 return { sign: crypto.MD5(secretString.toString()), buildTime: currentTimeStamp };
}

export default {
 getFontSize,
 isAbsoluteURL,
 combineURLs,
 buildSign
};
