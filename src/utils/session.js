/**
 * Created by zhangjie on 2020/12/15
 */
const tokenKey = window.serverAPI.VUE_APP_TOKEN_KEY;
const tosSessionStorage = {
 setItem(key, value) {
  window.sessionStorage.setItem(key, JSON.stringify(value));
 },
 getItem(key) {
  const value = window.sessionStorage.getItem(key);
  return JSON.parse(value);
 },
 removeItem(key) {
  window.sessionStorage.removeItem(key);
 },
 clear() {
  window.sessionStorage.clear();
 }
};
// 移除sessionstorage item
function removeSessionStorage(key) {
 tosSessionStorage.removeItem(key);
}
// 清空sessionstorage
function clearSessionStorage() {
 tosSessionStorage.clear();
}
// 设置用户信息
function setUserInfo(user = {}) {
 tosSessionStorage.setItem('TOSUSERINFO', user);
}
// 获取用户信息
function getUserInfo() {
 const user = tosSessionStorage.getItem('TOSUSERINFO');
 if (user) {
  return user;
 } else {
  const res = {
   name: '',
   personName: '',
   ssoToken: '',
   id: '',
   enterpriseId: '',
   enterpriseCode: ''
  };
  res[tokenKey] = '';
  return res;
 }
}
// 设置文件预览地址
function setFilePreviewUrl(url = '') {
 tosSessionStorage.setItem('FILE_PREVIEW_URL', url);
}
// 获取文件预览地址
function getFilePreviewUrl() {
 const current = tosSessionStorage.getItem('FILE_PREVIEW_URL');
 return current;
}

export default {
 removeSessionStorage,
 clearSessionStorage,
 setUserInfo,
 getUserInfo,
 setFilePreviewUrl,
 getFilePreviewUrl
};
