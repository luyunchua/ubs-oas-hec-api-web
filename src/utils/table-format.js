/**
 * Created by liangyani 20220412
 * 通用VXE表格列数据处理方法
 * handleVxeColumn(obj = {})
 */
import { getDictData, getDictLabel } from '@/utils/get-dict-data';

const dictDataList = {
 statusOptions: [], // 币种选项coin_type
 fileTypeList: [], // 文件类型
 frequencyList: [], // 统计周期
 accountScopeList: [], // 统计账户范围
 outlayTypeList: [], // 支出类型
 flatTypeList: [], // 单位类型
 organizationAttributeList: [] // 组织机构属性
};
function getDictList() {
 dictDataList.statusOptions = getDictData('coin_type');// 币种选项coin_type
 dictDataList.fileTypeList = getDictData('file_type'); // 文件类型
 dictDataList.frequencyList = getDictData('frequency');// 统计周期
 dictDataList.accountScopeList = getDictData('account_scope');// 统计账户范围
 dictDataList.outlayTypeList = getDictData('outlay_type');// 支出类型
 dictDataList.flatTypeList = getDictData('flat_type');// 单位类型
 dictDataList.organizationAttributeList = getDictData('organization_attribute');// 组织机构属性
}
// 表格列处理
function formattest(obj = {}) {
 console.log(obj);
 getDictList();
 const columnName = obj.column.field;
 if (columnName == 'coinType') {
  const newVal = getDictLabel(dictDataList.statusOptions, obj.cellValue);
  return newVal;
 } else if (columnName == 'fileType') {
  const newVal = getDictLabel(dictDataList.fileTypeList, obj.cellValue);
  return newVal;
 } else if (columnName == 'frequency') {
  const newVal = getDictLabel(dictDataList.frequencyList, obj.cellValue);
  return newVal;
 } else if (columnName == 'accountScope') {
  const newVal = getDictLabel(dictDataList.accountScopeList, obj.cellValue);
  return newVal;
 } else if (columnName == 'outlayType') {
  const newVal = getDictLabel(dictDataList.outlayTypeList, obj.cellValue);
  return newVal;
 } else if (columnName == 'flatType') {
  const newVal = getDictLabel(dictDataList.flatTypeList, obj.cellValue);
  return newVal;
 } else if (columnName == 'organizationAttribute') {
  const newVal = getDictLabel(dictDataList.organizationAttributeList, obj.cellValue);
  return newVal;
 } else if (columnName == 'limitValue') {
  const newVal = `${obj.cellValue}%`;
  return newVal;
 } else {
  return obj.cellValue;
 }
}
// 封装方法
export function handleVxeColumn(obj = {}) {
 formattest(obj = {});
}
