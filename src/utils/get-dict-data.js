/**
 * Created by zhangJie on
 * 获取字典数据
 */

import { getDictSelect } from '@/api/api-info/public/public';

/**
 * 获取字典的数据
 * @type 类型
 */
export async function getDictData(type) {
 const { data } = await getDictSelect({ type });
 return data || [];
}
/**
 * 获取字典的label
 * @data {array} 字典列表
 * @param {string} 值
 */
export function getDictLabel(data = [], value = '') {
 let label = '';
 data.forEach((item) => {
  if (item.value == value) {
   label = item.label;
  }
 });
 return label;
}
