/**
 *create by Jancheng ON 2022/1/8
 * 加载路由文件信息
 **/
import { constantRoutes, permissionRoutes } from '@/router/index';

// 路由列表转Map
function buildMapRoutes(routeList = [], path = '', key = 'path', isDeep = true) {
 let res = {};
 routeList.forEach((route) => {
  const item = { ...route };
  const currentPath = isDeep ? (path + item[key]).replace(new RegExp('/', 'gm'), '') : item[key].replace(new RegExp('/', 'gm'), '');
  res[currentPath] = {};
  res[currentPath][key] = currentPath;
  if (item.children) {
   const childrenRes = buildMapRoutes(item.children, currentPath, key, isDeep);
   res = Object.assign(res, childrenRes);
  }
 });
 return res;
}
// 重置路由地址
function restAsyncRoutes(asyncRoutes = [], fullParentUrl = '') {
 for (let i = 0; i < asyncRoutes.length; i++) {
  const item = asyncRoutes[i];
  let { url, subType } = item;
  if (fullParentUrl !== '' && subType !== 2) {
   url = `${fullParentUrl}/${url}`;
  }
  item.url = url;
  if (item.children && item.children.length > 0) {
   restAsyncRoutes(item.children, url);
  }
 }
 return asyncRoutes;
}
// 过滤认证路由，该数据用于动态路由添加(仅添加具备权限的路由)
function filterPermissionRoutes(permissionRoutes = [], mapRoutes = {}, pathKay = '') {
 // 先排除子节点
 for (let i = permissionRoutes.length - 1; i > -1; i--) {
  const item = permissionRoutes[i];
  const currentPathKey = (pathKay + item.path).replace(new RegExp('/', 'gm'), '');
  if (mapRoutes[currentPathKey] === undefined &&
    (item.children === undefined || item.children.length === 0) &&
    (item.isPermission === undefined || item.isPermission === true)) {
   permissionRoutes.splice(i, 1);
  } else if (item.children) {
   filterPermissionRoutes(item.children, mapRoutes, currentPathKey);
  }
 }
 return permissionRoutes;
}

// 自动设置重定向路由地址
function resetRedirectPath(permissionRoutes = [], parentPath = '') {
 // 重新设置重定向路径（防止权限调整不能更新路由）
 for (let i = 0; i < permissionRoutes.length; i++) {
  const item = permissionRoutes[i];
  if (item.children != undefined && item.children.length > 0) {
   const childrenItem = item.children[0];// 获取第一个子节点路径
   const currentFullPath = parentPath === '' ? item.path : `${parentPath}/${item.path}`;
   const redirectPath = (`${currentFullPath}/${childrenItem.path}`).replace(new RegExp('/', 'gm'), '/');
   item.redirect = item.redirect === 'autoRedirect' ? redirectPath : item.redirect;
   resetRedirectPath(item.children, currentFullPath);
  }
 }
 return permissionRoutes;
}

// 处理异步路由
export function handelAsyncRoutes(asyncRoutes = []) {
 return restAsyncRoutes(asyncRoutes);
}

// 处理动态路由
export function handelDynamicRoutes(asyncRoutes = []) {
 const asyncMapRoutes = buildMapRoutes(asyncRoutes, '', 'url', false);
 // 排除没有权限路由
 const permissionRouters = filterPermissionRoutes(permissionRoutes, asyncMapRoutes, '');

 // 重置重定向路由
 resetRedirectPath(permissionRouters, '');
 return permissionRouters;
}

