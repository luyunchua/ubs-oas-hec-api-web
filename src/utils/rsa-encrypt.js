import CryptoJS from 'crypto-js';
/**
 * AES加密 ：字符串 key iv  返回base64
 */
export function getCrypAes(word = '', keyStr = '', ivStr = '') {
 let key = CryptoJS.enc.Utf8.parse('80f2cb54c1c7d5c8');// 秘钥
 let iv = CryptoJS.enc.Utf8.parse();// 秘钥偏移量
 if (keyStr) {
  key = CryptoJS.enc.Utf8.parse(keyStr);
  iv = CryptoJS.enc.Utf8.parse(ivStr);
 }
 const srcs = CryptoJS.enc.Utf8.parse(word);
 const encrypted = CryptoJS.AES.encrypt(srcs, key, {
  iv,
  mode: CryptoJS.mode.ECB, // 加密模式
  padding: CryptoJS.pad.Pkcs7 // 补码方式
 });
 return CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
}
/**
 * AES 解密 ：字符串 key iv  返回base64
 *
 */
export function getCrypDAes(word = '', keyStr = '', ivStr = '') {
 let key = CryptoJS.enc.Utf8.parse('80f2cb54c1c7d5c8');// 秘钥
 let iv = CryptoJS.enc.Utf8.parse();// 秘钥偏移量
 if (keyStr) {
  key = CryptoJS.enc.Utf8.parse(keyStr);
  iv = CryptoJS.enc.Utf8.parse(ivStr);
 }
 const base64 = CryptoJS.enc.Base64.parse(word);
 const src = CryptoJS.enc.Base64.stringify(base64);
 const decrypt = CryptoJS.AES.decrypt(src, key, {
  iv,
  mode: CryptoJS.mode.ECB,
  padding: CryptoJS.pad.Pkcs7
 });
 const decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
 return decryptedStr.toString();
}

