// import store from '@/store'

// /**
//  * @param {Array} value
//  * @returns {Boolean}
//  * @example see @/views/permission/directive.vue
//  */
// export default function checkPermission(value) {
//   if (value && value instanceof Array && value.length > 0) {
//     const roles = store.getters && store.getters.roles
//     const permissionRoles = value

//     const hasPermission = roles.some(role => {
//       return permissionRoles.includes(role)
//     })
//     return hasPermission
//   } else {
//     console.error(`need roles! Like v-permission="['admin','editor']"`)
//     return false
//   }
// }

/**
 * 判断按钮是否有权限
 */
import store from '@/store';

export function isHaveBtnAuth(permissionRoles) {
 const roles = store.getters.buttonAuthList;
 const rolesMap = setMap(roles);
 // 如果存在一个'Y'则有权限
 const item = permissionRoles.find((ele) => {
  if (rolesMap[ele]) {
   return rolesMap[ele].isAuth == 'Y';
  } else {
   return true;
  }
 });
 // 没有权限权限
 if (item == undefined) {
  return false;
 }
 return true;
}
function setMap(array) {
 const map = [];
 array.forEach((item) => {
  map[item.code] = item;
 });
 return map;
}

