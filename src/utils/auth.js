import Cookies from 'js-cookie';
import Config from '@/settings';

const tokenKey = window.serverAPI.VUE_APP_TOKEN_KEY;
const userInfoKey = 'userInfo';
export function getToken() {
 return Cookies.get(tokenKey);
}

export function setToken(token) {
 return Cookies.set(tokenKey, token);
}

export function removeToken() {
 return Cookies.remove(tokenKey);
}

export function setUserInfo(userInfo = {}) {
 return Cookies.set(userInfoKey, userInfo);
}
export function getUserInfo() {
 const data = Cookies.get(userInfoKey);
 return data ? JSON.parse(data) : data;
}

export function getCookieInfo(cookieKey = '') {
 return Cookies.get(cookieKey);
}

export function setCookieInfo(cookieKey = '', info = {}, expires = Config.passCookieExpires) {
 return Cookies.set(cookieKey, info, expires);
}

export function removeCookies() {
 const cookies = document.cookie;
 cookies.split(';').forEach((item) => {
  const cookieKey = item.split('=')[0];
  const nowTime = new Date().getTime();
  // 过期一小时
  const oldDate = new Date(nowTime - 1000 * 60 * 60);
  document.cookie = `${cookieKey}=; expires=${oldDate};path=/`;
 });
}
