
// 对json和xml进行格式化
import vkbeautify from 'vkbeautify';
// jsonlint格式化json的js
import jsonlint from './json-lint';

export function stringToJsonWrap(v) {
 try {
  if (isJson(v)) {
   return unicodeToChina(JSON.stringify(stringToJson(v), null, '\t'));
  } else {
   return v;
  }
 } catch (e) {
  console.log(e);
 }
 return v;
}

export function jsonWrapToString(v) {
 try {
  if (isJson(v)) {
   return unicodeToChina(JSON.stringify(stringToJson(v)));
  } else {
   return v;
  }
 } catch (e) {
  console.log(e);
 }
 return v;
}

export function stringToXmlWrap(v) {
 try {
  return vkbeautify.xml(v);
 } catch (e) {
  return v;
 }
}

export function xmlWrapToString(v) {
 try {
  return vkbeautify.xmlmin(v);
 } catch (e) {
  return v;
 }
}

export function isJson(str) {
 if (typeof str == 'string') {
  try {
   const obj = JSON.parse(str);
   if (typeof obj == 'object' && obj) {
    return true;
   } else {
    return false;
   }
  } catch (e) {
   return false;
  }
 }
}

export function checkStringType(v) {
 try {
  if (v.startsWith('<!DOCTYPE html')) {
   return 'HTML';
  } else if (v.startsWith('<')) {
   return 'XML';
  } else if (isJson(v)) {
   return 'JSON';
  } else {
   return 'TEXT';
  }
 } catch (e) {
  return 'TEXT';
 }
}

export function wrapToString(v, t) {
 const type = t || checkStringType(v);
 switch (type) {
  case 'JSON': {
   return jsonWrapToString(v);
  }
  case 'XML': {
   return xmlWrapToString(v);
  }
  case 'HTML': {
   return xmlWrapToString(v);
  }
  default: {
   break;
  }
 }
 return v;
}

export function stringToWrap(v, t) {
 const type = t || checkStringType(v);
 switch (type) {
  case 'JSON': {
   return stringToJsonWrap(v);
  }
  case 'XML': {
   return stringToXmlWrap(v);
  }
  case 'HTML': {
   return stringToXmlWrap(v);
  }
  default: {
   break;
  }
 }
 return v;
}

export function stringToJson(v) {
 try {
  if (isJson(v)) {
   return jsonlint.parse(v);
  } else {
   return v;
  }
 } catch (e) {
  return v;
 }
}

export function unicodeToChina(input) {
 try {
  return input.replace(/\\\\u([0-9a-fA-F]{2,4})/g, (string, matched) => {
   return String.fromCharCode(parseInt(matched, 16));
  });
 } catch (e) {
  console.log(e);
 }
 return input;
}
