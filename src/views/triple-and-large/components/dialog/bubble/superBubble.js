// 用 graph 类型的图标来模拟文字气泡图
export const getSuperBubbleOptions = (originData) => {
  // 同步 graph item 与 legend 的颜色
  const colorList = ['#a076ed', '#d677be', '#6fa1ff', '#e49e7a', '#70be67'];

  console.log('originData: ', originData);
  console.log('colorList: ', colorList);

  const options = {
    animationDurationUpdate: 1500,
    animationEasingUpdate: 'quinticInOut',
    series: [
      {
        type: 'graph',
        layout: 'force',
        avoidLabelOverlap: false,
        force: {
          repulsion: 180, // 节点之间的斥力因子。
          edgeLength: 180 // 边的两个节点之间的距离，这个距离也会受 repulsion。
        },
        roam: false, // 关闭鼠标缩放和平移
        zoom: 1,
        center: ['60%', '50%'],
        edgeSymbol: ['circle', 'arrow'],
        // label: {
        //   formatter: (params) => {
        //     return originData.find((item) => item.name == params.name).personName;
        //   }
        // },
        // progressiveThreshold: 700,
        categories: originData.map((item) => ({ name: item.name })),
        data: originData.map((node, index) => {
          const { id, name, value, size } = node;
          return {
            id,
            name,
            value,
            size,
            category: index,
            symbolSize: size,
            itemStyle: {
              color: colorList[node.id],
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            },
            label: {
              normal: {
                show: true,
                formatter (data) {
                  return `{b|${data.name}}\n{a|${data.value}}`;
                  // return `${data.name}\n${data.value}`;
                },
                rich: {
                  a: {
                    color: '#fff',
                    fontSize: 16,
                    fontWeight: 600,
                    align: 'center'
                  },
                  b: {
                    color: '#fff',
                    fontSize: 12
                  }
                }
              }
              // color: '#ffffff',
              // overflow: 'truncate',
              // ellipsis: '...'
            }
          };
        })
      }
    ]
  };

  return options;
};
