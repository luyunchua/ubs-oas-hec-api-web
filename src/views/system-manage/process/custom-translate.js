/* eslint-disable no-mixed-spaces-and-tabs */
import translations from './translations-german';

export default function customTranslate(template, replacements) {
 const newReplacements = replacements || {};

 // Translate
 const newTemplate = translations[template] || template;

 // Replace
 return newTemplate.replace(/{([^}]+)}/g, (_, key) => {
  let str = newReplacements[key];
  if (translations[newReplacements[key]] != null && translations[newReplacements[key]] != 'undefined') {
   str = translations[newReplacements[key]];
  }
  return str || `{${key}}`;
 });
}
