const getters = {
 sidebar: (state) => state.app.sidebar,
 size: (state) => state.app.size,
 device: (state) => state.app.device,
 user: (state) => state.user.user,
 visitedViews: (state) => state.tagsView.visitedViews,
 cachedViews: (state) => state.tagsView.cachedViews,
 token: (state) => state.user.token,
 avatar: (state) => state.user.avatar,
 name: (state) => state.user.name,
 isLoadMenus: (state) => state.user.isLoadMenus, // 是否加载菜单
 buttonAuthList: (state) => state.permission.buttonAuthList,
 introduction: (state) => state.user.introduction,
 roles: (state) => state.user.roles,
 permissionRoutes: (state) => state.permission.routes,
 status: (state) => state.user.status,
 organizationList: (state) => state.permission.getOrganizationList,
 isLoadRouters: (state) => state.permission.isLoadRouters
};
export default getters;
