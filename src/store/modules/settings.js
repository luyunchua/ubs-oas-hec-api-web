import variables from '@/assets/styles/element-variables.scss';
import defaultSettings from '@/settings';

const { tagsView, fixedHeader, sidebarLogo, uniqueOpened } = defaultSettings;

const state = {
 theme: variables.theme,
 showSettings: false,
 tagsView,
 fixedHeader,
 sidebarLogo,
 uniqueOpened
};

const mutations = {
 CHANGE_SETTING: ({ key, value }) => {
  // eslint-disable-next-line no-prototype-builtins
  if (state.hasOwnProperty(key)) {
   state[key] = value;
  }
 }
};

const actions = {
 changeSetting({ commit }, data) {
  commit('CHANGE_SETTING', data);
 }
};

export default {
 namespaced: true,
 state,
 mutations,
 actions
};

