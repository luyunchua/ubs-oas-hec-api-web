import { constantRoutes, permissionRoutes } from '@/router/index';
import { getButtonAuth } from '@/api/api-info/login/login.js';
import { getOrganizationList } from '@/api/api-info/system-manage/dept';

// 状态
const state = {
 routes: [],
 addRoutes: [],
 buttonAuthList: [],
 organizationList: [],
 isLoadRouters: false
};
// 同步方法
const mutations = {
 // 设置路由
 SET_ROUTES: (state, asyncRouter) => {
  state.addRoutes = asyncRouter;
  state.routes = asyncRouter;
 },
 // 设置按钮权限
 SET_BUTTON_AUTH_LIST: (state, buttonAuthList) => {
  state.buttonAuthList = buttonAuthList;
 },
 // 保存公司列表
 SET_ORGANIZATION_LIST: (state, organizationList) => {
  state.organizationList = mutations.FILER_PERMISSIONS(organizationList);
 },
 // 树转list
 FILER_PERMISSIONS: (tree) => {
  const list = [];
  tree.forEach((tmp) => {
   list.push(tmp);
   if (tmp.children.length !== 0) {
    mutations.TO_LIST_PS(tmp.children, list);
   }
  });
  return list;
 },
 TO_LIST_PS: (tree, list) => {
  tree.forEach((tmp) => {
   list.push(tmp);
   if (tmp.children.length !== 0) {
    mutations.TO_LIST_PS(tmp.children, list);
   }
  });
 },
 // 更新路由加载状态
 UPDATE_IS_LOAD_ROUTERS: (state, type = true) => {
  state.isLoadRouters = type;
 }
};

// 异步方法
const actions = {
 // 提交权限路由
 async generateRoutes({ commit }, asyncRouter) {
  commit('SET_ROUTES', asyncRouter);
  commit('UPDATE_IS_LOAD_ROUTERS');
 },
 // 获取按钮权限
 getButtonAuthList({ commit }) {
  return new Promise((resolve, reject) => {
   getButtonAuth().then((response) => {
    const { data } = response;
    commit('SET_BUTTON_AUTH_LIST', data);
    resolve(data);
   }).catch((error) => {
    reject(error);
   });
  });
 },
 // 获取组织机构列表
 getOrganizationList({ commit }) {
  return new Promise((resolve, reject) => {
   getOrganizationList().then((response) => {
    const { data } = response;
    commit('SET_ORGANIZATION_LIST', data);
    resolve(data);
   }).catch((error) => {
    reject(error);
   });
  });
 }
};
export default {
 namespaced: true,
 state,
 mutations,
 actions
};
