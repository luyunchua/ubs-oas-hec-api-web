import { login, ssoLogin, ssoLogin1, logout } from '@/api/api-info/login/login';
import { getMenuTree } from '@/api/api-info/system-manage/menu';
import { getToken, setToken, removeToken, removeCookies, setUserInfo, getUserInfo, getCookieInfo } from '@/utils/auth';
import session from '@/utils/session';
import loginMock from '@/mock/login/login-mock';
import { passwordAging } from '../../api/api-info/login/login';

const state = {
  userInfo: getUserInfo(),
  token: getToken(),
  name: '',
  avatar: '',
  introduction: '',
  roles: [],
  status: 2,
  menuList: [],
  user: { username: getCookieInfo('username') },
  isLoadMenus: true // 是否更新菜单
};
const mutations = {
  SET_USERINFO: (token) => {
    state.userInfo = token;
  },
  SET_MENULIST: (token) => {
    state.menuList = token;
  },
  SET_TOKEN: (token) => {
    state.token = token;
  },
  SET_INTRODUCTION: (introduction) => {
    state.introduction = introduction;
  },
  SET_NAME: (name) => {
    state.name = name;
  },
  SET_AVATAR: (avatar) => {
    state.avatar = avatar;
  },
  SET_ROLES: (roles) => {
    state.roles = roles;
  },
  SET_LOAD_MENUS: (isLoadMenus) => {
    state.isLoadMenus = isLoadMenus;
  },
  PASS_WORD_AGING_STATUS: (status) => {
    state.status = status;
  }
};
const actions = {
  // user login
  login ({ commit }, userInfo) {
    const { userName, password } = userInfo;
    return new Promise((resolve, reject) => {
      login({ userName: userName.trim(), password, template: loginMock.loginTemplate })
        .then((response) => {
          const { data } = response;
          // 待优化配置
          commit('SET_TOKEN', data.lfsToken);
          commit('SET_USERINFO', data);
          setToken(data.lfsToken);
          setUserInfo(data);
          session.setUserInfo(data);
          resolve(data);
        }).catch((error) => {
          console.log(222);
          reject(error);
        });
    });
  },
  ssoLogin ({ commit }, code) {
    const { userName } = code;
    return new Promise((resolve, reject) => {
      ssoLogin({ userName: userName.trim(), template: loginMock.loginTemplate }).then((res) => {
        const { data } = res;
        commit('SET_TOKEN', data.lfsToken);
        commit('SET_USERINFO', data);
        // setToken(data[window.serverAPI.VUE_APP_TOKEN_KEY]);
        setToken(data.lfsToken);
        setUserInfo(data);
        session.setUserInfo(data);
        resolve(data);
      }).catch((error) => {
        reject(error);
      });
    });
  },
  SSoLogin ({ commit }, ssoToken) {
    return new Promise((resolve, reject) => {
      ssoLogin1({ ssoToken }).then((res) => {
        const { data } = res;
        commit('SET_TOKEN', data.lfsToken);
        commit('SET_USERINFO', data);
        // setToken(data[window.serverAPI.VUE_APP_TOKEN_KEY]);
        setToken(data.lfsToken);
        setUserInfo(data);
        session.setUserInfo(data);
        resolve(data);
      }).catch((error) => {
        reject(error);
      });
    });
  },
  // user logout
  logout ({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      logout().then(() => {
        commit('SET_TOKEN', '');
        session.clearSessionStorage();
        removeCookies();
        resolve();
      })
    });
  },
  // 是否加载菜单
  updateLoadMenus ({ commit }) {
    return new Promise((resolve, reject) => {
      commit('SET_LOAD_MENUS', false);
    });
  },
  // remove token
  resetToken ({ commit }) {
    return new Promise((resolve) => {
      commit('SET_TOKEN', '');
      session.clearSessionStorage();
      removeToken();
      resolve();
    });
  },

  // dynamically modify permissions
  async changeRoles ({ commit, dispatch }, role) {
    const token = `${role}-token`;
    commit('SET_TOKEN', token);
    setToken(token);
  },
  //  密码失效
  passwordAging ({ commit }) {
    return new Promise((resolve, reject) => {
      passwordAging({ template: loginMock.passwordAgingTemplate }).then((response) => {
        const { data } = response;
        commit('PASS_WORD_AGING_STATUS', data.status);
      });
    });
  }

};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
